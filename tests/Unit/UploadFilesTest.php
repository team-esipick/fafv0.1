<?php

namespace Tests\Unit;

use FAF\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
/**
 * Class UploadFilesTest
 * @package Tests\Unit
 */

class UploadFilesTest extends TestCase
{
   public  function testCreateUser(){

       $faker = Factory::create();

       $resp =  $this->json('POST','/api/v1/auth/signup',[

           'company_name' => $faker->name,
           'first_name' => 'admin',
           'last_name' => 'user',
           'phone' => '+010000000000',
           'email' => $faker->email,
           'password' => 'Fetch@file1',
           'c_password' => 'Fetch@file1',
           'sub_domain' => $faker->name,

       ],['Accept' => 'application/json']);


       $user =  \GuzzleHttp\json_decode($resp->content()) ;

   }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testImportCompany()
    {
        $files = Storage::allFiles('test');
        dd($files);
        foreach ($files as $file)
        {
            echo (string)$file, "\n";
        }

        exit;
        $path          = '/Users/usama/PhpstormProjects/faf/test/mensa.json';
        $original_name = 'mensa.json';
        $mime_type     = 'application/json';
        $size          = 2476;
        $error         = null;
        $test          = true;

        $file = new UploadedFile($path, $original_name, $mime_type, $size, $error, $test);

        /* create a new test user in db
        // create new company engeo
        // read all directory and files within that directory
        // for each files hit the store end point
        // keep three status
          1) files stored 1
          2) files stored and indexed 2

         at the end compile a report all the files
        which got indexed or which were left their status
        */

        $resp =  $this->json('POST',
            '/api/v1/storage/store',

            ['file' => $file ,
              'company_id' => 28],

            ['Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjhkZWZiMDA5ZTA4NDlkZGFiYTgxZjE1NjMxMDMwYjNlZDYxNjkxNjlkMjZiZmNkZWI2YTdkNGU4ZDA2MTFmM2QxZWFjYzI5MzM4YTRkN2Y1In0.eyJhdWQiOiI2IiwianRpIjoiOGRlZmIwMDllMDg0OWRkYWJhODFmMTU2MzEwMzBiM2VkNjE2OTE2OWQyNmJmY2RlYjZhN2Q0ZThkMDYxMWYzZDFlYWNjMjkzMzhhNGQ3ZjUiLCJpYXQiOjE1MTQxOTAzMTksIm5iZiI6MTUxNDE5MDMxOSwiZXhwIjoxNTIyODMwMzE5LCJzdWIiOiIyOCIsInNjb3BlcyI6W119.Ywwzz7okQSzuW5TYUzC8Wlr9KRjW_4Bu7CPHnfOZaiFEWS6d9l1QlCjEKSH4icI8O6UfK0fR6N_sl_8Gngg6mimSwrNWdnaDBmstSPCdgGat951SfB5_JSFDXCF24SUQTyqj8NJOXzOcqwJt_Doy7EbarrkaR1vyRlu1FptuvKOq8HSqkCO8zRifYpgOcKIOlggezIZ7M1mDt958Yr1iz7mYu4Lnrm86E6g5Obxg72ITE1Qb71Jqaf_WwDfr6Qq_pZ2ev4_1NO9LmgZYWaUyZ8kglpsvRWoOnP2l5NAMYmqZoby7SsM_uyVsIAMoWgSpMuAYtX7EpUfC8wrvIQEtx79NBdKxAJOQAtQM7V4xsDKB3aXYCXJzyz0mxk_ClFV7Hoo9760C1gRoZaz_rmTuw0ILEBd7hei7343Nvgt6nHvoTRL0t9hq0T0fVp4yFj300c0xo9XLN7kki8-Y2Pdx1d72P84rtvJ4XMKKOvK8W2VR1AiROZbevz1DH5ucCXsCuJ07v38ezID2v8-AUPme7r-09bBufq4hG3fm5kOcmfqoD8wo_EH7RKRQXdTw8HInnATf1iU7MQot6nfpxbf9Ja7FsOuTJ4edzKndIB4KYdHYvF7t2v5xlWndk-AdFHExVLzbd0uj2wqwbb8rs4OCA-My9kx_cJT4VvfWLRZexnY']
        );

        dd($resp);

    }

    /**
     * @param string $fileName
     *
     * @return array
     */
    public static function getMockFile($fileName = 'i.jpeg')
    {
        return $file = new UploadedFile(__FILE__, $fileName, null, null, null, true);
        return [ApiInterface::PARAM_ORIGINAL_FILE_DATA => $file];
    }
}
