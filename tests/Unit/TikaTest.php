<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use FAF\Libs\TikaIngestor;

/**
 * Class TikaTest
 * @package Tests\Unit
 */
class TikaTest extends TestCase
{

    /**
     * Test getText method
     */
    public function testGetText()
    {
        $tika = new TikaIngestor();
        $data = $tika->getText('/Users/usama/PhpstormProjects/faf/test.pdf');
        $this->assertContains('laravel',$data);

    }

    public function testGetMeta()
    {
        $tika = new TikaIngestor();
        $data = $tika->getMeta('/Users/usama/PhpstormProjects/faf/test/Hungr, Oldrich; 1987_Debris Flow Defenses in British Columbia.pdf');
        echo json_encode($data);
        exit;

        $this->assertContains('laravel',$data);
    }


}
