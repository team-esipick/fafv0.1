<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use FAF\Libs\Dexter;
use FAF\Libs\TikaIngestor;

class DexterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAnalyze()
    {
        $dx = new Dexter();
        $tika = new TikaIngestor();
        $data = $tika->getText('C:\test.txt');
        $result = $dx->analyze(1, $data);
        dd($result);
        exit;
        $this->assertNotEquals( array_search('intelligence', $result->Tags), FALSE);
    }
}
