<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use FAF\Libs\Elasticsearch\ElasticSearchIndexer;
use Faker\Factory;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
/**
 * Class ElasticSearchTest
 * @package Tests\Unit
 * @property \FAF\Libs\Elasticsearch\ElasticSearchIndexer $es

 */
class ElasticSearchTest extends TestCase
{

    public $es;
    public function setUp()
    {
        parent::setUp();
        $this->es = new ElasticSearchIndexer();

    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {

        $names = ['pdf','excel','word'];
        $com = ['engeo','esipick'];
        $result = $this->es->store('storage_objects_test_03_05','doc',rand(1,999),[]);
        $collection = factory(\FAF\StorageObject::class,1)->make();

        $result = [];
        foreach ($collection as $storage_object){
            $result[] = $this->es->store('storage_objects_test_03_05','doc',rand(1,10),$storage_object['attributes']);

        }
        dd($result);

    }
    public function testDelete(){
        $this->es->delete('storage_objects_test');
        exit();
    }
    public function testMapping(){

        $r= $this->es->client->indices()->getMapping(['index' => 'storage_objects_test', 'type' => 'string']);
        dd($r);
        exit();
    }

    public function testUpdate(){
        $r= $this->es->update->data(17,['hello','name'] );
        //$r = $es->updateIndex('storage_object', 'string',17,['meta'=>['test']])    ;
        dd($r);
    }

    public function testSearch(){


       $search =  'POST storage_objects/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "bool": {
            "should": [
              {
                "term": {
                  "user_id": {
                    "value": 553
                  }
                }
              },
              {
                "terms": {
                  "user_ids": [
                    379
                  ]
                }
              },
              {
                "terms": {
                  "team_ids": [
                    1043,
                    1146,
                    1080
                  ]
                }
              },
              {
                "terms": {
                  "company_ids": [
                    1043
                  ]
                }
              }
            ]
          }
        },
        {
          "multi_match": {
            "query": "rerum",
            "fields": [
              "title^10",
              "description",
              "tags^8",
              "topics^6"
            ],
            "fuzziness": "auto"
          }
        }
      ],
      "filter": [
        {
          "terms": {
            "file_extn": [
              "pdf",
              "ppt",
              "docx"
            ]
          }
        },
        {
          "range": {
            "file_size": {
              "gte": 2,
              "lte": 20000000000
            }
          }
        },
        {
          "range": {
            "created_date": {
              "gte": "1990-06-04 00:00:00",
              "format": "yyyy-MM-dd HH:mm:ss"
            }
          }
        }
      ]
    }
  },
  "highlight": {
    "fields": {
      "short_description": {},
      "description": {},
      "title": {},
      "tags": {},
      "file_name": {}
    }
  },
  "sort": [
    {
      "_score": {}
    },
    {
      "created_at": {
        "order": "desc"
      }
    },
    {
      "file_size": {
        "order": "desc"
      }
    }
  ],
  "from": 0,
  "size": 10,
  "aggs": {
    "Top File Extn": {
      "terms": {
        "field": "file_extn.keyword",
        "size": 10
      },
      "aggs": {
        "Top Topic\'s": {
          "terms": {
            "field": "topics.keyword"
          },
          "aggs": {
            "Top Tag\'s": {
              "terms": {
                "field": "tags.keyword"
              }
            }
          }
        }
      }
    }
  }
}';

        //searchBool($q,$company_name,$sort_size ='asc', $sort_type ='asc',$from=0,$size=10)
        $r= $this->es->search('storage_objects_test_03_03','GEOLOGIC dfsdfdsf',110);
        dd($r);

    }

    public function testSearchQuery(){


        $data = $this->es->getSearchQuery();
        Arr::set( $data ,$this->es->query->keyword,'this is usama') ;
        Arr::set( $data ,$this->es->query->multi->tags,['lahore','pakistan']) ;
        Arr::set( $data ,$this->es->query->multi->user_ids, [1,2,3,4]) ;
        Arr::set( $data ,$this->es->query->single->user_id,9999) ;

        $data = Arr::dot($data);

        dd($data);


    }
    public function testsearchPerson(){

        $r= $this->es->searchPerson( ['Gold']);
        dd($r);

    }
}
