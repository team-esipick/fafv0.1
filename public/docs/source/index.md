---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Introduction

Well, Fetch-a-File offers just that.

Search through your emails, customer data, and intranet files in one go. Want to search for database data (MySQL, PostgreSQL)? No problem! With Fetch-a-File’s platform integrations, your data is connected – wherever it’s stored. Finding the data you need has never been this easy.


[Get Postman Collection](http://local.faf.com/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_e200731b4b69da66709a66bfbcd204d5 -->
## Heartbeat

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/heartbeat" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/heartbeat",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/heartbeat`

`HEAD /api/v1/heartbeat`


<!-- END_e200731b4b69da66709a66bfbcd204d5 -->

<!-- START_3a9158108fb8966a09bf885f6a2b5854 -->
## User Register

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/signup" \
-H "Accept: application/json" \
    -d "company_name"="accusamus" \
    -d "first_name"="accusamus" \
    -d "last_name"="accusamus" \
    -d "email"="zvandervort@example.net" \
    -d "password"="accusamus" \
    -d "c_password"="accusamus" \
    -d "sub_domain"="accusamus" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/signup",
    "method": "POST",
    "data": {
        "company_name": "accusamus",
        "first_name": "accusamus",
        "last_name": "accusamus",
        "email": "zvandervort@example.net",
        "password": "accusamus",
        "c_password": "accusamus",
        "sub_domain": "accusamus"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/signup`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_name | string |  required  | Maximum: `255`
    first_name | string |  required  | Maximum: `255` Minimum: `1`
    last_name | string |  required  | Maximum: `255` Minimum: `1`
    email | email |  required  | 
    password | string |  required  | 
    c_password | string |  required  | Must be the same as `password`
    sub_domain | string |  required  | 

<!-- END_3a9158108fb8966a09bf885f6a2b5854 -->

<!-- START_f76cc718539c2362f0d0a7069100319e -->
## User Login

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/login" \
-H "Accept: application/json" \
    -d "email"="eudora.jacobson@example.net" \
    -d "password"="in" \
    -d "sub_domain"="in" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/login",
    "method": "POST",
    "data": {
        "email": "eudora.jacobson@example.net",
        "password": "in",
        "sub_domain": "in"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/login`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  required  | 
    password | string |  required  | 
    sub_domain | string |  required  | 

<!-- END_f76cc718539c2362f0d0a7069100319e -->

<!-- START_b591815eb7298ac67431a28c5c83b415 -->
## /api/v1/auth/refresh

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/refresh" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/refresh",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/refresh`


<!-- END_b591815eb7298ac67431a28c5c83b415 -->

<!-- START_a74630f31659c578c0a95bd6fd851140 -->
## User Forgot Password Sending email

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/recovery" \
-H "Accept: application/json" \
    -d "email"="alexis.casper@example.org" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/recovery",
    "method": "POST",
    "data": {
        "email": "alexis.casper@example.org"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/recovery`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  required  | 

<!-- END_a74630f31659c578c0a95bd6fd851140 -->

<!-- START_5e62b96a0004f7f8322743d3df4c272f -->
## User Forgot Password Change password

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/reset" \
-H "Accept: application/json" \
    -d "token"="corrupti" \
    -d "email"="darian.dickens@example.org" \
    -d "password"="corrupti" \
    -d "password_confirmation"="corrupti" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/reset",
    "method": "POST",
    "data": {
        "token": "corrupti",
        "email": "darian.dickens@example.org",
        "password": "corrupti",
        "password_confirmation": "corrupti"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/reset`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    token | string |  required  | 
    email | email |  required  | 
    password | string |  required  | 
    password_confirmation | string |  required  | 

<!-- END_5e62b96a0004f7f8322743d3df4c272f -->

<!-- START_1fb8424c49215245a5a31826b0882c9b -->
## Verify Website Register

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/verify/website" \
-H "Accept: application/json" \
    -d "sub_domain"="velit" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/verify/website",
    "method": "POST",
    "data": {
        "sub_domain": "velit"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/verify/website`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    sub_domain | string |  required  | 

<!-- END_1fb8424c49215245a5a31826b0882c9b -->

<!-- START_7cf4219f6eca350873392fdda2b97369 -->
## Verify Email Register

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/verify/email" \
-H "Accept: application/json" \
    -d "email"="szieme@example.org" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/verify/email",
    "method": "POST",
    "data": {
        "email": "szieme@example.org"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/verify/email`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  required  | 

<!-- END_7cf4219f6eca350873392fdda2b97369 -->

<!-- START_c8c73112914f548ba96e89700b73137e -->
## Verify Email Register

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/verify/company" \
-H "Accept: application/json" \
    -d "company_name"="vero" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/verify/company",
    "method": "POST",
    "data": {
        "company_name": "vero"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/verify/company`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_name | string |  required  | 

<!-- END_c8c73112914f548ba96e89700b73137e -->

<!-- START_dd1d00382e3f72739a4bb059f057df2b -->
## Verify password token

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/verify/password_token" \
-H "Accept: application/json" \
    -d "token"="consequatur" \
    -d "email"="lind.melba@example.org" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/verify/password_token",
    "method": "POST",
    "data": {
        "token": "consequatur",
        "email": "lind.melba@example.org"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/verify/password_token`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    token | string |  required  | 
    email | email |  required  | 

<!-- END_dd1d00382e3f72739a4bb059f057df2b -->

<!-- START_6554a1e034cb490af2ee054f889cc32b -->
## logout

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/auth/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/logout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/auth/logout`


<!-- END_6554a1e034cb490af2ee054f889cc32b -->

<!-- START_d9029afd42745be90d5cceeba2cd6e09 -->
## User Login Data

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/auth/user" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/auth/user",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/auth/user`

`HEAD /api/v1/auth/user`


<!-- END_d9029afd42745be90d5cceeba2cd6e09 -->

<!-- START_f5175082387e9e3af56dc8bcbdd14894 -->
## Storage Add

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/storage/store" \
-H "Accept: application/json" \
    -d "company_id"="36" \
    -d "file"="eos" \
    -d "share_with"="eos" \
    -d "share_id"="36" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/store",
    "method": "POST",
    "data": {
        "company_id": 36,
        "file": "eos",
        "share_with": "eos",
        "share_id": 36
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/storage/store`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    file | file |  required  | Must be a file upload
    share_with | string |  optional  | 
    share_id | numeric |  optional  | 

<!-- END_f5175082387e9e3af56dc8bcbdd14894 -->

<!-- START_18f2f810f634e4e3503618638f12a46f -->
## Storage listing

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/storage/listing" \
-H "Accept: application/json" \
    -d "page"="625" \
    -d "sort"="exercitationem" \
    -d "size"="exercitationem" \
    -d "per_page"="625" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/listing",
    "method": "GET",
    "data": {
        "page": 625,
        "sort": "exercitationem",
        "size": "exercitationem",
        "per_page": 625
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/storage/listing`

`HEAD /api/v1/storage/listing`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    page | numeric |  optional  | 
    sort | string |  optional  | 
    size | string |  optional  | 
    per_page | numeric |  optional  | 

<!-- END_18f2f810f634e4e3503618638f12a46f -->

<!-- START_0cd113c55d45dc46f40d89256ac49b09 -->
## Storage listing

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/storage/share_with_me_storage" \
-H "Accept: application/json" \
    -d "page"="1931970" \
    -d "sort"="est" \
    -d "size"="est" \
    -d "per_page"="1931970" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/share_with_me_storage",
    "method": "GET",
    "data": {
        "page": 1931970,
        "sort": "est",
        "size": "est",
        "per_page": 1931970
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/storage/share_with_me_storage`

`HEAD /api/v1/storage/share_with_me_storage`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    page | numeric |  optional  | 
    sort | string |  optional  | 
    size | string |  optional  | 
    per_page | numeric |  optional  | 

<!-- END_0cd113c55d45dc46f40d89256ac49b09 -->

<!-- START_8813738a633c92da775c1f4554ff9370 -->
## Storage listing

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/storage/recent_storage" \
-H "Accept: application/json" \
    -d "page"="80707" \
    -d "sort"="provident" \
    -d "size"="provident" \
    -d "per_page"="80707" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/recent_storage",
    "method": "GET",
    "data": {
        "page": 80707,
        "sort": "provident",
        "size": "provident",
        "per_page": 80707
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/storage/recent_storage`

`HEAD /api/v1/storage/recent_storage`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    page | numeric |  optional  | 
    sort | string |  optional  | 
    size | string |  optional  | 
    per_page | numeric |  optional  | 

<!-- END_8813738a633c92da775c1f4554ff9370 -->

<!-- START_fbd8bae277b1b3516e1ab7dc84a08706 -->
## Storage get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/storage/detail" \
-H "Accept: application/json" \
    -d "company_id"="31846627" \
    -d "storage_object_id"="31846627" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/detail",
    "method": "GET",
    "data": {
        "company_id": 31846627,
        "storage_object_id": 31846627
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/storage/detail`

`HEAD /api/v1/storage/detail`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    storage_object_id | numeric |  required  | 

<!-- END_fbd8bae277b1b3516e1ab7dc84a08706 -->

<!-- START_9dae2bd69077b8d7e5b69631e21fe6c4 -->
## Search listing
 This function works as the primary search on all files
 in elastic search

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/storage/search" \
-H "Accept: application/json" \
    -d "company_id"="7243" \
    -d "keyword"="qui" \
    -d "from_date"="2017-12-27" \
    -d "to_date"="2017-12-27" \
    -d "to"="7243" \
    -d "from"="7243" \
    -d "sort_type"="date" \
    -d "sort_order"="asc" \
    -d "tags"="qui" \
    -d "topics"="qui" \
    -d "doc_types"="qui" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/search",
    "method": "POST",
    "data": {
        "company_id": 7243,
        "keyword": "qui",
        "from_date": "2017-12-27",
        "to_date": "2017-12-27",
        "to": 7243,
        "from": 7243,
        "sort_type": "date",
        "sort_order": "asc",
        "tags": "qui",
        "topics": "qui",
        "doc_types": "qui"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/storage/search`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    keyword | string |  required  | 
    from_date | date |  optional  | Date format: `Y-m-d`
    to_date | date |  optional  | Date format: `Y-m-d`
    to | numeric |  optional  | 
    from | numeric |  optional  | 
    sort_type | string |  optional  | `date` or `size`
    sort_order | string |  optional  | `asc` or `desc`
    tags | array |  optional  | 
    topics | array |  optional  | 
    doc_types | array |  optional  | 

<!-- END_9dae2bd69077b8d7e5b69631e21fe6c4 -->

<!-- START_d6baef649e3e35db354ba670080136c9 -->
## Storage downloadLink

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/storage/share_link" \
-H "Accept: application/json" \
    -d "storage_object_id"="293" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/share_link",
    "method": "GET",
    "data": {
        "storage_object_id": 293
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/storage/share_link`

`HEAD /api/v1/storage/share_link`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    storage_object_id | numeric |  required  | 

<!-- END_d6baef649e3e35db354ba670080136c9 -->

<!-- START_3a32317a72c86b4ba02929e12a49a9be -->
## DirectDownload Storage

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/storage/direct_download/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/storage/direct_download/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/storage/direct_download/{id}`

`HEAD /api/v1/storage/direct_download/{id}`


<!-- END_3a32317a72c86b4ba02929e12a49a9be -->

<!-- START_fc3ed0b473c8c814d20e2cae25513bd2 -->
## Topic Add

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/topics/store" \
-H "Accept: application/json" \
    -d "company_id"="47163" \
    -d "storage_object_id"="47163" \
    -d "title"="perspiciatis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/store",
    "method": "POST",
    "data": {
        "company_id": 47163,
        "storage_object_id": 47163,
        "title": "perspiciatis"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/topics/store`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    storage_object_id | numeric |  required  | 
    title | string |  required  | 

<!-- END_fc3ed0b473c8c814d20e2cae25513bd2 -->

<!-- START_a0526fdc0c6a24ae626bdb43f0485cba -->
## Topic Update

> Example request:

```bash
curl -X PUT "http://local.faf.com//api/v1/topics/update" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/update",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /api/v1/topics/update`


<!-- END_a0526fdc0c6a24ae626bdb43f0485cba -->

<!-- START_c21d9adb23ef64a338f5846ede295e9e -->
## Topic get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/topics/listing" \
-H "Accept: application/json" \
    -d "company"="natus" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/listing",
    "method": "GET",
    "data": {
        "company": "natus"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/topics/listing`

`HEAD /api/v1/topics/listing`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company | string |  required  | 

<!-- END_c21d9adb23ef64a338f5846ede295e9e -->

<!-- START_1d23757ae5b9a5f31bdd6960ab623d2b -->
## Topic get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/topics/detail" \
-H "Accept: application/json" \
    -d "company"="maxime" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/detail",
    "method": "GET",
    "data": {
        "company": "maxime"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/topics/detail`

`HEAD /api/v1/topics/detail`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company | string |  required  | 

<!-- END_1d23757ae5b9a5f31bdd6960ab623d2b -->

<!-- START_b83588cc1ef460f00818545e735d914b -->
## Topic Search

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/topics/search" \
-H "Accept: application/json" \
    -d "company_id"="15557195" \
    -d "title"="repellendus" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/search",
    "method": "GET",
    "data": {
        "company_id": 15557195,
        "title": "repellendus"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/topics/search`

`HEAD /api/v1/topics/search`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    title | string |  required  | 

<!-- END_b83588cc1ef460f00818545e735d914b -->

<!-- START_301d0a1c24fc304e7fa9f731f877bf45 -->
## Topic delete

> Example request:

```bash
curl -X DELETE "http://local.faf.com//api/v1/topics/delete" \
-H "Accept: application/json" \
    -d "id"="3" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/delete",
    "method": "DELETE",
    "data": {
        "id": 3
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /api/v1/topics/delete`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | numeric |  required  | 

<!-- END_301d0a1c24fc304e7fa9f731f877bf45 -->

<!-- START_5d8d0f930a148e4b32b8ac950cc5c5f0 -->
## Topic get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/topics/autocomplete" \
-H "Accept: application/json" \
    -d "company"="nihil" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/topics/autocomplete",
    "method": "GET",
    "data": {
        "company": "nihil"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/topics/autocomplete`

`HEAD /api/v1/topics/autocomplete`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company | string |  required  | 

<!-- END_5d8d0f930a148e4b32b8ac950cc5c5f0 -->

<!-- START_075337d6d13beefdfc053ef5872f2673 -->
## Tag Add

> Example request:

```bash
curl -X POST "http://local.faf.com//api/v1/tags/store" \
-H "Accept: application/json" \
    -d "company_id"="723237639" \
    -d "storage_object_id"="723237639" \
    -d "title"="delectus" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/store",
    "method": "POST",
    "data": {
        "company_id": 723237639,
        "storage_object_id": 723237639,
        "title": "delectus"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/tags/store`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    storage_object_id | numeric |  required  | 
    title | string |  required  | 

<!-- END_075337d6d13beefdfc053ef5872f2673 -->

<!-- START_794181243cc415fda8462da6aa262068 -->
## Tag Update

> Example request:

```bash
curl -X PUT "http://local.faf.com//api/v1/tags/update" \
-H "Accept: application/json" \
    -d "company_id"="240359" \
    -d "id"="240359" \
    -d "status"="fuga" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/update",
    "method": "PUT",
    "data": {
        "company_id": 240359,
        "id": 240359,
        "status": "fuga"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /api/v1/tags/update`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    id | numeric |  required  | 
    status | string |  required  | 

<!-- END_794181243cc415fda8462da6aa262068 -->

<!-- START_84956622b73ed03d6c33e79434045d20 -->
## Tag get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/tags/detail" \
-H "Accept: application/json" \
    -d "company_id"="9790" \
    -d "storage_object_id"="9790" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/detail",
    "method": "GET",
    "data": {
        "company_id": 9790,
        "storage_object_id": 9790
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/tags/detail`

`HEAD /api/v1/tags/detail`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    storage_object_id | numeric |  required  | 

<!-- END_84956622b73ed03d6c33e79434045d20 -->

<!-- START_0c3d5e76a081d77c01e23b8887e42a82 -->
## Tag Search

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/tags/search" \
-H "Accept: application/json" \
    -d "company_id"="5" \
    -d "title"="dolor" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/search",
    "method": "GET",
    "data": {
        "company_id": 5,
        "title": "dolor"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/tags/search`

`HEAD /api/v1/tags/search`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    title | string |  required  | 

<!-- END_0c3d5e76a081d77c01e23b8887e42a82 -->

<!-- START_44aaae3ec30dc9e819c324a42ad00610 -->
## Tag get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/tags/listing" \
-H "Accept: application/json" \
    -d "company_id"="404910" \
    -d "storage_object_id"="404910" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/listing",
    "method": "GET",
    "data": {
        "company_id": 404910,
        "storage_object_id": 404910
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/tags/listing`

`HEAD /api/v1/tags/listing`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    storage_object_id | numeric |  required  | 

<!-- END_44aaae3ec30dc9e819c324a42ad00610 -->

<!-- START_5c40d8847daad6204633f2693f46bb15 -->
## Tag delete

> Example request:

```bash
curl -X DELETE "http://local.faf.com//api/v1/tags/delete" \
-H "Accept: application/json" \
    -d "id"="531" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/delete",
    "method": "DELETE",
    "data": {
        "id": 531
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /api/v1/tags/delete`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | numeric |  required  | 

<!-- END_5c40d8847daad6204633f2693f46bb15 -->

<!-- START_135bf8c09f99a74f0d9a28b39e86acc6 -->
## Tag get

> Example request:

```bash
curl -X GET "http://local.faf.com//api/v1/tags/autocomplete" \
-H "Accept: application/json" \
    -d "company_id"="1620" \
    -d "storage_object_id"="1620" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://local.faf.com//api/v1/tags/autocomplete",
    "method": "GET",
    "data": {
        "company_id": 1620,
        "storage_object_id": 1620
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/tags/autocomplete`

`HEAD /api/v1/tags/autocomplete`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    company_id | numeric |  required  | 
    storage_object_id | numeric |  required  | 

<!-- END_135bf8c09f99a74f0d9a28b39e86acc6 -->

