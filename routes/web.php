<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;

Route::get('/addes/{id}', function ($id) {

    $client = ClientBuilder::create()
        ->setHosts([ 'host' => env('ELASTIC_HOST') ])
        ->build();
    $params = [
        'index' => 'storage_object',
        'type' => 'string',
        'id' => $id,
        'body' => ['description' =>  'Sheet1\n\t\tPrepared by\t\t\t\t\tDate\n\t\tLast updated by\t\t\t\t\tDate\n\t\tNumber of test cases\t\t\t0\n\t\tPassed\t\t\t0\t\tExecuted\t0\n\t\tFailed\t\t\t0\t\t\t0\n\t\tNA\t\t\t0\t\t\t0\n\t\n\t\tTest scenarios for AIR API\n\t\n\tS.No\tDescription\tTRIPID\tXML Request\tXML Response\tActual Results\tStatus\tComments & Remarks\tAPI Parter\n\t1\tOne way booking - LCC (Spicejet, Indigo, Air India Express and GO air)\n\t2\tOne way booking - GDS (9W,IT,IT-Red, Jetlite, Jet Konnect and Air India)\n\t3\tRound trip booking - LCC (Spicejet, Indigo, Air India Express and GO air)\n\t4\tRound trip booking - GDS (9W,IT,IT-Red, Jetlite, Jet Konnect and Air India)\n\t5\tRound trip booking - Different combination of LCC (Spicejet, Indigo, Air India Express and GO air)\n\t6\tRound trip booking - Different combination of GDS (9W,IT,IT-Red, Jetlite, Jet Konnect and Air India)\n\t7\tRound trip booking with combination of LCC and GDS\n\t8\tRoundtrip booking with Special Fare RT fare (LCC and GDS)\n\t9\tOne way booking - Stop over flight\n\t10\tOne way booking - Via flight\n\t11\tOne way booking with mulitple pax (without infant)\n\t12\tOne way booking with mulitple pax (with child and infant)\n\t13\tRound trip booking with Multiple pax (GDS and LCC)\n\t14\tPartial Cancellation of flights\n\t15\tFull cancellation of flights\n\t16\tFare rules - request and response\n\t17\tRound trip - stop over flights\n\t18\tInternational flights -Oneway\n\t19\tInternational flights -Roundtrip\n\t20\tInternational flights - direct flight - one way\n\t21\tinternational flights - rountdtrip - direct\n\t22\tinternational flights - multicarrier - oneway\n\t23\tinternational flights - Multicarrier- rountdtrip\n\t24\tinternational flights - Via flights - One way\n\t25\tinternational flights - Via flights - Round trip\n\t26\tInternational flights - Stop over - One way\n\t27\tInternational flights - Stop over - Round trip\n\n\nSheet2\n\t\n\n\nSheet3\n\t\n\n\n'  ]
    ];
    $response = $client->index($params);
    return $response;
});

Route::get('/say/{id}', function ($id) {

    /*$client = new Client([
        // Base URI is used with relative requests
        // 'base_uri' => 'http://localhost:9998/tika',
        // You can set any number of default request options.
        //'timeout'  => 2.0,
    ]);
    $body =  fopen("https://s3.us-east-2.amazonaws.com/faf-storage-01/test/kfyCSL8e6GmLTE5OZQS8z2kJWDmSAa7BgLOJlgPE.xls","r");
    $r = $client->request('PUT','http://localhost:9998/tika',['body' => $body , 'header' => "Accept: text/html"]);
    echo   utf8_encode( $r->getBody());*/
    $sparams = [
        'index' => 'storage_object',
        'type' => 'string',
        'body' => [
            'query' => [
                'match' => [
                    'description' => $id
                ]
            ]
        ]
    ];
    $fparams = [
        'index' => 'storage_object',
        'type' => 'string',
         'id' => $id
    ];
    $client = ClientBuilder::create()
        ->setHosts([ 'host' => env('ELASTIC_HOST') ])
        ->build();
    $response = $client->get($fparams);
    print_r($response);


});
Route::get('/', function () {
//Route::get('/','HomeController@landing');
return view('auth.login');
});

Auth::routes();
/*
Route::prefix('api/v1/')->group(function () {
    Route::get('heartbeat', function () {
        return 'I am alive';
    });
});
*/
Route::get('download/{token}', 'StorageObjectController@downloadStorage');

Route::get('add/node', 'StorageObjectController@store');
Route::get('savebeforenode', 'StorageObjectController@saveBeforeNode');
Route::get('saveafternode', 'StorageObjectController@saveAfterNode');
Route::get('traverse_nodes/{id}', 'StorageObjectController@findNodes');

Route::get('admin/home', 'AdminController@index');
Route::get('admin/accessboth', 'EditorController@accessBoth');
Route::get('editor/home', 'EditorController@index');
Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::POST('admin', 'Admin\LoginController@login');

Route::POST('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');
Route::POST('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::POST('admin-password/reset', 'Admin\ResetPasswordController@rest');
Route::GET('admin-password/reset/{token}', 'Admin\ResetPasswordController@rest')->name('admin.password.reset');
Route::GET('VerifyEmailFirst', 'Auth\RegisterController@VerifyEmailFirst')->name('VerifyEmailFirst');
Route::GET('verify/{email}/{verifyToken}','Auth\RegisterController@EmailDone')->name('EmailDone');


//admin user
Route::GET('admin/users/', 'AdminController@users');
Route::POST('admin/users/', 'AdminController@users');
Route::GET('admin/addUser/', 'AdminController@userStoreForm');
Route::POST('admin/addUser/', 'AdminController@userStore');
Route::get('admin/user/delete/{id}', 'AdminController@userDelete');
Route::get('admin/user/edit/{id}', 'AdminController@userEdit');
Route::POST('admin/user/update/{id}', 'AdminController@userUpdate');
Route::get('admin/users/search', 'AdminController@searchUsers');


//Category

Route::GET('admin/categories/', 'AdminController@categories');
Route::POST('admin/categories/', 'AdminController@categories');
Route::GET('admin/addCategory/', 'AdminController@categoryStoreForm');
Route::POST('admin/addCategory/', 'AdminController@categoryStore');
Route::get('admin/category/delete/{id}', 'AdminController@categoryDelete');
Route::get('admin/category/edit/{id}', 'AdminController@categoryEdit');
Route::POST('admin/category/update', 'AdminController@categoryUpdate');
Route::get('admin/category/search', 'AdminController@searchCategories');



//Admin Post
Route::get('admin/posts','AdminController@posts');
// show new post form
Route::get('admin/addPost','AdminController@createPost');
// save new post
Route::post('admin/addPost','AdminController@storePost');
// edit post form
Route::get('admin/editPost/{slug}','AdminController@editPost');
// update post
Route::post('admin/updatePost','AdminController@updatePost');
// delete post
Route::get('admin/deletePost/{id}','AdminController@destroyPost');
// display user's all posts
Route::get('admin/my-all-posts','AdminController@user_posts_all');
// display user's drafts
Route::get('admin/my-drafts','AdminController@user_posts_draft');
// add comment
Route::post('admin/comment/add','CommentController@storePost');
// delete comment
Route::post('admin/comment/delete/{id}','CommentController@distroyPost');


//social
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

Route::group(['middleware' => ['auth','author']], function()
{
    // show new post form
    Route::get('new-post','PostController@create');

    // save new post
    Route::post('new-post','PostController@store');

    // edit post form
    Route::get('edit/{slug}','PostController@edit');

    // update post
    Route::post('update','PostController@update');

    // delete post
    Route::get('delete/{id}','PostController@destroy');

    // display user's all posts
    Route::get('my-all-posts','UserController@user_posts_all');

    // display user's drafts
    Route::get('my-drafts','UserController@user_posts_draft');

    // add comment
    Route::post('comment/add','CommentController@store');

    // delete comment
    Route::post('comment/delete/{id}','CommentController@distroy');


   // edit profile

    Route::post('/profile/update','UserController@update_profile');
    Route::get('/dashboard', 'UserController@activeUsers');
    Route::get('/home', 'UserController@activeUsers');
    Route::get('/activeUsers', 'UserController@activeUsers');
    Route::get('/license', 'CommentController@licenses');
    Route::get('/reports', 'CommentController@reports');
    Route::get('/search', 'CommentController@search');
    Route::get('/searchkeyword', 'CommentController@searchStorageObjects');
    Route::get('/license/cost', 'CommentController@licenseCost');
    Route::get('/download', 'CommentController@download');
    Route::get('/user/invoices', 'UserController@invoices');
    Route::get('/cancel_subscription_now','UserController@cancel_subscription_now');
    Route::get('/cancel_subscription','UserController@cancel_subscription');

    //Team
    Route::get('/team/add','TeamController@add');
    Route::post('/team/add','TeamController@store');
    Route::get('/team/manage/{team}','TeamController@manage');
    Route::post('/team/manage/{team}','TeamController@saveMember');


    //Uploads
    Route::get('/storage','StorageObjectController@index');
    Route::get('/storagelisting','StorageObjectController@listing');
    Route::post('/storage','StorageObjectController@store');
    Route::post('/storage/view/{id}','StorageObjectController@view');



});
Route::group(['middleware' => ['auth']], function() {
    Route::get('/profile/edit','UserController@edit_profile');
    Route::get('/user/invoices', 'UserController@invoices');
});

Route::group(['middleware' => ['auth','author']], function()
{
    Route::get('/subscription','UserController@subscription');
    Route::post('/make_subscription','UserController@make_subscription');
});

Route::group(['middleware' => ['auth','author','notsubscribed']], function()
{
    Route::get('/subscription','UserController@subscription');
    Route::post('/make_subscription','UserController@make_subscription');
});

//users profile
//Route::get('user/{id}','UserController@profile')->where('id', '[0-9]+');

// display list of posts
Route::get('user/{id}/posts','UserController@user_posts')->where('id', '[0-9]+');

// display single post
Route::get('/post/{slug}',['as' => 'post', 'uses' => 'PostController@show'])->where('slug', '[A-Za-z0-9-_]+');


//Route::get('storage/{folder}/{filename}', function ($folder,$filename)
/*Route::get('storage/app/{folder}/{filename}', function ($folder, $filename) {
    $path = storage_path('app/' . $folder . '/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});*/

Route::get('user/invoice/{invoice}', function (Request $request, $invoiceId) {
    return $request->user()->downloadInvoice($invoiceId, [
        'vendor'  => 'Process Meter NGO',
        'product' => 'Subscription',
    ]);
});

Route::get('/verifyTeamMember/{email}/{verifyToken}','CommonController@verifyTeamMember');
Route::post('/verifyTeamMember','CommonController@verifyTeamMemberPost');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
