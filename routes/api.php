<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Dingo\Api\Routing\Router;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'v1'], function(Router $api) {
        $api->get('heartbeat', 'FAF\\Http\\Controllers\\UserController@heartBeat');
        $api->post('throwback', function(Router $api){

           return getFullRequest();

        });

        $api->group(['prefix' => 'auth'], function (Router $api) {
            $api->post('signup', 'FAF\\Http\\Controllers\\UserController@register');
            $api->post('login', 'FAF\\Http\\Controllers\\UserController@login');
            $api->post('refresh', 'FAF\\Http\\Controllers\\UserController@refresh');
            $api->post('recovery', 'FAF\\Http\\Controllers\\UserController@sendResetEmail');
            $api->post('reset', 'FAF\\Http\\Controllers\\UserController@resetPassword');
            $api->group(['prefix' => 'verify'], function (Router $api) {
                $api->post('website', 'FAF\\Http\\Controllers\\UserController@verifyWebsite');
                $api->post('email', 'FAF\\Http\\Controllers\\UserController@verifyEmail');
                $api->post('company', 'FAF\\Http\\Controllers\\UserController@verifyCompany');
                $api->post('password_token', 'FAF\\Http\\Controllers\\UserController@verifyPasswordToken');
            });
        });
        $api->group(['prefix' => 'team', 'middleware' => 'auth:api'], function (Router $api) {
            $api->post('store', 'FAF\\Http\\Controllers\\TeamController@store');
            $api->get('listing', 'FAF\\Http\\Controllers\\TeamController@get');

        });
        $api->group(['prefix' => 'invite', 'middleware' => 'auth:api'], function (Router $api) {
            $api->post('member', 'FAF\\Http\\Controllers\\UserController@storeMembers');
            $api->post('listing', 'FAF\\Http\\Controllers\\UserController@invitesMembers');

        });
        $api->group(['prefix' => 'verify'], function (Router $api) {
            $api->post('invitation', 'FAF\\Http\\Controllers\\UserController@verifyInvitation');
        });

        $api->group(['prefix' => 'teammember', 'middleware' => 'auth:api'], function (Router $api) {
            $api->post('store', 'FAF\\Http\\Controllers\\TeamMemberController@store');
            $api->get('listing', 'FAF\\Http\\Controllers\\TeamMemberController@get');
        });
        $api->group(['prefix' => 'auth', 'middleware' => 'auth:api'], function (Router $api) {
            $api->post('logout', 'FAF\\Http\\Controllers\\UserController@logout');
            $api->get('user', 'FAF\\Http\\Controllers\\UserController@details');
        });
        $api->group(['prefix' => 'storage', 'middleware' => 'auth:api'], function (Router $api) {
            $api->post('store', 'FAF\\Http\\Controllers\\StorageObjectController@store');
            $api->post('folder', 'FAF\\Http\\Controllers\\StorageObjectController@storeDirectory');
            $api->get('listing', 'FAF\\Http\\Controllers\\StorageObjectController@listing');
            $api->get('listing/folder/{id?}', 'FAF\\Http\\Controllers\\StorageObjectController@folderListing');
            $api->get('share_with_me_storage', 'FAF\\Http\\Controllers\\StorageObjectController@sharedListing');
            $api->get('recent_storage', 'FAF\\Http\\Controllers\\StorageObjectController@listing');
            $api->get('detail', 'FAF\\Http\\Controllers\\StorageObjectController@get');
            $api->post('search', 'FAF\\Http\\Controllers\\StorageObjectController@search');
            $api->get('share_link', 'FAF\\Http\\Controllers\\StorageObjectController@downloadLink');
            $api->get('direct_download/{id}', 'FAF\\Http\\Controllers\\StorageObjectController@directDownload');
        });

        $api->group(['prefix' => 'topics', 'middleware' => 'auth:api'], function (Router $api) {
            $api->post('store', 'FAF\\Http\\Controllers\\TopicController@store');
            $api->put('update', 'FAF\\Http\\Controllers\\TopicController@update');
            $api->get('listing', 'FAF\\Http\\Controllers\\TopicController@get');
            $api->get('detail', 'FAF\\Http\\Controllers\\TopicController@get');
            $api->get('search', 'FAF\\Http\\Controllers\\TopicController@search');
            $api->delete('delete', 'FAF\\Http\\Controllers\\TopicController@delete');
            $api->get('autocomplete', 'FAF\\Http\\Controllers\\TopicController@get');
        });

        $api->group(['prefix' => 'tags', 'middleware' => [ 'auth:api' , 'AddUserAuthData:api' ]], function (Router $api) {
            $api->post('store', 'FAF\\Http\\Controllers\\TagController@store');
            $api->put('update', 'FAF\\Http\\Controllers\\TagController@update');
            $api->get('detail', 'FAF\\Http\\Controllers\\TagController@get');
            $api->get('search', 'FAF\\Http\\Controllers\\TagController@search');
            $api->get('listing', 'FAF\\Http\\Controllers\\TagController@get');
            $api->delete('delete', 'FAF\\Http\\Controllers\\TagController@delete');
            $api->get('autocomplete', 'FAF\\Http\\Controllers\\TagController@get');
        });

        $api->group(['prefix' => 'sources','middleware' => [ 'auth:api' ]], function (Router $api){

            //list available scources in faf platform
            $api->get('available', function (){
                return ['dropbox'];
            });

            //@todo send list of
            $api->get('list', function (){
                return ['todo' => [
                    'name',
                    'last_sync',
                    'size',
                    'share_with' => [], // team names and its ,
                    'active',
                    'type' =>  'cloud' //local disk or whatever
                    ]
                ];
            });
            /* source/{dropbox}/{
                user_id
                access_token
                scope  {}

            }
            */
            $api->post('list',function(){
                return ['todo'];
            });

            $api->put('list',function(){
                return ['todo'];
            });

            //@todo
            //
        });

       /* $api->post('create_company', 'FAF\\Http\\Controllers\\TagController@store');{
            //create bucket amazon
        }*/


    });
});

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
