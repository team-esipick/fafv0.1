@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
<style>

    .card img {
        width: 10%;
        height: auto;
    }
</style>
@endsection
@section('content')
    <div id="appFaf">
        <div>
            <div class="col-lg-12">
                    <div class="form-group is-empty col-lg-8 col-xs-8">
                        <input v-on:keyup.enter="searchStorageObjects" name="keyword" type="text" v-model="keyword" class="form-control" placeholder="Search" value="{{$search}}">
                        <span class="material-input"></span>
                        <span class="material-input"></span></div>
                    <button v-on:click="searchStorageObjects" class="btn btn-white btn-round btn-just-icon col-lg-2 col-xs-2">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>

                <div class="card" v-if="noRecord">
                    <div class="card-header" data-background-color="blue">
                        <h4 class="title"> No Record Found</h4>

                    </div>

                </div>
                <div class="card" id="loader" style="text-align: center;display: none">
                    <img src="/images/loader.gif">
                </div>

                <div class="card" v-if="storage.hits.hits.length != 0">
                    <div class="card-header" data-background-color="blue">
                        <h4 class="title">Search Files</h4>
                        <p class="category">Files and Directories</p>
                    </div>
                    <div  class="card-content table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                            <tr ><th>ID</th>
                                <th>Name</th>
                                <th>Tags</th>
                                <th>Topics</th>

                                <th>Action</th>
                            </tr></thead>
                            <tbody>
                            <tr v-if="noRecord">
                                <td colspan="3">No Record Found</td>
                            </tr>
                            {{--@foreach($storageObjects as $node)--}}
                            <tr v-for="node in storage.hits.hits">
                                <td>@{{node._id}}</td>
                                <td>@{{node._source.title}}</td>
                                <td>@{{node._source.tags}}</td>
                                <td>@{{node._source.topics}}</td>
                            <!--td>@{{node.description}}</td-->
                                <td class="td-actions text-right">
                                    <a  :href="'https://s3.us-east-2.amazonaws.com/faf-storage-01/test/'+ node._source.file" download type="button" rel="tooltip" title="" class="btn btn-primary btn-simple btn-xs" data-original-title="Download File" aria-describedby="tooltip773603">
                                        <i class="material-icons">file_download</i>
                                    </a>
                                </td>
                            </tr>
                            {{--  @endforeach--}}
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <pagination :data="storage" v-on:pagination-change-page="loadStorageObjects"></pagination>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <script>
        const app = new Vue({
            el: '#appFaf',
            components: {
                vueDropzone: DropZone,
                pagination: Pagination
            },
            data: {
                baseUrl:window.Laravel.baseUrl,
                message: 'Hello VUE!',
                dropzoneOptions: {
                    url: '/storage',
                    thumbnailWidth: 100,
                    thumbnailHeight: 100,
                    headers: { 'x-csrf-token':  window.Laravel.csrfToken }
                },
                storage: {
                    hits:{
                        hits:[]
                    }
                },
                storageObject: {},
                errors: [],
                keyword:'',
                noRecord:false
            },
            mounted: function(){

            },
            methods: {
                loadStorageObjects:function(page){
                    if (typeof page === 'undefined') {
                        page = 1;
                    }
                    axios.get(this.baseUrl+'/storagelisting?page='+ page)
                            .then(response => {
                        // JSON responses are automatically parsed.
                        this.storage = response.data;
                })
                    .catch(e => {
                        this.errors.push(e)
                })
                },

                searchStorageObjects:function(){
                    $('#loader').show();
                    this.storage= {
                        hits:{
                            hits:[]
                        }
                    };
                    this.noRecord=false;
                    axios.get(this.baseUrl+'/searchkeyword?keyword='+ this.keyword)
                            .then(response => {
                        $('#loader').hide();

                        // JSON responses are automatically parsed.
                        var response = response.data.response;

                        if(response.hits.hits.length != 0){
                            this.storage = response;

                        }else{
                            this.noRecord=true;
                        }


                })
                    .catch(e => {
                        this.errors.push(e)
                })
                }
            }

        });
    </script>

@endsection
