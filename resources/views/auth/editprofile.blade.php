@extends('layouts.app')

@section('title')

@endsection

@section('content')
	<div class="row">
		<div class="col-md-8">
			@if(session('status'))
				{{session('status')}}
			@endif
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">Edit Profile</h4>
					<p class="category">Edit your profile</p>
				</div>
				<div class="card-content">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('en/profile/update') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">Name</label>

							<div class="col-md-6">
								<input  id="name" type="text" class="form-control" name="name" value="{{ AUTH::user()->name }}" required autofocus>

								@if ($errors->has('name'))
									<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">Phone No</label>

							<div class="col-md-6">
								<input  id="phone" type="text" class="form-control" name="phone" value="{{ AUTH::user()->phone }}" required autofocus>

								@if ($errors->has('name'))
									<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Update
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12"  data-background-color="blue">
			<ul class="nav">
				{{--{{dd(Auth::user()->subscription('main'))}}--}}
				@if(Auth::user()->subscribed('main'))
					<li class="pull-left">


							<a href="javascript:void(0)">Currently subscribed to <b>{{Auth::user()->subscription('main')->stripe_plan}}</b> Plan.
							</a>

					</li>
					<li class="pull-right">
						<button v-on:click="clickHandler" class="btn btn-link btn-sm" href="{{ url('/cancel_subscription_now') }}"><i class="material-icons">cancel</i> Cancel Subscription
						</button>
					</li>
				@endif
			</ul>
		</div>
	</div>
@endsection
@section('scripts')
<script>

  Vue.use(VueHighcharts);
  const app = new Vue({
	el: '#app',
	data: {
	  message: 'Hello VUE!',
	},
	methods: {
	  clickHandler: function() {
        swal({
          title: "Are you sure?",
          text: "Do you really want to cancel your subscription!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
          confirmButtonColor :'#9c27b0',
          confirmButtonShow :true,
        }).then(function(willDelete) {
          if (willDelete) {
            window.location.href = "{{ url('/cancel_subscription_now') }}";

          } else {
            swal("Cancelled", "Operation cancelled!", "info");
      }
      })







	  }
	}
  });

</script>
@endsection