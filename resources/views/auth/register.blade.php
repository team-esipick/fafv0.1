@extends('layouts.preapp')
@section('title')
    Signup
@endsection
@section('content')
    <section class="signup-account padding-50" id="sign-up">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-4">
                    <div class="section-title text-center">
                        <h2 class="heading">Set up your Account</h2>
                        <p class="sub-heading">Please fill in the fields below</p>
                    </div>
                    <!-- title -->
                    <div class="padding-20 padding-btm-0">
                        <form action="{{ route('register') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Company name</label>
                                <input type="text" name="company-name" class="inputText">
                            </div>
                            <div class="form-group">
                                <label>First name</label>
                                <input id="name" type="text" class="inputText" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" name="last-name" class="inputText">
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Phone number <span class="color-light">Optional</span></label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <input type="tel" name="phone-code" class="inputText">
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <input type="tel" name="phone-number" class="inputText">
                                </div>
                            </div>
                            <hr class="seperator margin-40">
                            <div class="form-group full-inherit">
                                <label> <span class="pull-left">Claim your website</span> <span class="pull-right"> <a href="javascript:;" data-toggle="tooltip" data-placement="left" title="write your information"> <i class="fa fa-question-circle" aria-hidden="true"></i> </a> </span> </label>
                                <div class="clearfix"></div>
                                <input type="text" name="website" class="inputText">
                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input id="email" type="email" class="inputText" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input id="password" type="password" class="inputText" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Confirm password</label>
                                <input id="password-confirm" type="password" class="inputText" name="password_confirmation" required>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" name="submit" class="btn-signup btn-blue">Sign up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Register</h4>
                    <p class="category">Register with us</p>
                </div>
                <div class="card-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                                <div class="social-buttons" style="font-size: 27px;">
                                    <a href="auth/facebook"
                                       target="">
                                        <i class="fa fa-facebook-official"></i>
                                    </a>
                                    <a href="auth/twitter"
                                       target="">
                                        <i class="fa fa-twitter-square"></i>
                                    </a>
                                    <a href="auth/google"
                                       target="">
                                        <i class="fa fa-google-plus-square"></i>
                                    </a>
                                    <a href="auth/instagram"
                                       target="">
                                        <i class="fa fa-instagram"></i>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
