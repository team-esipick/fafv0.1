@extends('layouts.app')

@section('title')
{{ $user->name }}
@endsection

@section('content')
	<div class="row">
		<div class="col-md-8">
			@if(session('status'))
				{{session('status')}}
			@endif
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">Edit Profile</h4>
					<p class="category">edit your profile</p>
				</div>
				<div class="card-content">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">Name</label>

							<div class="col-md-6">
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

								@if ($errors->has('name'))
									<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">E-Mail Address</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

								@if ($errors->has('email'))
									<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Password</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
									<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
								<div class="social-buttons" style="font-size: 27px;">
									<a href="auth/facebook"
									   target="">
										<i class="fa fa-facebook-official"></i>
									</a>
									<a href="auth/twitter"
									   target="">
										<i class="fa fa-twitter-square"></i>
									</a>
									<a href="auth/google"
									   target="">
										<i class="fa fa-google-plus-square"></i>
									</a>
									<a href="auth/instagram"
									   target="">
										<i class="fa fa-instagram"></i>
									</a>

								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
