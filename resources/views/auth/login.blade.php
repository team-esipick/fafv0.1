@extends('layouts.preapp')
@section('title')
    Login
@endsection

@section('content')

    <section class="signup-account login-account" id="sign-up">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-4">
                    <div class="section-title text-center">
                        <h2 class="heading">Login</h2>
                        <p class="sub-heading">Dear User, login to access your account</p>
                    </div>
                    <!-- title -->
                    <div class="padding-20 padding-btm-0">
                        <form  role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Email address</label>
                                <input id="email" type="email" class="inputText" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input id="password" type="password" class="inputText" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                        </label>
                                    </div>
                            </div>
                            <div class="form-group">
                                <a href="{{ route('password.request') }}" class="link-blue">Forgot your password?</a>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" name="submit" class="btn-signup btn-blue">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Login</h4>
                    <p class="category">User login form</p>
                </div>
                <div class="card-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                                <div class="social-buttons" style="font-size: 27px;">
                                    <a href="auth/facebook"
                                       target="">
                                        <i class="fa fa-facebook-official"></i>
                                    </a>
                                    <a href="auth/twitter"
                                       target="">
                                        <i class="fa fa-twitter-square"></i>
                                    </a>
                                    <a href="auth/google"
                                       target="">
                                        <i class="fa fa-google-plus-square"></i>
                                    </a>
                                    <a href="auth/instagram"
                                       target="">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
