<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="active">
            <a href="{{ url('/dashboard') }}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        @if (!Auth::guest())
            <li>
                <a href="#">
                    <i class="material-icons">supervisor_account</i>
                    <p class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></p>
                    <ul class="dropdown-menu" role="menu">
                        <li><i class="material-icons">supervisor_account</i>
                            <p onclick="window.location = '{{ url('/activeUsers') }}';">Active Users</p></li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="{{ url('/search') }}">
                    <i class="material-icons">content_paste</i>
                    <p>Search</p>
                </a>
            </li>


            @if(belongsTeam())
            <li>
                <a href="{{ url('/team/add') }}">
                    <i class="material-icons">supervisor_account</i>
                    <p>Teams </p>
                </a>
            </li>
            @endif
            <li>
                <a href="{{ url('/storage') }}">
                    <i class="material-icons">file_upload</i>
                    <p>Upload </p>
                </a>
            </li>

        @endif
    </ul>
</div>