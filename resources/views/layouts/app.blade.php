<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Fecth-a-File &trade;</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('/tim/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('/tim/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet">
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('/tim/css/demo.css') }}" rel="stylesheet">
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    @yield('header')
    <script>
        // rename myToken as you like
        window.Laravel =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl' =>URL::to('/')
        ]); ?>
    </script>
    <script src="{{asset('/js/app.js')}}"></script>
</head>

<body>


<div class="wrapper" >
    <div class="sidebar" data-color="blue" data-image="../assets/img/sidebar-1.jpg">
        <!--
    Tip 1: You can change the color of the sidebar using: data-color="blue | blue | green | orange | red"
    Tip 2: you can also add an image using data-image tag
    -->
        <div class="logo">
            <a href="{{url('/home')}}" class="simple-text">
                <img src="images/logo.png" alt="logo">
            </a>
        </div>
        @include('layouts.sidebar')
    </div>
    <div class="main-panel">

        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> @yield('title') </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                            <a href="/en/storage" class="dropdown-toggle" data-toggle="dropdown">
                           <i class="material-icons">file_upload</i>
                                <p class="hidden-lg hidden-md">Uploads</p>
                            </a>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">dashboard</i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">shopping_cart</i>
                                <span class="notification">1</span>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li>

                            <a href="{{ route('logout') }}" class="dropdown-toggle" data-toggle="dropdown"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="material-icons">mood_bad</i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">notifications</i>
                                <span class="notification">5</span>
                                <p class="hidden-lg hidden-md">Notifications</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Mike John responded to your email</a>
                                </li>
                                <li>
                                    <a href="#">You have 5 new tasks</a>
                                </li>
                                <li>
                                    <a href="#">You're now friend with Andrew</a>
                                </li>
                                <li>
                                    <a href="#">Another Notification</a>
                                </li>
                                <li>
                                    <a href="#">Another One</a>
                                </li>

                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">person</i>
                                <p class="hidden-lg hidden-md">@if (Auth::guest()) Person  @else {{ Auth::user()->name }} @endif </p>
                            </a>
                                @if (Auth::guest())
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>

                                @else
                                        <ul class="dropdown-menu">
                                            @if (!Auth::guest())
                                                <li>
                                                    <a href="#">@if (!Auth::guest()) <b>{{ Auth::user()->name }}</b> @endif</a>
                                                </li>
                                            @endif
                                            @if (Auth::user()->can_post())

                                            @endif
                                            <li>
                                                <a href="{{ url('/en/profile/edit') }}">Edit Profile</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                    <div  id="app">
                        @yield('content')
                    </div>
            </div>
        </div>
        <footer class="footer">
            @include('layouts.footer_menu')
        </footer>
    </div>
</div>
</body>

<!--   Core JS Files   -->
{{--<script src="{{ asset('tim/js/jquery-3.2.1.min.js') }}"></script>--}}

<script src="{{ asset('tim/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('tim/js/material.min.js') }}"></script>
<script src="{{ asset('tim/js/arrive.min.js') }}"></script>
<script src="{{ asset('tim/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('tim/js/bootstrap-notify.js') }}"></script>
<script src="{{ asset('tim/js/material-dashboard.js?v=1.2.0') }}"></script>
{{--<script src="{{ asset('/js/vue/dist/vue.min.js') }}"></script>--}}
<script src="https://unpkg.com/highcharts@6.0.1/highcharts.js"></script>
<script src="https://unpkg.com/vue-highcharts@0.0.10/dist/vue-highcharts.min.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
{{--<script src="{{ asset('/js/vue/dist/vue.min.js') }}"></script>--}}





@yield('scripts')
<script>

  flash = {
    showNotification: function(type,msg) {
      $.notify({
        icon: "notifications",
        message: msg
      }, {
        type: type,
        timer: 4000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
    }
  }
</script>
@if (Session::has('message'))
  <script>
    flash.showNotification('{{Session::get('message.type')}}','{{Session::get('message.message')}}');
  </script>
@endif
@if ($errors->any())
    @foreach ( $errors->all() as $error )
        <script>
          flash.showNotification('danger','{{ $error }}');
        </script>
    @endforeach
@endif


</html>