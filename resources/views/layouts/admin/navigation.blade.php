<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/assets/admin/img/profile_small.jpg" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ ucwords(Auth::user()->name)}}</strong>
                             </span> <span class="text-muted text-xs block">{{ ucwords(Auth::user()->role[0]->name)}} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">

                       {{-- <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>--}}

                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('admin.logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="active">
                <a href="/admin/home"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> {{--<span class="fa arrow"></span>--}}</a>
               {{-- <ul class="nav nav-second-level">
                    <li ><a href="index.html">Dashboard v.1</a></li>
                    <li class="active"><a href="dashboard_2.html">Dashboard v.2</a></li>
                    <li ><a href="dashboard_3.html">Dashboard v.3</a></li>
                    <li ><a href="dashboard_4_1.html">Dashboard v.4</a></li>
                </ul>--}}
            </li>
          {{--  <li>
                <a href="/admin/users"><i class="fa fa-users"></i> <span class="nav-label">Users</span> --}}{{--<span class="label label-primary pull-right">NEW</span>--}}{{--</a>
            </li>--}}
            <li>
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/admin/addUser">Add New</a></li>
                    <li><a href="/admin/users">User listing</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">Categories </span> <span class="fa arrow"></span>{{--<span class="label label-warning pull-right">16/24</span>--}}</a>
                <ul class="nav nav-second-level">
                    <li><a href="/admin/addCategory">Add New</a></li>
                    <li><a href="/admin/categories">Category Listing</a></li>

                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">Posts </span> <span class="fa arrow"></span>{{--<span class="label label-warning pull-right">16/24</span>--}}</a>
                <ul class="nav nav-second-level">
                    <li><a href="/admin/addPost">Add New</a></li>
                    <li><a href="/admin/posts">Post Listing</a></li>

                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Miscellaneous</span>{{--<span class="label label-info pull-right">NEW</span>--}}</a>
                <ul class="nav nav-second-level">
                    <li><a href="toastr_notifications.html">SMTP Settings</a></li>
                    <li><a href="nestable_list.html">Site Logo</a></li>
                    <li><a href="agile_board.html">Change Password</a></li>
                </ul>
            </li>

        </ul>

    </div>
</nav>