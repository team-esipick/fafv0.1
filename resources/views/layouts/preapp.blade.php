<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/normalize.css') }}" rel="stylesheet" media="all">

    <script>
        // rename myToken as you like
        window.Laravel =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl' =>URL::to('/')
        ]); ?>
    </script>
    @yield('header')
</head>

<body>

<header class="header-login" id="header-login">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="logo text-left"> <a href="/" title="Fetch a file"> <img src="images/logo.png" alt="logo"> </a> </div>
            </div>
            <!-- col-6 -->
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="login-here text-right">
                    <a href="{{ route('login') }}" title="have account">New here?</a>&nbsp;
                    <a href="{{ route('register') }}" class="btn-signup-header btn-blue">Sign up</a>
                </div>
            </div>
            <!-- col-6 -->
        </div>
        <!-- row -->
    </div>
</header>
<!-- container-fluid -->
@yield('content')
<footer class="footer-bg"></footer>
</body>

<!--   Core JS Files   -->

<!-- Jquery 3.1.1 -->
<script src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
<!-- bootstrap 3.3.6 -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- jquery initializer -->
<script src="{{ asset('js/init.js') }}"></script>
{{--
<script src="{{ asset('tim/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('tim/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('tim/js/material.min.js') }}"></script>
<script src="{{ asset('tim/js/arrive.min.js') }}"></script>
<script src="{{ asset('tim/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('tim/js/bootstrap-notify.js') }}"></script>
<script src="{{ asset('tim/js/material-dashboard.js?v=1.2.0') }}"></script>
<script src="{{ asset('/js/vue/dist/vue.min.js') }}"></script>
<script src="https://unpkg.com/highcharts@6.0.1/highcharts.js"></script>
<script src="https://unpkg.com/vue-highcharts@0.0.10/dist/vue-highcharts.min.js"></script>--}}
<script src="{{asset('/js/app.js')}}"></script>
@yield('scripts')
{{--<script>
  flash = {
    showNotification: function(type,msg) {
      $.notify({
        icon: "notifications",
        message: msg
      }, {
        type: type,
        timer: 4000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
    }
  }
</script>--}}
@if (Session::has('status'))
    <script>
      flash.showNotification('success','{{ Session::get('status') }}');
    </script>
@endif
@if (Session::has('message'))
    <script>
      flash.showNotification('{{Session::get('message.type')}}','{{Session::get('message.message')}}');
    </script>
@endif
@if ($errors->any())
    @foreach ( $errors->all() as $error )
        <script>
          flash.showNotification('danger','{{ $error }}');
        </script>
    @endforeach
@endif

</html>