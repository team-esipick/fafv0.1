@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">{{$title}}</h4>
                <p class="category">User Invoices</p>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr><th>Invoice id</th>
                        <th>Date</th>
                        <th>Total</th>
                        <th>Link</th>
                    </tr></thead>
                    <tbody>
                    @if(empty($invoices))
                        <tr>
                            <td class="text-center" colspan="4">No Invoice Data</td>
                        </tr>
                    @endif
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->id}}</td>
                            <td>{{$invoice->date()->toFormattedDateString()}}</td>
                            <td>{{$invoice->total()}}</td>
                            <td class="text-primary"><a href="{{ url('/en/user/invoice/'.$invoice->id) }}" download>Download</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection