@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="col-md-4">
            <a class="btn btn-primary btn-block" disabled>Utilization<div class="ripple-container"></div></a>
        </div>
        <div class="col-md-4">
            <a href="{{url('en/license/cost')}}" class="btn btn-primary btn-block" >Cost<div class="ripple-container"></div></a>
        </div>
    </div>
</div>
<div class="card">
    <highcharts :options="top_utilize_licences" ></highcharts>
</div>
<div class="card">
    <highcharts :options="top_underutilized_licenses"></highcharts>
</div>
<div class="card">
    <highcharts :options="top_overutilized_licenses"></highcharts>
</div>
@endsection
@section('scripts')

<script>
  var json_data = JSON.parse('<?=json_encode($arr)?>');
  console.log(json_data);
  Vue.use(VueHighcharts);
  var top_utilize_licences = {
    chart: {
      type: 'bar',
      backgroundColor: '#f3f3f3'
    },
    title: {
      text: json_data.tul.name
    },
    xAxis: {
      categories: json_data.tul.cat
    },
    yAxis: {
      min: 0,
      title: {
        text: '',
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [ {
      name: 'Test 2',
      color:'#eb7d78',
      data: json_data.tul.data2
    },{
      name: 'Test 1',
      color:'#80e885',
      data: json_data.tul.data1
    }]
  }
  var top_underutilized_licenses = {
    chart: {
      type: 'bar',
      backgroundColor: '#f3f3f3'
    },
  title: {
      text: json_data.tuul.name
    },
    xAxis: {
      categories: json_data.tuul.cat
    },
    yAxis: {
      min: 0,
      title: {
        text: '',
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [ {
      name: 'Test 2',
      //borderColor:'#000',
      color:'#fff',
      data: json_data.tuul.data2
    },{
      //borderColor:'#80e885',
      name: 'Test 1',
      color:'#80e885',
      data: json_data.tuul.data1
    }]
  }
  var top_overutilized_licenses = {
    chart: {
      type: 'bar',
      backgroundColor: '#f3f3f3'
    },
  title: {
      text: json_data.tol.name
    },
    xAxis: {
      categories: json_data.tol.cat
    },
    yAxis: {
      min: 0,
      title: {
        text: '',
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [{
      //borderColor:'#80e885',
      name: 'Test 1',
      color:'#80e885',
      data: json_data.tol.data1
    },{
      name: 'Test 2',
      color:'#eb7d78',
      data: json_data.tol.data2
    }]
  }
  const app = new Vue({
    el: '#app',
    data: {
      message: 'Hello VUE!',
      //options: options,
      top_utilize_licences : top_utilize_licences,
      top_underutilized_licenses : top_underutilized_licenses,
      top_overutilized_licenses : top_overutilized_licenses
    },
    methods: {

    }
  });

</script>
@endsection