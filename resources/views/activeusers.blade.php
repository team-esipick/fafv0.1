@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')
<div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <p class="category">Active Users</p>
                    <h3 class="title">55%
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">person</i>
                        Total 55% Users are active now
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <p class="category">Inactive Users</p>
                    <h3 class="title">30%</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">person</i>
                        Total 30% Users are Inactive now
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <p class="category">Idle Users</p>
                    <h3 class="title">15%</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">person</i>
                        Total 15% Users are Inactive now
                    </div>
                </div>
            </div>
        </div>
    </div>
<highcharts :options="options" ref="highcharts"></highcharts>
@endsection
@section('scripts')
<script>
   var json_data = JSON.parse('<?=json_encode($arr)?>');
   Vue.use(VueHighcharts);

  var options = {
    title: {
      text: 'User Activity Chart'
    },
    xAxis: {
      categories: json_data.interval
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'middle'
    },

    yAxis: {
      //allowDecimals: false,
      title: {
        text: 'User Numbers'
      },
      //visible: false,
    },

    plotOptions: {
      series: {
        //pointStart: new date(),
        //pointInterval:  3600 * 1000 // one day
      },
      line: {
        marker: {
          enabled: false
        }
      }

    },

    series: [{
      name: 'Active Users',
      color: '#6fe368',
      lineWidth: 5,
      data: json_data.active_user
    }, {
      name: 'Idle Users',
      color: '#f78000',
      dashStyle: 'dash',
      data: json_data.inactive_user
    }, {
      lineWidth: 1,
      name: 'Inactive Users',
      color: '#ee372e',
      data: json_data.idle_user
    }]
  }
  const app = new Vue({
    el: '#app',
    data: {
      message: 'Hello VUE!',
      options: options
    },
    methods: {
    }
  });

</script>
@endsection