@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')


    <div class="col-lg-8 col-md-6">
            <div class="card">
                <div class="card-header" data-background-color="orange">
                    <h4 class="title">Teams List</h4>
                    <p class="category">List of teams</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                        <tr><th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr></thead>
                        <tbody>
                        @foreach($teams as $team)
                        <tr>
                            <td>{{$team->id}}</td>
                            <td>{{$team->title}}</td>
                            <td>{{$team->description}}</td>
                            <td class="td-actions text-right">
                                <a target="_blank" href="{{url('/en/team/manage/'.$team->id)}}" type="button" rel="tooltip" title="" class="btn btn-primary btn-simple btn-xs" data-original-title="View Team" aria-describedby="tooltip773603">
                                    <i class="material-icons">visibility</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{ $teams->links() }}
                    </div>

                </div>
            </div>
        </div>
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Add Team</h4>
                <p class="category">You can add new team.</p>
            </div>
            <div class="card-content">
                <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" required>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">File upload</label>

                        <div class="col-md-6">
                            <input id="file" type="file" class="form-control" name="file" required>

                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-4 control-label">Description</label>

                        <div class="col-md-6">
                            <textarea id="description" class="form-control" name="description" required></textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Add Team</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection