@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Accessible for Both Admin and Edito</div>

                <div class="panel-body">
                    Welcome both of you!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
