@extends("layouts.admin.master")
@section('title', "Adminpanel | Posts")
@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10">
			<h2>Posts</h2>
			<ol class="breadcrumb">
				<li>
					<a href="/admin">home</a>
				</li>
				<li>
					<a>Posts</a>
				</li>
				<li class="active">
					<strong>Listing</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<script type="text/javascript" src="{{ asset('/assets/admin/js/plugins/tinymce/tinymce.min.js') }}"></script>
			<script type="text/javascript">
                tinymce.init({
                    selector : "textarea",
                    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
                    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                });
			</script>
			<div class="col-lg-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<div class="ibox-tools">
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="#">Config option 1</a>
								</li>
								<li><a href="#">Config option 2</a>
								</li>
							</ul>
							<a class="close-link">
								<i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">

						<form action="/admin/addPost" method="post" enctype="multipart/form-data" >
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<input required="required" value="{{ old('title') }}" placeholder="Enter title here" type="text" name = "title"class="form-control" />
							</div>
							<div class="form-group">
								<textarea name='body'class="form-control">{{ old('body') }}</textarea>
							</div>

							<div class="form-group">
								<label class="col-lg-2 control-label">Thumbnail</label>

								<input required="required"  type="file" name = "thumbnail"class="form-control" />

							</div>

							<input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
							<input type="submit" name='save' class="btn btn-default" value = "Save Draft" />
						</form>

					</div>
				</div>
			</div>

		</div>
	</div>


	{{--		<ul>
                @foreach ($categories as $parent)
                    <li>{{ $parent->name }}
                        @if ($parent->children->count())
                            <ul>
                                @foreach ($parent->children as $child)
                                    <li>{{ $child->name }}</li>
                                    <ul>
                                        @foreach ($child->children as $child1)
                                            <li>{{ $child1->name }}</li>

                                        @endforeach
                                    </ul>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>--}}
@endsection


{{--
@extends('layouts.app')

@section('title')
Add New Post
@endsection

@section('content')

<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
	tinymce.init({
		selector : "textarea",
		plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
		toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
	}); 
</script>

<form action="/new-post" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-group">
		<input required="required" value="{{ old('title') }}" placeholder="Enter title here" type="text" name = "title"class="form-control" />
	</div>
	<div class="form-group">
		<textarea name='body'class="form-control">{{ old('body') }}</textarea>
	</div>
	<input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
	<input type="submit" name='save' class="btn btn-default" value = "Save Draft" />
</form>
@endsection
--}}
