@extends("layouts.admin.master")
@section('title', "Adminpanel | Posts")
@section('content')

		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Posts</h2>
				<ol class="breadcrumb">
					<li>
						<a href="/admin">home</a>
					</li>
					<li>
						<a>Posts</a>
					</li>
					<li class="active">
						<strong>Listing</strong>
					</li>
				</ol>
			</div>
			<div class="col-lg-2">

			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<div class="tools hidden-xs" style="    margin-top: 12px;">
								<div class="col-md-4 pull-right">
									<form name="search" class="form-inline hidden-xs" action="/admin/post/search" method="get">
										<div class="input-group input-medium">
											<input type="text" name="keyword" class="form-control" placeholder="Search Categories" required>
											<span class="input-group-btn">
														<button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
														</span>
										</div>
									</form>
								</div>

								<a href="/admin/posts" class="btn btn-success">All</a>


							</div>
						</div>
						<div class="ibox-content">

							<table class="table table-striped table-bordered table-hover dataTables-example" >
								<thead>
								<tr>
									<th>id</th>
									<th>Thumbnail</th>
									<th>Name</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($posts as $post)
									<tr class="gradeX">
										<td>{{$post->id}}</td>
										<td><img style="width:70px;height: 70px" src="/storage/app/thumnails/{{$post->image}}"></td>
										<td>{{$post->title}}</td>
										<td class="center">@if($post->active == 0) Draft @else Publish @endif</td>
										<td class="center"> <a href="/admin/editPost/{{$post->slug}}">
												<i class="fa fa-edit"></i>
											</a>  <i class="fa fa-search"></i><a href="/admin/deletePost/{{$post->id}}" onclick="return confirm('you want to delete?')?true:false;">
												<i class="fa fa-trash"></i>
											</a></td>
									</tr>
								@endforeach
								</tfoot>
							</table>
							{{ $posts->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>


{{--		<ul>
			@foreach ($categories as $parent)
				<li>{{ $parent->name }}
					@if ($parent->children->count())
						<ul>
							@foreach ($parent->children as $child)
								<li>{{ $child->name }}</li>
								<ul>
									@foreach ($child->children as $child1)
										<li>{{ $child1->name }}</li>

									@endforeach
								</ul>
							@endforeach
						</ul>
					@endif
				</li>
			@endforeach
		</ul>--}}
@endsection