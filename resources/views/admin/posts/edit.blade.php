@extends("layouts.admin.master")
@section('title', "Adminpanel | Posts")
@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10">
			<h2>Edit Post</h2>
			<ol class="breadcrumb">
				<li>
					<a href="/admin">home</a>
				</li>
				<li>
					<a href="/admin/posts">Posts</a>
				</li>
				<li class="active">
					<strong>New Post</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<script type="text/javascript" src="{{ asset('/assets/admin/js/plugins/tinymce/tinymce.min.js') }}"></script>
			<script type="text/javascript">
                tinymce.init({
                    selector : "textarea",
                    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
                    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                });
			</script>
			<div class="col-lg-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<div class="ibox-tools">
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="#">Config option 1</a>
								</li>
								<li><a href="#">Config option 2</a>
								</li>
							</ul>
							<a class="close-link">
								<i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">

						<form method="post" action='{{ url("/admin/updatePost") }}' enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="post_id" value="{{ $post->id }}{{ old('post_id') }}">
							<div class="form-group">
								<input required="required" placeholder="Enter title here" type="text" name="title"
									   class="form-control"
									   value="@if(!old('title')){{$post->title}}@endif{{ old('title') }}"/>
							</div>
							<div class="form-group">
								<textarea name='body' class="form-control">
									@if(!old('body'))
										{!! $post->body !!}
									@endif
									{!! old('body') !!}
								</textarea>
							</div>
							<div class="form-group">
								<img style="width:100px;height: 100px" src="/storage/app/thumnails/{{$post->image}}">

								<input   type="file" name = "thumbnail"class="form-control" />

							</div>
							@if($post->active == '1')
								<input type="submit" name='publish' class="btn btn-success" value="Update"/>
							@else
								<input type="submit" name='publish' class="btn btn-success" value="Publish"/>
							@endif
							<input type="submit" name='save' class="btn btn-default" value="Save As Draft"/>
							<a href="{{  url('/admin/deletePost/'.$post->id.'?_token='.csrf_token()) }}"
							   class="btn btn-danger">Delete</a>
						</form>

					</div>
				</div>
			</div>

		</div>
	</div>
@endsection



{{--
@extends('layouts.app')

@section('title')
Edit Post
@endsection

@section('content')
<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
	tinymce.init({
		selector : "textarea",
		plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
		toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
	}); 
</script>

<form method="post" action='{{ url("/update") }}'>
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="post_id" value="{{ $post->id }}{{ old('post_id') }}">
	<div class="form-group">
		<input required="required" placeholder="Enter title here" type="text" name = "title" class="form-control" value="@if(!old('title')){{$post->title}}@endif{{ old('title') }}"/>
	</div>
	<div class="form-group">
		<textarea name='body'class="form-control">
			@if(!old('body'))
			{!! $post->body !!}
			@endif
			{!! old('body') !!}
		</textarea>
	</div>
	@if($post->active == '1')
	<input type="submit" name='publish' class="btn btn-success" value = "Update"/>
	@else
	<input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
	@endif
	<input type="submit" name='save' class="btn btn-default" value = "Save As Draft" />
	<a href="{{  url('delete/'.$post->id.'?_token='.csrf_token()) }}" class="btn btn-danger">Delete</a>
</form>
@endsection
--}}
