@extends("layouts.admin.master")
@section('title', "Adminpanel | Add User")
@section('content')

		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Users</h2>
				<ol class="breadcrumb">
					<li>
						<a href="/admin">home</a>
					</li>
					<li>
						<a>Users</a>
					</li>
					<li class="active">
						<strong>Listing</strong>
					</li>
				</ol>
			</div>
			<div class="col-lg-2">

			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<div class="tools hidden-xs" style="    margin-top: 12px;">
								<div class="col-md-4 pull-right">
									<form name="search" class="form-inline hidden-xs" action="/admin/users/search" method="get">
										<div class="input-group input-medium">
											<input type="text" name="keyword" class="form-control" placeholder="Search Users" required>
											<span class="input-group-btn">
														<button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
														</span>
										</div>
									</form>
								</div>

								<a href="/admin/users" class="btn btn-success">All</a>


							</div>
						</div>
						<div class="ibox-content">

							<table class="table table-striped table-bordered table-hover dataTables-example" >
								<thead>
								<tr>
									<th>id</th>
									<th>User</th>
									<th>Email</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($users as $user)
									<tr class="gradeX">
										<td>{{$user->id}}</td>
										<td>{{$user->name}}</td>
										<td>{{$user->email}}</td>
										<td class="center">@if($user->status == 1) Active @else Inactive @endif</td>
										<td class="center"> <a href="/admin/user/edit/{{$user->id}}">
												<i class="fa fa-edit"></i>
											</a>  <i class="fa fa-search"></i><a href="/admin/user/delete/{{$user->id}}" onclick="return confirm('you want to delete?')?true:false;">
												<i class="fa fa-trash"></i>
											</a></td>
									</tr>
								@endforeach
								</tfoot>
							</table>
							{{ $users->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection