@extends("layouts.admin.master")
@section('title', "Adminpanel | Edit User")
@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10">
			<h2>New User</h2>
			<ol class="breadcrumb">
				<li>
					<a href="index.html">Home</a>
				</li>
				<li>
					<a>User</a>
				</li>
				<li class="active">
					<strong>Edit User</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2"></div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>User Information</h5>
					</div>
					<div class="ibox-content">
						<form action="/admin/user/update/{{ $user->id }}" method="POST" enctype="multipart/form-data" role="form">
							<div class="row">
								<div class="col-sm-6 b-r">
									@if(Session::has('message'))
										<center><p>{{ Session::get('message') }}</p></center>
									@endif
									<div class="form-group">
										<label>Name</label>
										<input type="text" placeholder="Enter Name" class="form-control" name="fullName"
											   required  value="{{ $user->name }}">
									</div>
									<div class="form-group">
										<label>Password</label>
										<input type="password" placeholder="Enter Password" class="form-control"
											   name="password" >
									</div>
									<div class="form-group">
										<label>Role</label>
										<select class="form-control m-b" name="role">
											<option value="author">Author</option>
											<option value="buyer">Buyer</option>

										</select>
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="email" placeholder="Enter Email" class="form-control"
											   name="email" required value="{{ $user->email }}">
									</div>

									<div class="checkbox i-checks">
										<label>
											<input name="status" type="checkbox" value="1" @if($user->status == 1)checked @endif> <i></i> Active
										</label>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
										<strong>Update</strong>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection