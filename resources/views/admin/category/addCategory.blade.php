@extends("layouts.admin.master")
@section('title', "Adminpanel | Add User")
@section('content')

		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Add User</h2>
				<ol class="breadcrumb">
					<li>
						<a href="/admin/home">Home</a>
					</li>
					<li>
						<a href="/admin/categories">Categories</a>
					</li>
					<li class="active">
						<strong>Add Category</strong>
					</li>
				</ol>
			</div>
			<div class="col-lg-2">

			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-lg-7">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									<i class="fa fa-wrench"></i>
								</a>
								<ul class="dropdown-menu dropdown-user">
									<li><a href="#">Config option 1</a>
									</li>
									<li><a href="#">Config option 2</a>
									</li>
								</ul>
								<a class="close-link">
									<i class="fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<form method="post" action="/admin/addCategory" class="form-horizontal">
								{{ csrf_field() }}

								@if(Session::has('message'))
									<center><p>{{ Session::get('message') }}</p></center>
								@endif
								<div class="form-group"><label class="col-lg-2 control-label">Name</label>
									<div class="col-lg-10">
										<input type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Description</label>
									<div class="col-lg-10">
										<textarea class="form-control" name="description" required>{{ old('description') }}</textarea>

									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Parent</label>
									<div class="col-lg-10">
										<select class="form-control" name="parent_id" value="{{ old('parent_id') }}">
											<option value="NULL">Self Parent</option>
											@foreach($categories as $category)
												<option value="{{$category->id}}" >{{$category->name}}</option>
											@endforeach
										</select>
								</div>
									<br>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<div class="checkbox i-checks"><label>
												<input type="checkbox"  name="active"><i></i> Active </label></div>
									</div>

								</div>


								</div>
								<div class="form-group pull-right">
									<div class="col-lg-offset-2 col-lg-10 ">
										<button class="btn btn-sm btn-success" type="submit">Save</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>

@endsection