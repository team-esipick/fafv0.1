@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')

        <div class="row">
            <div class="card">
                <div class="card-content text-center">
                    <div class="col-xs-12 col-md-3 col-lg-offset-1">
                        <div class="card panel panel-primary">
                            <div class="card-header" data-background-color="blue">
                                <h3 class="panel-title">
                                    Basic</h3>
                            </div>
                            <div class="card-content">
                                <div class="the-price">
                                    <h2>
                                        $100<span class="subscript">/mo</span></h2>
                                    <small>15 Day FREE trial</small>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                            1 Account
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            1 Project
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            100K API Access
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            100MB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <form action="/en/make_subscription" method="POST">
                                    {{ csrf_field() }}
                                    <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="pk_test_iMdMbUJBUFh61cyh1iaNDRn3"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-name="Basic"
                                            data-description="Basic Plan"
                                            data-panel-label="Subscribe"
                                            data-label="Basic Plan"
                                            data-amount="10000"
                                            data-email="{{Auth::user()->email}}"
                                    >
                                    </script>
                                    <input type="hidden" name="plan" value="basic">
                                    <button type="submit" class="btn btn-block btn-primary pull-right">Basic Plan<div class="ripple-container"></div></button>
                                </form>
                                <p>15 day FREE trial</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="card panel panel-primary">
                            <div class="card-header" data-background-color="orange">
                                <h3 class="panel-title">
                                    Standard</h3>
                            </div>
                            <div class="card-content">
                                <div class="the-price">
                                    <h2>
                                        $200<span class="subscript">/mo</span></h2>
                                    <small>15 Day FREE trial</small>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                            5 Account
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            5 Project
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            1000K API Access
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            1GB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <form action="/en/make_subscription" method="POST">
                                    {{ csrf_field() }}
                                    <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="pk_test_iMdMbUJBUFh61cyh1iaNDRn3"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-name="Standard"
                                            data-description="Standard Plan"
                                            data-panel-label="Subscribe"
                                            data-label="Standard Plan"
                                            data-amount="20000"
                                            data-email="{{Auth::user()->email}}"
                                    >
                                    </script>
                                    <input type="hidden" name="plan" value="standard">
                                    <button type="submit" class="btn btn-block btn-warning pull-right">Standard Plan<div class="ripple-container"></div></button>
                                </form>
                                <p>15 day FREE trial</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="card panel panel-primary">
                            <div class="card-header" data-background-color="green">
                                <h3 class="panel-title">
                                    Premium</h3>
                            </div>
                            <div class="card-content">
                                <div class="the-price">
                                    <h2>
                                        $100<span class="subscript">/mo</span></h2>
                                    <small>15 Day FREE trial</small>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                            Unlimited Account
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            10 Project
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            1000K API Access
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            10GB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <form action="/en/make_subscription" method="POST">
                                    {{ csrf_field() }}
                                    <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="pk_test_iMdMbUJBUFh61cyh1iaNDRn3"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"



                                            data-id="premium"
                                            data-name="Premium Plan"
                                            data-description="Premium Plan"
                                            data-panel-label="Subscribe"
                                            data-label="Premium Plan"
                                            data-amount="30000"
                                            data-email="{{Auth::user()->email}}"
                                    >
                                    </script>
                                    <input type="hidden" name="plan" value="premium">
                                    <button type="submit" class="btn btn-block btn-success pull-right">Premium Plan<div class="ripple-container"></div></button>
                                </form>
                                <p>15 day FREE trial</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <style>
        .stripe-button-el{
            display: none;
        }
    </style>


@endsection