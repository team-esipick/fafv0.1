<style>
    .logo{
        width: 100px!important;
        height: 40px!important;
    }
    .btn-verify-p{
        margin: 0 auto;
        text-align: -webkit-center;
        text-align: center;
        margin-bottom: 20px;
    }
    .btn-verify-p-c{
        margin: 0 auto;
        text-align: -webkit-center;
        text-align: center;
    }
    .btn-verify{
        background-color: #0366d6;
        color: #FFFFFF;
        border: none;
        border-radius: 3px;
        position: relative;
        padding: 12px 30px;
        margin: 10px 1px;
        font-size: 12px;
        font-weight: 400;
        text-transform: uppercase;
        letter-spacing: 0;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
    }
.mail-blue-btn{
    font-family: "avenir" , "helvetica" , sans-serif;
    box-sizing: border-box;
    border-radius: 3px;
    box-shadow: 0 2px 3px rgba(0 , 0 , 0 , 0.16);
    color: #fff;
    display: inline-block;
    text-decoration: none;
    background-color: #3097d1;
    border-top: 10px solid #3097d1;
    border-right: 18px solid #3097d1;
    border-bottom: 10px solid #3097d1;
    border-left: 18px solid #3097d1;
}
    .footer-mailtext{
        font-family: "avenir" , "helvetica" , sans-serif;
        box-sizing: border-box;
        color: #74787e;
        line-height: 1.5em;
        margin-top: 0;
        text-align: left;
        font-size: 12px;
    }
</style>
@extends('beautymail::templates.minty')

@section('content')
    @include('beautymail::templates.minty.contentStart')
    <tr>
        <td class="title">
            Hello {{ucfirst($data['user']->first_name)}},
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            {!! $data['message'] !!}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Link', 'link' => $data['link']])
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">

        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td class="paragraph">
            Kind Regards,
            <br>FAF Support Team
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td class="paragraph">

        </td>
    </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop
