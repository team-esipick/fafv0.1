@extends('layouts.app')
@section('header')
@endsection
@section('title')
    {{$title}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title"><i class="material-icons">assignment_returned</i></h4>
                    <p class="category">Download</p>
                </div>
                <div class="card-content">
                    <div class="col-lg-10 col-lg-offset-1">
                        <p >
                            <b>Download App</b> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum.
                        <p>
                            <div class="col-lg-6">
                        <p><a href="#" class="btn btn-app-store"><i class="fa fa-apple"></i> <span class="small">Download on the</span> <span class="big">App Store</span></a></p>
                    </div>
                    <div class="col-lg-6">
                        <p><a href="#" class="btn btn-app-andriod"><i class="fa fa-android"></i> <span class="small">Download on the</span> <span class="big">Play Store</span></a></p>
                    </div>
                    <p>
                        <br>
                        Material Dashboard comes with 5 color filter choices for both the sidebar and the card headers (blue, green, orange, red and blue) and an option to have a background image on the sidebar.<br>
                        <br>
                        Downlaod application uses a framework built by our friend <a target="_blank" rel="nofollow" href="http://fezvrasta.github.io/bootstrap-ss-design/">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.<br>
                            <br>
                            Special thanks go to:<br>
                            <ul><li><a target="_blank" rel="nofollow" href="https://github.com/mouse0270">Robert McIntosh</a> for the notification system.</li><li><a target="_blank" rel="nofollow" href="https://gionkunz.github.io/chartist-js/">Chartist</a> for the wonderful charts.</li></ul>
                            We are very excited to share this dashboard with you and we look forward to hearing your feedback!<br><br>You can find the <a target="_blank" rel="nofollow" href="https://github.com/creativetimofficial/material-dashboard">Github Repo here</a>.<br><br>
                            <span>
                        <br>
                    </span></p>


                </div>
            </div>
        </div>
    </div>
@endsection