@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')

    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Generate Report</h4>
                <p class="category">Please select the details of the report to generate.</p>
            </div>
            <div class="card-content">
                <form>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">Type</label>
                                <select class="form-control">
                                    <option value=""></option>
                                    <option value="id-1">License Utilization</option>
                                    <option value="id-2">License Cost</option>
                                    <option value="id4622725">Subscription Utilization</option>
                                    <option value="id96722726">Subscription Cost</option>
                                    <option value="id18922727">User Sessions</option>
                                </select>
                                <span class="material-input"></span>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">User</label>
                                <select class="form-control">
                                    <option value=""></option>
                                    <option value="id-1">All</option>
                                    <option value="id-2">Project 1</option>
                                    <option value="id4622725">Project 2</option>
                                    <option value="id96722726">Project 3</option>
                                    <option value="id18922727">Project 4</option>
                                </select>
                                <span class="material-input"></span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">Application Typw</label>
                                <select class="form-control">
                                    <option value=""></option>
                                    <option value="id-1">Word Processing</option>
                                    <option value="id-2">Database</option>
                                    <option value="id4622725">Spreadsheet</option>
                                    <option value="id96722726">Multimedia</option>
                                    <option value="id18922727">Presentation</option>
                                    <option value="id18922727">Educational</option>
                                    <option value="id18922727">Content Access</option>
                                    <option value="id18922727">Application Suites</option>
                                    <option value="id18922727">Software for Engineering and Product Development</option>
                                </select>
                                <span class="material-input"></span>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">Application Type</label>
                                <select class="form-control">
                                    <option value=""></option>
                                    <option value="id-1">Application 1</option>
                                    <option value="id-2">Application 2</option>
                                    <option value="id-2">Application 3</option>
                                    <option value="id-2">Application 4</option>
                                    <option value="id-2">Application 5  </option>
                                </select>
                                <span class="material-input"></span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">Start Date</label>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group label-floating is-empty">
                                <input type="date" value="12-NOV-2017" class="form-control">
                                <span class="material-input"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">End Date</label>
                            </div>
                        </div>

                        <div class="col-md-10">
                            <div class="form-group label-floating is-empty">
                                <input type="date" class="form-control">
                                <span class="material-input"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">Interval</label>
                                <select class="form-control">
                                    <option value=""></option>
                                    <option value="id-1">Option 1</option>
                                    <option value="id-2">Option 2</option>
                                    <option value="id-2">Option 5  </option>
                                </select>
                                <span class="material-input"></span>

                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Generate Report</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection