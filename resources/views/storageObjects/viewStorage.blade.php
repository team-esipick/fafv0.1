@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-6">
            <div class="card">
                <div class="card-header" data-background-color="orange">
                    <h4 class="title">{{$team->title}}</h4>
                    <p class="category">{{$team->description}}</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                        <tr><th>ID</th>
                            <th>First Name</th>
                            <th>email</th>
                            <th>Status</th>
                        </tr></thead>
                        <tbody>

                        @foreach($members as $member)
                            <tr>
                                <td>{{$member->id}}</td>
                                <td>{{$member->user->name}}</td>
                                <td>{{$member->user->email}}</td>
                                <td>{{$member->status}}</td>
                            </tr>
                        @endforeach
                        @if(empty($members))
                            <tr>
                                <td colspan="5" class="text-center">No Member</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    @if(!empty($members))
                    <div class="pull-right">
                        {{$members->links()}}
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Add Team Member</h4>
                    <p class="category">You can add new team members from here.</p>
                </div>
                <div class="card-content">
                    <form class="form-horizontal" role="form" method="POST" action="">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Team</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection