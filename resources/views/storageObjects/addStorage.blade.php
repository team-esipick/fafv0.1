@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}

@endsection
@section('content')
    <div id="appFaf">
    <div class="col-lg-12">
            <div class="card">
                <div class="card-header" data-background-color="">
                    <h4 class="title">Uploaded Files</h4>
                    <p class="category">Files and Directories</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                        <tr><th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr></thead>
                        <tbody>
                        {{--@foreach($storageObjects as $node)--}}
                        <tr v-for="node in storage.data">
                            <td>@{{node.id}}</td>
                            <td>@{{node.slug}}</td>
                            <!--td>@{{node.description}}</td-->
                            <td class="td-actions text-right">
                                <a  :href="'https://s3.us-east-2.amazonaws.com/faf-storage-01/test/'+ node.title" download type="button" rel="tooltip" title="" class="btn btn-primary btn-simple btn-xs" data-original-title="Download File" aria-describedby="tooltip773603">
                                    <i class="material-icons">file_download</i>
                                </a>
                            </td>
                        </tr>
                      {{--  @endforeach--}}
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <pagination :data="storage" v-on:pagination-change-page="loadStorageObjects"></pagination>
                    </div>

                </div>
            </div>
        </div>

    <div class="col-lg-12" >
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Upload Files @{{message}}</h4>
                <p class="category">You can upload files.</p>
            </div>
            <div class="card-content">
                <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                   {{-- <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" required>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>--}}

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">

                       {{-- <vue-dropzone ref="myVueDropzone" id="dropzone" :options="dropzoneOptions" v-on:vdropzone-success="loadStorageObjects">--}}
                        <vue-dropzone v-on:vdropzone-error="dropZoneError"ref="myVueDropzone" id="dropzone" :options="dropzoneOptions" v-on:vdropzone-success="loadStorageObjects">
                        <label for="title" class="col-md-4 control-label">File upload</label>

                        <div class="col-md-6">


                            <input id="file" type="file" class="form-control" name="file" required>

                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                   {{-- <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-4 control-label">Description</label>

                        <div class="col-md-6">
                            <textarea id="description" class="form-control" name="description" required></textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>--}}
                  {{--  <button type="submit" class="btn btn-primary pull-right">Add Team</button>--}}
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    </div>

    <script>
        const app = new Vue({
            el: '#appFaf',
            components: {
                vueDropzone: DropZone,
                pagination: Pagination
            },
            data: {
                baseUrl:window.Laravel.baseUrl,
                message: 'Hello VUE!',
                dropzoneOptions: {
                    url: '/storage',
                    thumbnailWidth: 100,
                    thumbnailHeight: 100,
                    headers: { 'x-csrf-token':  window.Laravel.csrfToken }
                },
                storage: {},
                storageObject: {},
                errors: [],
            },
            mounted: function(){
                this.loadStorageObjects();
            },
            methods: {
                loadStorageObjects:function(page){
                    if (typeof page === 'undefined') {
                        page = 1;
                    }
                    axios.get(this.baseUrl+'/storagelisting?page='+ page)
                            .then(response => {
                        // JSON responses are automatically parsed.
                        this.storage = response.data;
                })
                    .catch(e => {
                        this.errors.push(e)
                })
                },
                dropZoneError:function (file, message, xhr) {
                    console.log(file);
                    console.log(message);
                    console.log(xhr);
                }
            }

        });
    </script>
@endsection