@extends('layouts.app')
@section('header')
@endsection
@section('title')
{{$title}}
@endsection
@section('content')
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="col-md-4">
            <a class="btn btn-primary btn-block" href="{{url('/license')}}" >Utilization<div class="ripple-container"></div></a>
        </div>
        <div class="col-md-4">
            <a class="btn btn-primary btn-block" disabled>Top Center<div class="ripple-container"></div></a>
        </div>
    </div>
</div>
<div class="card">
    <highcharts :options="options" ref="highcharts"></highcharts>
</div>
<div class="card">
    <highcharts :options="options" ref="highcharts"></highcharts>
</div>
<div class="card">
    <highcharts :options="options" ref="highcharts"></highcharts>
</div>
@endsection
@section('scripts')
<script src="https://unpkg.com/highcharts@6.0.1/highcharts.js"></script>
<script src="https://unpkg.com/vue-highcharts@0.0.10/dist/vue-highcharts.min.js"></script>
<script>
  console.log('I am here');
  Vue.use(VueHighcharts);

  var options = {
    title: {
      text: 'Monthly Average Temperature',
      x: -20 //center
    },
    subtitle: {
      text: 'Source: WorldClimate.com',
      x: -20
    },
    xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ]
    },
    yAxis: {
      title: {
        text: 'Temperature (°C)'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#808080'
      }]
    },
    tooltip: {
      valueSuffix: '°C'
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      borderWidth: 0
    },
    series: [{
      name: 'Tokyo',
      data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
    }, {
      name: 'New York',
      data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
    }, {
      name: 'Berlin',
      data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
    }, {
      name: 'London',
      data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
    }]
  };
  const app = new Vue({
    el: '#app',
    data: {
      message: 'Hello VUE!',
      options: options
    },
    methods: {
      updateCredits: function() {
        var chart = this.$refs.highcharts.chart;
        chart.credits.update({
          style: {
            color: '#' + (Math.random() * 0xffffff | 0).toString(16)
          }
        });
      }
    }
  });

</script>
@endsection