# Fetch-a-File Backend #

FetchAfile backend API is main application that powers FAF application.
It is based on Laravel 5.4, couple of number of opensource libraries. 

This application is based on specification document on FAF, which is available 
in proofhub under FAF project files.

Anyone starting this project should read about Laravel framework in details to understand 
how different section of application together. 


## FAF Backend API ##

### Deployment Steps ###


#####  First Deployment for Production
 
1. composer install
2. php artisan migrate --force
3. php artisan passport:install
4. php artisan scout:mysql-index FAF\\Tag
5. php artisan scout:mysql-index FAF\\Topic
6. php artisan storage:link
  

 
#####  First Deployment for testing
 
1. composer install
2. php artisan migrate --force --env=testing
3. php artisan passport:install --env=testing
4. php artisan scout:mysql-index FAF\\Tag --env=testing
5. php artisan scout:mysql-index FAF\\Topic --env=testing

**Only on staging**  
6. 
php artisan api:generate --router="dingo" --routePrefix="v1"


#####  Updates Deployment for Production

1. update the source code 
2. composer install
3. php artisan migrate --force



### Configuration file ###

There is a file called .env.example file located in the root directory 
of the project which contain all the configurational values required to run 
the application. 


## Running Supervisor ##

sudo /etc/init.d/supervisor restart
sudo /etc/init.d/supervisor start
sudo /etc/init.d/supervisor stop


## For Developers laravel FAF Commands ##

Update tags and topic having not added in ES and DB
storage:updatetagsandtopics userId

Move file extension from s3_name to file_type field in Storage Object 
storage:extension

Remove Storage object of given user from S3 and  
directory:delete

Upload Directory from one disk to another
directory:upload





