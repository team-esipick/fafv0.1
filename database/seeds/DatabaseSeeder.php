<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(AdminUserSeeder::class);
         //$this->call(RoleAdminsSeeder::class);
        // $this->call(RolesSeeder::class);
         //$this->call(CategorySeeder::class);
        // $this->call(CompanySeeder::class);
         $this->call(ExtensionSeeder::class);
    }
}
