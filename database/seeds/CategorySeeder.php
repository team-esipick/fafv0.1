<?php

use Illuminate\Database\Seeder;
use FAF\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_gb');
        for($a =0; $a<10;$a++){
            Category::create([
                'name' => $faker->realText(10),
                'description' => $faker->realText(100),
                'parent_id' => $faker->numberBetween(0,10),
                'slug' => $faker->slug(10),
                'status' => $faker->numberBetween(0,1),
            ]);
        }

    }
}
