<?php

use Illuminate\Database\Seeder;
use FAF\Company;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'name' => 'Engeo',
            'email' => 'admin@craft.com',
            'phone' => '3242423432',
            'sub_domain' => 'engeo',
            'site_url' => 'engeo.com',
            'site_title' => 'Engeo',
        ]);
    }
}
