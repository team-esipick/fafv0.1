<?php

use Illuminate\Database\Seeder;
use FAF\RoleAdmin;

class RoleAdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoleAdmin::create([
            'role_id' => '1',
            'admin_id' => '1',
        ]);
    }
}
