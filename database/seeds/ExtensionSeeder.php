<?php

use Illuminate\Database\Seeder;

class ExtensionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $storage_objects = array(
            array('file_type' => 'dwf','total' => '112'),
            array('file_type' => 'CAB','total' => '90'),
            array('file_type' => 'exe','total' => '86'),
            array('file_type' => 'lnk','total' => '76'),
            array('file_type' => 'jgw','total' => '54'),
            array('file_type' => 'swf','total' => '51'),
            array('file_type' => 'msg','total' => '50'),
            array('file_type' => 'wmv','total' => '48'),
            array('file_type' => 'MPG','total' => '46'),
            array('file_type' => 'aif','total' => '42'),
            array('file_type' => 'sdb','total' => '35'),
            array('file_type' => 'zip','total' => '31'),
            array('file_type' => 'kmz','total' => '30'),
            array('file_type' => 'inf','total' => '25'),
            array('file_type' => 'dbf','total' => '21'),
            array('file_type' => 'sdw','total' => '20'),
            array('file_type' => 'rnd','total' => '16'),
            array('file_type' => 'tif','total' => '15'),
            array('file_type' => 'cat','total' => '15'),
            array('file_type' => 'abt','total' => '12'),
            array('file_type' => 'eps','total' => '11'),
            array('file_type' => 'bat','total' => '11'),
            array('file_type' => 'DETAIL','total' => '10'),
            array('file_type' => 'ddd','total' => '9'),
            array('file_type' => 'mht','total' => '8'),
            array('file_type' => 'mp3','total' => '8'),
            array('file_type' => 'mp4','total' => '7'),
            array('file_type' => 'class','total' => '7'),
            array('file_type' => 'ai','total' => '6'),
            array('file_type' => 'ico','total' => '6'),
            array('file_type' => 'rtf','total' => '6'),
            array('file_type' => 'flv','total' => '6'),
            array('file_type' => 'bmp','total' => '5'),
            array('file_type' => 'pdd','total' => '5'),
            array('file_type' => 'wld','total' => '4'),
            array('file_type' => 'm4v','total' => '4'),
            array('file_type' => 'did','total' => '4'),
            array('file_type' => 'ini','total' => '4'),
            array('file_type' => 'frm','total' => '4'),
            array('file_type' => 'url','total' => '4'),
            array('file_type' => 'bin','total' => '3'),
            array('file_type' => 'trn','total' => '3'),
            array('file_type' => 'ocx','total' => '3'),
            array('file_type' => 'kml','total' => '3'),
            array('file_type' => 'idx','total' => '2'),
            array('file_type' => 'mov','total' => '2'),
            array('file_type' => 'dat','total' => '2'),
            array('file_type' => 'app','total' => '2'),
            array('file_type' => 'ppsx','total' => '2'),
            array('file_type' => 'msi','total' => '2'),
            array('file_type' => 'gz','total' => '2'),
            array('file_type' => 'PDX','total' => '2'),
            array('file_type' => 'db','total' => '2'),
            array('file_type' => 'DS_Store','total' => '1'),
            array('file_type' => 'avi','total' => '1'),
            array('file_type' => '','total' => '1'),
            array('file_type' => 'jar','total' => '1'),
            array('file_type' => 'emf','total' => '1'),
            array('file_type' => 'mpeg','total' => '1'),
            array('file_type' => 'scc','total' => '1'),
            array('file_type' => 'dir','total' => '1'),
            array('file_type' => 'wmf','total' => '1'),
            array('file_type' => 'shp','total' => '1'),
            array('file_type' => 'aqt','total' => '1'),
            array('file_type' => 'mdb','total' => '1'),
            array('file_type' => 'psd','total' => '1'),
            array('file_type' => 'vcf','total' => '1'),
            array('file_type' => 'bni','total' => '1'),
            array('file_type' => 'search','total' => '1'),
            array('file_type' => '05I','total' => '1'),
            array('file_type' => 'dot','total' => '1'),
        );
        foreach($storage_objects as $storage_object){
            \FAF\Extension::firstOrCreate(['type'=>$storage_object['file_type']]);
        }
    }
}
