<?php

use Illuminate\Database\Seeder;
use FAF\Admin;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'Admin',
            'email' => 'admin@craft.com',
            'password' => bcrypt('admin@123'),
        ]);
    }
}
