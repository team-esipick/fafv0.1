<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //id, on_blog, from_user, body, at_time
        Schema::create('categories', function(Blueprint $table)
        {
            $table->increments('id');
            $table -> string('name');
            $table -> text('description');
            $table -> string('parent_id')->nullable();
            $table -> string('slug');
            $table -> integer('status')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop comment
        Schema::drop('categories');
    }
}
