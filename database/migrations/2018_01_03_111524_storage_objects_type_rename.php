<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StorageObjectsTypeRename extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('storage_objects', function (Blueprint  $table) {
            $table->renameColumn('type', 'file_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('storage_objects', function (Blueprint $table) {
            $table->renameColumn('file_type', 'type');
        });
    }
}
