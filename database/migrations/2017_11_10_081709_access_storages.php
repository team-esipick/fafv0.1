<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccessStorages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_storage_objects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('storage_object_id');
            $table->integer('share_id')->nullable();
            $table->string('share_with')->nullable();
            $table->boolean('read')->default(0);
            $table->boolean('write')->default(0);
            $table->boolean('update')->default(0);
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_storage_objects');
    }
}
