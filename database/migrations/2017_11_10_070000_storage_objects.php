<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class StorageObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage_objects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            NestedSet::columns($table);
            $table->string('title')->nullable();
            $table->string('s3_name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('short_description')->nullable();
            $table->string('slug')->nullable();
            $table->text('detail')->nullable();
            $table->string('size')->nullable();
            $table->string('type')->nullable();
            $table->string('mime_type')->nullable();
            $table->string('ip')->nullable();
            $table->string('lat')->nullable();
            $table->string('lag')->nullable();
            $table->string('city')->nullable();
            $table->text('meta')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('storage_objects');
    }
}
