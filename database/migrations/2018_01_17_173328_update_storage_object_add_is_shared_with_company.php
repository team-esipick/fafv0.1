<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStorageObjectAddIsSharedWithCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('storage_objects', function (Blueprint  $table) {
            $table->boolean('is_shared_with_company')->default(0);;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('storage_objects', function(Blueprint $table) {
            $table->dropColumn('is_shared_with_company');
        });
    }
}
