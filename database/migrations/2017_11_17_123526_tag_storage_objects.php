<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TagStorageObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tag_storage_objects', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('storage_object_id');
            $table->bigInteger('tag_id');
            $table->enum('type',['ml','user'])->default('user');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_storage_objects');
    }
}
