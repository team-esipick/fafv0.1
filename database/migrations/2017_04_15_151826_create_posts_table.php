<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // blog table
        Schema::create('posts', function(Blueprint $table)
        {
            $table->increments('id');
            $table -> integer('author_id') -> unsigned() ->default(0);
			$table->string('title')->unique();
			$table->string('image')->unique();
			$table->text('body');
			$table->string('slug')->unique();
			$table->boolean('active');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
