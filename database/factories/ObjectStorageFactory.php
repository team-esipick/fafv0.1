<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(FAF\StorageObject::class, function (Faker\Generator $faker) {

    return [

        'description' => $faker->text(200),
        'short_description' => $faker->text(50),
        'title'=> $faker->name() ,
        'author' => $faker->name(),
        // access info

        'company_id'=> $faker->numberBetween(1,500) , //
        'user_id'=> $faker->numberBetween(501,999) ,  //
        'team_ids'=> [  $faker->numberBetween(1000,1500) , $faker->numberBetween(1000,1500) ] , //
        'team_id'=> $faker->numberBetween(1000,1500) , //
        'user_ids'=> [  $faker->numberBetween(1,500) , $faker->numberBetween(1,500) ] , //

        'company_name'=> $faker->company(),
        'file_name'=> $faker->file('test','public'),
        'file_size'=> $faker->numberBetween(1000,11111112211),
        'file_extn'=> $faker->fileExtension,
        'file_mime_type'=> 'application/' . $faker->fileExtension ,
        'created_at'=> $faker->date('Y-m-d'),
        'updated_at'=> $faker->date('Y-m-d'),
        'tags'=>  [ $faker->colorName, $faker->colorName, 'Gold'],
        'topics'=> [ $faker->colorName, $faker->colorName, 'Pakistan'],
        'city'=> $faker->city,
        'country'=> $faker->country,
        'location' => [[

            'lat'=> $faker->latitude,
            'lon'=> $faker->longitude,

        ],
            [
                'lat'=> $faker->latitude,
                'lon'=> $faker->longitude,
            ]
        ],
        'ip' => $faker->ipv4

    ];
});
