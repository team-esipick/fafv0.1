<?php
/**
 * Created by PhpStorm.
 * User: Esipick
 * Date: 25-Oct-17
 * Time: 1:32 PM
 */
use Auth as Auth;
function canAddTeam(){
    if(!empty(Auth::user()->team_id)){
        return false;
    }
    return true;
}
function belongsTeam(){
    if(!empty(Auth::user()->team_id)){
        return false;
    }
    return true;
}

/**
 *
 * pass the data to be decode to json,
 * incase of a error this method will throw an exception
 * with the text of the json error
 *
 */
function jsonDecode($data,$assoc = false, $depth = 512, $options = 0){

    $data = json_decode($data,$assoc, $depth , $options);

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            return $data;
            break;
        case JSON_ERROR_DEPTH:
            throw new Exception(' - Maximum stack depth exceeded');
            break;
        case JSON_ERROR_STATE_MISMATCH:
            throw new Exception(' - Underflow or the modes mismatch');
            break;
        case JSON_ERROR_CTRL_CHAR:
            throw new Exception(' - Unexpected control character found');
            break;
        case JSON_ERROR_SYNTAX:
            throw new Exception(' - Syntax error, malformed JSON');
            break;
        case JSON_ERROR_UTF8:
            throw new Exception(' - Malformed UTF-8 characters, possibly incorrectly encoded');
            break;
        default:
            throw new Exception(' - Unknown error');
            break;
    }


}
/*
 * Extracts the file from a given give result from file name with extension
 * @param String $path a single output from the above function  'directory/directory 2/my file.php'
 * $return string file path
 */

function getFileNameWithoutExtension($filename){

    return pathinfo($filename, PATHINFO_FILENAME);

}

function getObject(){

    return (object)[];

}

function arrToObject(array $array ) : stdClass{
    return jsonDecode(json_encode($array), FALSE);
}

/**
 *  send the all php request globla variables
 * @return array
 */
function getFullRequest() : array {
    return  [
        '$_REQUTEST' => $_REQUEST,
        '$_FILES' => $_FILES,
        '$_POST' => $_POST,


    ];
;
}

