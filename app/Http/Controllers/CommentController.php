<?php namespace FAF\Http\Controllers;

use FAF\Post;
use FAF\Comment;
use Illuminate\Http\RedirectResponse;
use Redirect;
use FAF\Http\Requests;
use FAF\Http\Controllers\Controller;
use FAF\Libs\Contracts\IndexerInterface;

use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;

class CommentController extends Controller {
    protected $es;
    public function __construct(IndexerInterface $es)
    {
        $this->es = $es;
    }
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//on_post, from_user, body
		$input['from_user'] = $request->user()->id;
		$input['on_post'] = $request->input('on_post');
		$input['body'] = $request->input('body');
		$slug = $request->input('slug');
		Comment::create( $input );
 
		return redirect($slug)->with('message', 'Comment published'); 	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function licenses()
    {
        $data['title'] = 'Utilization';
        $data['arr']['tul']['name'] = 'Top Utilized Licenses';
        $data['arr']['tul']['cat'] = ['License 1', 'License 2', 'License 3', 'License 4'];
        $data['arr']['tul']['data1'] = [15, 23, 44, 37];
        $data['arr']['tul']['data2'] = [11, 10, 8, 5];

        $data['arr']['tuul']['name'] = 'Top Utilized Licenses';
        $data['arr']['tuul']['cat'] = ['License 1', 'License 2', 'License 3', 'License 4'];
        $data['arr']['tuul']['data1'] = [8, 12, 16, 6, 13];
        $data['arr']['tuul']['data2'] = [2, 2, 3, 2, 1];

        $data['arr']['tol']['name'] = 'Top Overutilized Licenses';
        $data['arr']['tol']['cat'] = ['License 5', 'License 6', 'License 7', 'License 8'];
        $data['arr']['tol']['data1'] = [100, 80, 50, 100];
        $data['arr']['tol']['data2'] = [40, 0, 0, 0];
         return view('licensesUtilization',$data);
    }

	public function licenseCost()
    {
        return view('licensesCost')->withTitle('Cost');
    }

	public function reports()
    {
        return view('reports')->withTitle('Reports');
    }
    public function search(Request $request)
    {
        $search = '';
        $response = [];
        if ( $request->exists('find') ) {
            $search = $request->get('find');

            $params = [
                'index' => 'storage_object',
                'type' => 'string',
                'body' => [
                    'query' => [
                        'match' => [
                            'description' => $search
                        ]
                    ]
                ]
            ];
            $client = ClientBuilder::create()
                ->setHosts([ 'host' => env('ELASTIC_HOST') ])
                ->build();
            $response = $client->search($params);

        }
        //dd($response);
        return view('search')
            ->withTitle('Reports')
            ->withSearch($search)
            ->withResult($response);


    }
    public function searchStorageObjects(Request $request)
    {
        $search = '';
        $response = [];
        if ( $request->exists('keyword') ) {
            $search = $request->get('keyword');

            $params = [
                'index' => 'storage_object',
                'type' => 'string',
                'body' => [
                    'query' => [
                        'match' => [
                            'description' => $search,

                        ]
                    ]
                ]
            ];
            $client = ClientBuilder::create()
                ->setHosts([ 'host' => env('ELASTIC_HOST') ])
                ->build();
            $response = $client->search($params);

        }
        //dd($response);
        return ['response'=>$response];
        return view('search')
            ->withTitle('Reports')
            ->withSearch($search)
            ->withResult($response);


    }
	public function download()
    {
        $data['title'] = 'Download';
        return view('download',$data);
    }

}
