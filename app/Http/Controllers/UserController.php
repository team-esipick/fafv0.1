<?php
namespace FAF\Http\Controllers;

use FAF\Company;
use FAF\Events\NewUserSignup;
use FAF\Http\Requests;
use FAF\Http\Controllers\Controller;
use FAF\Jobs\SendMemberInvitationEmail;
use FAF\User;
use FAF\Post;
use FAF\UserCompany;
use FAF\StorageObject;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Snowfire\Beautymail\Beautymail;
use Stevebauman\Location\Location;
use Validator;
use FAF\Http\Requests\LoginPostRequest;
use FAF\Http\Requests\RegisterPostRequest;
use Illuminate\Support\Facades\Password;
use FAF\Http\Requests\ForgotPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FAF\Http\Requests\ResetPasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;
use FAF\Http\Requests\WebsiteVerifyRequest;
use FAF\Http\Requests\EmailVerifyRequest;
use FAF\Http\Requests\CompanyVerifyRequest;
use FAF\Jobs\say;
use FAF\Exceptions\Rescue;
use FAF\Http\Requests\StorageRequest;
use FAF\Auth\LoginProxy;
use Illuminate\Support\Facades\Cookie;
use FAF\Http\Requests\VerifyTokenRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use FAF\Http\Requests\UserCreateRequest;
use FAF\Http\Requests\UserInvitationRequest;
use FAF\Http\Requests\UserInviteListingRequest;

class UserController extends Controller {
    use ResetsPasswords;
    public $successStatus = 200;

    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }



    /**
     * User Login
     *
     * @param LoginRequest $request
     *
     */

    public function login(LoginPostRequest $request){

        //check if user's belongs to a the given company
        $user = User::Where('email', $request->get('email'))->first();

        if($user === null){

           return $this->response->errorUnauthorized();

        }

        if( ! $user->hasCompanyBySubdomain($request->get('sub_domain')) ){

             $this->response->errorUnauthorized('sub_domain does belong to user');
        }

        $token =  $this->loginProxy->attemptLogin($request->get('email'), $request->get('password'));

        if($token == false){

            return $this->response->errorUnauthorized();

        }

        $success = [];
        $success = array_merge($success,$token);

        foreach($user->companies as $company){
            $company->company;
        }
        $success['user']= $user->toArray();

        array_forget($success,['user.role','user.status','user.verify_token','user.api_token','user.password_set_token','user.stripe_id','user.card_brand','user.card_last_four','user.team_id','user.trial_ends_at']);
        return $this->response->array($success);



    }
    public function refresh(Request $request)
    {

        if (Cookie::get($this->loginProxy->getRefreshTokenName()) == null){
            $this->response->errorUnauthorized();
        }


        return $this->response->array($this->loginProxy->attemptRefresh()) ;
    }

    /**
     * @transformer Transfor
     */
    public function transformerTag()
    {
        return '';
    }

    /**
     * Verify Email Register
     *
     * @param EmailVerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     * * @Transaction({
     *      @Response(204, noContent),
     *      @Response(422, body={"error text"})
     * })

     */
    public function verifyEmail(EmailVerifyRequest $request)
    {
        return $this->response->noContent();
    }
    /**
     * Verify Email Register
     *
     * @param EmailVerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     * * @Transaction({
     *      @Response(200, noContent),
     *      @Response(422, body={"error text"})
     * })

     */
    public function verifyCompany(CompanyVerifyRequest $request)
    {
        return $this->response->noContent();
    }
    /**
     * Verify Website Register
     *
     * @param WebsiteVerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @Transaction({
     *      @Response(200, noContent),
     *      @Response(422, body={"error text"})
     * })
     */
    public function verifyWebsite(WebsiteVerifyRequest $request)
    {
        $company = Company::where('sub_domain',$request->sub_domain)->first();
        if($company){
            Rescue::PreconditionFailed('This site name is already taken.');
        }
        return $this->response->noContent();
    }

    /**
     * Create User By Admin User
     *
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMembers(UserCreateRequest $request)
    {
        $request->request->add(['invitation_token' => base64_encode($request->email)]);
        $request->request->add(['invited_by' => Auth::user()->id]);
        $user= User::create($request->all());
        $attributes=['company_id'=>$request->company_id,
                      'user_id'=>$user->id
                    ];
        $companyUser = UserCompany::create($attributes);

       dispatch(new SendMemberInvitationEmail($user, $companyUser));
        return response()
            ->json($companyUser, 201);
    }

    /**
     * Get Invited Members By Admin User
     *
     * @param UserInviteListingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function invitesMembers(UserInviteListingRequest  $request)
    {
        $perpahe = $request->perpage = 10;
        User::where('invited_id',Auth::user()->id)
            ->whereNull('invitation_token')
            ->paginate(10);

        $request->request->add(['invitation_token' => base64_encode($request->email)]);
        $user= User::create($request->all());
        $attributes=['company_id'=>$request->company_id,
            'user_id'=>$user->id
        ];
        $companyUser = UserCompany::create($attributes);

        dispatch(new SendMemberInvitationEmail($user, $companyUser));
        return response()
            ->json($companyUser, 201);
    }


    /**
     * User Verify invitation Send by admin user
     *
     * @param UserInvitationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyInvitation(UserInvitationRequest $request)
    {
        $user = User::where('invitation_token',$request->token)->first();

        if(empty($user)){
            return response()
                ->json('fail', 401);
        }
        $user->password = bcrypt($request->password);
        $user->invitation_token = null;
        $user->invitation_verified = 1;
        if($user->save()){
            return response()
                ->json('success', 201);
        }
    }


    /**
     * User Register
     *
     * @param RegisterPostRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function register(RegisterPostRequest $request)
    {
        $input = $request->all();
        $input['password'] =   bcrypt($request->password);
        $user = User::create($input);

       $success['user'] =  $user->toArray();
        $companyAttributes = [
            'name'=> $request->company_name,
            'email'=>$user->email,
            'phone'=>$user->phone,
            'sub_domain'=>$request->sub_domain,
            'storage_folder_name'=>$request->sub_domain,
            'site_url'=>'',
            'site_title'=>''
        ];
        $companies = Company::create($companyAttributes);

        $user->companies()->attach($companies->id);
        $success['company'] = $companies->toArray();

        $token =  $this->loginProxy->attemptLogin($input['email'], $request->password);
        $success = array_merge($success,$token);

        event(new NewUserSignup($user));
        return $this->response->created(null,$success);


    }
    /**
     * User Login Data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function details(){
        $user = Auth::user();
        return $this->response->array([$user], $this->successStatus);

    }
    /**
     * Heartbeat
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function heartBeat(Request $request){

        $storageObject= StorageObject::with(['owner','tags','topics','ancestors','company'])->where(['user_id'=> $request->user_id ])
            ->where('mime_type','<>','folder')
            ->orderBy('id','desc')->paginate(10);

        return $storageObject;
        //event(new NewUserSignup(new User()));
        //Rescue::AccessDenied('here you go');
        return $this->response->array(['i am alive'], 201);

    }

    /**
     * logout
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->loginProxy->logout();
        return response()
            ->json(['message' => 'Successfully logged out']);
    }
    /**
     * Verify password token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPasswordToken(VerifyTokenRequest $request){


        $reminder = DB::table('password_resets')->where('email', $request->email)->first();
        if($reminder == null){
            // Return 404, as Reminder was not found
            return $this->response()->errorNotFound('Token not found');
        }
        if ( ! Hash::check($request->token, $reminder->token)) {
            // Return 404, as Reminder was not found
            return $this->response()->errorNotFound('Token not found or expired');
        }

        return $this->response()->noContent();
    }
    /**
     * User Forgot Password Sending email
     *
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetEmail(ForgotPasswordRequest $request)
    {

        $user = User::where('email', '=', $request->get('email'))->first();

        if(!$user) {
            return response()->json(['error'=>'email not found'], 404);
        }

        $broker = $this->getPasswordBroker();
        $sendingResponse = $broker->sendResetLink($request->only('email'));

        if($sendingResponse !== Password::RESET_LINK_SENT) {
            $this->response()->errorNotFound('token not found or expired');
        }

        return response()->json([
            'status' => 'ok'
        ], 200);
    }
    /**
     * User Forgot Password Change password
     *
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {

        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->reset($user, $password);
        }
        );

        if($response !== Password::PASSWORD_RESET) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok'
        ]);
    }



    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function reset($user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
    }


    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    private function getPasswordBroker()
    {
        return Password::broker();
    }



		/*
	 * Display the posts of a particular user
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function user_posts($id)
	{
		//

		$posts = Post::where('author_id',$id)->where('active','1')->orderBy('created_at','desc')->paginate(5);
		$title = User::find($id)->name;
		return view('home')->withPosts($posts)->withTitle($title);
	}

	public function user_posts_all(Request $request)
	{
		//
		$user = $request->user();
		$posts = Post::where('author_id',$user->id)->orderBy('created_at','desc')->paginate(5);
		$title = $user->name;
		return view('home')->withPosts($posts)->withTitle($title);
	}
	
	public function user_posts_draft(Request $request)
	{
		//
		$user = $request->user();
		$posts = Post::where('author_id',$user->id)->where('active','0')->orderBy('created_at','desc')->paginate(5);
		$title = $user->name;
		return view('home')->withPosts($posts)->withTitle($title);
	}

	/**
	 * profile for user
	 */
	public function profile(Request $request, $id) 
	{
		$data['user'] = User::find($id);
		if (!$data['user'])
			return redirect('/');

		if ($request -> user() && $data['user'] -> id == $request -> user() -> id) {
			$data['author'] = true;
		} else {
			$data['author'] = null;
		}
		$data['comments_count'] = $data['user']->comments->count();
		$data['posts_count'] = $data['user']->posts->count();
		$data['posts_active_count'] = $data['user']->posts->where('active', 1)->count();
		$data['posts_draft_count'] = $data['posts_count'] - $data['posts_active_count'];
		$data['latest_posts'] = $data['user']->posts->where('active', 1)->take(5);
		$data['latest_comments'] = $data['user']->comments->take(5);
		return view('auth.profile', $data);
	}
	/**
	 * profile for user
	 */
	public function edit_profile()
	{
		return view('auth.editprofile')->withTitle('Edit Profile');
	}
	public function update_profile(Request $request)
	{
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->save();
        return redirect('/dashboard')->with('message', 'success|Record updated.');
	}



	/**
	 * profile for user
	 */
	public function activeUsers()
	{
	    /*echo "<pre>";
	    var_dump(Auth::user()->role);
        echo "</pre>";
	    die();*/
	    $data['title'] = 'Active Users';
        $data['arr']['interval'] = ['10AM' ,'12AM', '02PM', '04PM', '06PM', '08PM', '10PM', '12PM', '00AM', '02AM', '04AM', '06AM'];
        $data['arr']['active_user' ] = [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4];
        $data['arr']['inactive_user'] = [144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 29.9, 71.5, 106.4, 129.2];
        $data['arr']['idle_user'] = [144.0, 176.0, 110.6, 100.5, 516.4, 194.1, 20.6, 54.4, 29.9, 71.5, 106.4, 129.2];
        return view('activeusers',$data);
	}

	public function make_subscription(Request $request){
        $stripeToken = $request->stripeToken;
        $user = User::find(Auth::user()->id);
        try {
            $user->newSubscription('main', $request->plan)->create($stripeToken);
        } catch (\Exception $e)
        {
            Session::flash('message',['type'=>'error','message'=>'Error! Something went wrong try again later.']);
            return redirect()->action('UserController@activeUsers');
        }

        $data = [
            'subject'=> 'Subscription Confirmation!',
            'email'=>$user->email,
            'username'=> $user->name,
            'salute'=>'Dear',

        ];

        $data['message'] ='Your has been successfully subscribed to <stong>'.$request->plan.' Plan</stong>.<br>';
        $data['message'] .='You can now go to dashboard and  can enjoy our serveries';

        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('email.welcome', ['data'=>$data], function($message) use ($data)
        {
            $message
                ->from('info@craft.com','Craft')
                ->to($data['email'], $data['username'])
                ->subject($data['subject']);
        });

        Session::flash('message',['type'=>'success','message'=>'Subscribed! You are successfully subscribed.']);
        return redirect()->action('UserController@activeUsers');

	}
	public function cancel_subscription_now(){

        $user = User::find(Auth::user()->id);
        try {
            $user->subscription('main')->cancelNow();
        } catch (\Exception $e)
        {
            Session::flash('message',['type'=>'error','message'=>'Error! Something went wrong try again later.']);
            return redirect()->action('UserController@activeUsers');
        }
        $data = [
            'subject'=> 'Subscription Confirmation!',
            'email'=>$user->email,
            'username'=> $user->name,
            'salute'=>'Dear',

        ];

        $data['message'] ='Your has been  un-subscribed to to your Plan</stong>.<br>';
        $data['message'] .='You can\'t use our services now.';

        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('email.welcome', ['data'=>$data], function($message) use ($data)
        {
            $message
                ->from('info@craft.com','Craft')
                ->to($data['email'], $data['username'])
                ->subject($data['subject']);
        });

        Session::flash('message',['type'=>'success','message'=>'Un-subscribed! You are successfully unSubscribed immediately.']);
        return redirect()->action('UserController@subscription');
	}
	public function cancel_subscription(){
        $user = User::find(Auth::user()->id);
        try {
            $user->subscription('main')->cancel();
        } catch (\Exception $e)
        {
            Session::flash('message',['type'=>'error','message'=>'Error! Something went wrong try again later.']);
            return redirect()->action('UserController@activeUsers');
        }
        Session::flash('message',['type'=>'success','message'=>'Un-subscribed! You are successfully unSubscribed.']);
        return redirect()->action('UserController@activeUsers');
	}
	public function subscription(){
        $data['title'] = 'Subscription';
        return view('subscription',$data);
	}
	public function invoices(){
        $user = User::find(Auth::user()->id);
        $data['title'] = "Invoices";
        if(!is_null($user->stripe_id))
        $data['invoices'] = $user->invoices();
        else
            $data['invoices'] = [];

        return view('invoices',$data);
    }


}

