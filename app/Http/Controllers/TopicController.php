<?php

namespace FAF\Http\Controllers;

use FAF\Http\Requests\TopicAddRequest;
use FAF\Http\Requests\TopicDeleteRequest;
use FAF\Http\Requests\TopicGetRequest;
use FAF\Http\Requests\TopicSearchRequest;
use FAF\Http\Requests\TopicUpdateRequest;
use FAF\Libs\Contracts\IndexerInterface;
use FAF\StorageObject;
use FAF\Topic;
use FAF\TopicStorageObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{

    protected $Indexer;
    protected $TopicModel;
    public function __construct(IndexerInterface $indexer, Topic $TopicModel)
    {
        $this->Indexer = $indexer;
        $this->TopicModel = $TopicModel;
    }

    /**
     * Topic Add
     *
     * @param TopicAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TopicAddRequest $request){

        $request->request->add(['slug' => str_slug($request->slug)]);

        $EsTopics=[];


        $EsTopics = $this->TopicModel->store($request->all());


        return response()->json($EsTopics, 201);
    }

    public function storeTopics($ml_json_data,$so,$type = 'ml' ){

        foreach($ml_json_data->Topics as $topic){
            $attrributes = [
                'company_id'=> $so->company_id,
                'user_id'=>$so->user_id,
                'title'=>$topic,
                'slug'=>str_slug($topic),
                'storage_object_id'=> $so->id,
            ];
            $this->TopicModel->store($data);
            $topicStorageAttrributes = [
                'storage_object_id'=>$so->id,
                'topic_id'=>$topic->id,
                'type'=>$type
            ];
            TopicStorageObject::create($topicStorageAttrributes);
        }
    }

    /**
     * Topic Update
     *
     * @param TopicUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TopicUpdateRequest $request){
        $topic= TopicStorageObject::find($request->id);
        $EsTopics =[];
        if(empty($topic)){
            return response()->json('not found', 401);
        }
        $topic->status = ($request->status == 'correct')?1:2;
        $topic->save();
        if($request->status == 'incorrect'){
            $EsTopics = $this->storeTopicsES($topic->storage_object_id);
        }

        return response()->json(empty($EsTopics)?$topic:$EsTopics, 201);
    }

    private function storeTopicsES($storage_object_id){
        $EsTopics=[];
        $allCorrectTopics = TopicStorageObject::where(['storage_object_id'=>$storage_object_id])->whereIn('status',[0,1])->get();
        foreach ($allCorrectTopics as $topics){
            $EsTopics[]=$topics->topic->title;
        }
        $this->Indexer->updateTopics($storage_object_id,$EsTopics );
        return $EsTopics;
    }

    /**
     * Topic get
     *
     * @param TopicGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(TopicGetRequest $request){

        return response()->json(['success' => $request->all()], 201);
    }

    /**
     * Topic Search
     *
     * @param TopicSearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(TopicSearchRequest $request){
        $topics = Topic::search($request->title)->take(10)->get();
        $data=[];
        foreach($topics as $key=>$topic){
            $data[$key]['value'] = $topic->id;
            $data[$key]['display'] = $topic->title;
        }
        return response()->json($data, 201);
    }

    /**
     * Topic delete
     *
     * @param TopicDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TopicDeleteRequest $request){
        $topic =  topicStorageObject::find($request->id);
        $topic->delete();
        return response()->json($request->all(), 201);
    }
}
