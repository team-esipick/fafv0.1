<?php

namespace FAF\Http\Controllers;

use FAF\Http\Requests\TeamGetRequest;
use FAF\Http\Requests\TeamStoreRequest;
use FAF\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use FAF\Team;
use FAF\TeamMember;
use Snowfire\Beautymail\Beautymail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use FAF\Http\Controllers\UploadedToS3;
class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('dashboard')->withTitle('Dashboard');
    }
    public function add()
    {
        $data['teams'] = Team::where('status','1')->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(10);
        if(!canAddTeam(Auth::user()->id)){
            return redirect('/en/home')->withErrors('You can\'t add team');
        }
        $data['title'] = 'Add Team';
        return view('team.addTeam',$data);
    }
    public function manage($team,Request $request)
    {
        if(!canAddTeam(Auth::user()->id)){
            return redirect('/en/home')->withErrors('You can\'t add team');
        }


        $data['team'] = Team::where('status','1')->where('id',$team)->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->first();
        if(!count($data['team'])){
            $request->session()->flash('message',['type'=>'success','message'=>'Please select a valid team!'] );
            return redirect('en/team/add');
        }

        if((count($data['team']->members)))
        $data['members'] = $data['team']->members[0]->paginate(10);
        else
            $data['members'] = [];

        $data['title'] = 'Manage Team';
        return view('team.viewTeams',$data);
    }
    public function saveMember($team,Request $request)
    {
        if(!canAddTeam(Auth::user()->id)){
            return redirect('/en/home')->withErrors('You can\'t add team');
        }
        $team = Team::where('status','1')->where('id',$team)->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->first();

        if(!count($team)){
            $request->session()->flash('message',['type'=>'error','message'=>'Please select a valid team!'] );
            return redirect('en/team/add');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'unique:users,email|required|email',
        ]);
        if ($validator->fails()) {
            return redirect('en/team/manage/'.$team->id)
                ->withErrors($validator)
                ->withInput();
        }
        $pass = str_random(10);;
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($pass);
        $user->role = 'author';
        $user->team_id = $team->id;
        $user->password_set_token = $this->generateToken();
        $user->save();

        //Send email
        $data = [
            'subject'=> 'Team Invitation',
            'email'=> $user->email,
            'username'=> $user->name,
            'salute'=> 'Dear',
        ];

        $data['message'] = 'You are invited by on <b>'.$team->title.'</b> Team. Your account has been successfully added at FetchAFile you can use this details to activate your account.<br><strong>email:</strong> '.$user->email.'.';
        $data['message'] .= '<p style="text-align: center" "><a href="'.url('en/verifyTeamMember/'.$data['email'].'/'.$user->password_set_token).'" class="button button-blue mail-blue-btn" target="_other" rel="nofollow">Set Password</a></p>';
        $data['footer'] = '';

        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('email.invitation', ['data'=>$data], function($message) use ($data)
        {
            $message
                ->from('info@craft.com','Craft')
                ->to($data['email'], $data['username'])
                ->subject($data['subject']);
        });
        $teamMember = new TeamMember();
        $teamMember->user_id = $user->id;
        $teamMember->team_id = $team->id;
        $teamMember->status = 'pending';
        $teamMember->save();
        $request->session()->flash('message',['type'=>'success','message'=>'Team Member successfully Added!'] );
        return redirect('en/team/manage/'.$team->id);
    }

    /**
     * Team Add
     *
     * @param TeamStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TeamStoreRequest $request)
    {
        $request->request->add(['user_id' => Auth::user()->id]);
        $team = Team::create($request->all());
        $teams = Team::with('members')->where('user_id',Auth::user()->id)
            ->paginate(10);
        return response()
            ->json(['data' => $team,'listing'=>$teams], 201);
    }

    /**
     * Team Listing
     *
     * @param TeamGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(TeamGetRequest $request)
    {
        $teams = Team::with('members')
            ->where('company_id',$request->company_id)
            ->where('user_id',Auth::user()->id)
            ->paginate(10);
        return response()
            ->json($teams, 201);
    }

    protected function generateToken()
    {
        $token = str_random(40);
        if(User::where('password_set_token', $token)->first()) {
            return $this->generateToken();
        }
        return $token;
    }
}
