<?php

namespace FAF\Http\Controllers;
use Carbon\Carbon;
use Dompdf\Exception;
use FAF\AccessStorageObject;
use FAF\Exceptions\Rescue;
use FAF\Http\Requests\DirectoryStoreRequest;
use FAF\Http\Requests\SearchRequest;
use FAF\Http\Requests\SharedListingRequest;
use FAF\Http\Requests\ShareStorageRequest;
use FAF\Http\Requests\StorageGetRequest;
use FAF\Http\Requests\StorageListingRequest;
use FAF\Http\Requests\StorageRequest;
use FAF\Jobs\ExtractText;
use FAF\ShareStorageObject;
use FAF\StorageObject;
use FAF\Tag;
use FAF\TagStorageObject;
use FAF\User;
use FAF\Company;
use Illuminate\Http\Request;
use FAF\Team;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use FAF\Events\UploadedToS3;
use FAF\Jobs\say;
use Intervention\Image\Facades\Image;
use Stevebauman\Location\Location;
use Symfony\Component\HttpKernel\EventListener\StreamedResponseListener;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Elasticsearch\ClientBuilder;
use FAF\Libs\Contracts\IndexerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
class StorageObjectController extends Controller
{
    protected $Indexer;
    public function __construct(IndexerInterface $indexer)
    {
        $this->Indexer = $indexer;
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $data['title'] = 'upload';

        return view('storageObjects.addStorage',$data);
    }

    public function tester (){

        $ml_json_data= ['abc','def','ghi','jkl','mno'];
        foreach($ml_json_data as $key=> $tag){
            $attrributes = [
                'company_id'=>1,
                'user_id'=> 2,
                'title'=>$tag,
                'slug'=>str_slug($tag),
                'type'=>'ml'
            ];

            $tag = Tag::firstOrNew(['title'=>$tag, ],$attrributes);

            $tag->save($attrributes);

            $tagStorageAttrributes = [
                'storage_object_id'=>14,
                'tag_id'=>$tag->id,
            ];
            TagStorageObject::create($tagStorageAttrributes);



        }
        dd('here');
    }
    /**
     * Storage get
     *
     * @param StorageGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(StorageGetRequest $request){
        $storage =  StorageObject::with('ancestors')->find($request->storage_object_id);
        $storage->company;
        $storage->s3_link =  $storage->s3Link($storage->s3_name);
        foreach ($storage->tags as $tag){
            $tag->tag;
        }
        foreach ($storage->topics as $topic){
            $topic->topic;
        }
        $storage->owner;
        $storage->storageAccess;
        $storage->topics;
        return response()->json($storage, 201);
    }

    public function folderListing(StorageListingRequest $request){

        $storageObject= $this->getFolderListings($request->sort,$request->per_page,$request);

        return response()
            ->json([$storageObject], 200);
    }
    /**
     * Storage listing
     *
     * @param StorageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listing(StorageListingRequest $request)
    {
        $storageObject= $this->getListings($request,$request->sort,$request->per_page);
        $storageObject['ancestors'] = [];
        return response()
            ->json([$storageObject], 200);
    }

    private function getListings($request,$sort=null,$per_page=10){

        //@todo check company_id from request and verify if user belongs to that company.
        $company = Auth::user()->firstCompany;
        if($sort){
            $storageObject= StorageObject::with(['owner','tags','topics','ancestors','company'])->where(['user_id'=>Auth::user()->id])
                //@todo this should be change once we added the share record in access_storage_object table
                ->orWhere(function($query) use ($company){
                    $query->where('is_shared_with_company',1)
                        ->where('company_id',$company->company_id);
                })
                ->whereNull('type')
                ->orderBy('id',$sort)->paginate($per_page);
        }else {
            $storageObject= StorageObject::with(['owner','tags','topics','ancestors','company'])->where(['user_id'=>Auth::user()->id])
                //@todo this should be change once we added the share record in access_storage_object table
                ->orWhere(function($query) use ($company){
                    $query->where('is_shared_with_company',1)
                        ->where('company_id',$company->company_id);
                })
                ->whereNull('type')
                ->orderBy('id','desc')->paginate($per_page);
        }
        return $this->processListing($storageObject);

    }
    private function getFolderListings($sort=null,$per_page=10,$request){
        $company = Auth::user()->firstCompany;
        $id= ($request->id)?($request->id):null;
        if($sort){
            $storageObject= StorageObject::with(['owner','tags','topics','company'])
                ->where(['user_id'=>Auth::user()->id,'parent_id'=>null])
                ->orWhere(function($query) use ($company){
                    $query->where('is_shared_with_company',1)
                        ->where('company_id',$company->company_id);
                })
                ->orderBy('type',$sort)
                ->orderBy('created_at',$sort)
                ->paginate($per_page);
            if($id){
                $storageObject= StorageObject::with(['owner','tags','topics','company'])
                    ->where(['parent_id'=>$id])
                    ->orderBy('type',$sort)
                    ->orderBy('created_at',$sort)
                    ->paginate($per_page);
            }

        }else {
            $storageObject= StorageObject::with(['owner','tags','topics','company'])
                ->where(['user_id'=>Auth::user()->id, 'parent_id'=>null])
                ->orWhere(function($query) use ($company){
                    $query->where('is_shared_with_company',1)
                        ->where('company_id',$company->company_id);
                })
                ->orderBy('type','desc')
                ->orderBy('created_at',$sort)
                ->paginate($per_page);
            if($id){
                $storageObject= StorageObject::with(['owner','tags','topics','company'])
                    ->where(['parent_id'=>$id])
                    ->orderBy('type','desc')
                    ->orderBy('created_at',$sort)
                    ->paginate($per_page);
            }
        }


        return $this->processListing($storageObject);

    }
    private function processListing($storageObject){
        foreach($storageObject as $storage){
            $storage->quantity =  $storage->filecount($storage->id);
            foreach($storage->storageAccess as $key=>$accessStorage){
                if($accessStorage->share_with  == 'company'){
                    $accessStorage->company;
                }else if($accessStorage->share_with  == 'team'){
                    $accessStorage->team;
                }else{
                    $accessStorage->user;
                }
            }

        }

        $ancestors = $storageObject[0]->ancestors ?? [];
        $listArray =  $storageObject->toArray();
        // a workround to add a new index in LengthAwarePaginator
        $listArray['ancestors'] = $ancestors;
        return $listArray;
    }

    /**
     * Shared listing
     *  This function works
     *
     * @param  SharedListingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sharedListing(SharedListingRequest $request){
        $storageObject= $this->getSharedListing($request,$request->sort,$request->per_page);
        $storageObject['ancestors'] = [];
        return response()
            ->json([$storageObject], 200);
    }

    private function getSharedListing($request,$sort=null,$per_page=10){

        if($sort){
            $storageObject = AccessStorageObject::with(['owner','tags','topics','ancestors','company'])
                ->where('share_with','company')
                ->where('share_id',$request->company_id)
                ->where('read',1)
                ->orderBy('id',$sort)->paginate($per_page);

        }else {

            $storageObject = AccessStorageObject::with(['storage_object','storage_object.owner','storage_object.tags','storage_object.topics','storage_object.ancestors','storage_object.company'])->where('share_with','company')
                ->where('share_id',$request->company_id)
                ->orderBy('id','desc')->paginate($per_page);
        }
        return $storageObject;
        return $this->processListing($storageObject);

    }
    /**
     * Search listing
     *  This function works as the primary search on all files
     *  in elastic search
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(SearchRequest $request)
    {
        $per_page= $request->per_page??10;
        $page= $request->page??1;
        $fromTotal = $per_page*$page - $per_page;
        $from = (($fromTotal) != 0)? ++$fromTotal :$fromTotal ;
        $to = $per_page*$page;

        $response = $this->Indexer->search(env('STORAGE_OBJECT_INDEX_NAME'),
            $request->keyword,Auth::user()->id,
            !empty($request->sort_order)?$request->sort_order:'desc', !empty($request->sort_type) && $request->sort_type=='date'?'created_at':'size',
            $from,
            $per_page,
            $request->from_date,
            $request->to_date,
            $request->tags,
            $request->topics,
            $request->types,
            $request->locations,
            $request->distance
        );
        if($response['hits']['total'] == 0){

            $data = [
                'aggregations'      => [] ,
                'current_page'      => '',
                'title'   => '',
                'year'    => '',
                'data'=>[],
                'from'    => 0,
                'last_page'    => 0,
                'next_page_url'  => '',
                'path'    => '',
                'per_page'    => 0,
                'prev_page_url'    => '',
                'to'    =>0,
                'total'    => 0
            ];
            return response()
                ->json([$data], 200);
        }

        foreach ($response['hits']['hits'] as &$result) {
            $storage = StorageObject::find((int)$result['_id']);
            $result['_source']['s3_link'] = '';
            $result['_source']['tagss'] = [];
            $result['_source']['topicss'] = [];
            $result['_source']['owner_name'] ='';

            if(!empty($storage)){
                $result['_source']['tagss']= $storage->tags;
                $result['_source']['topicss'] = $storage->topics;
                $owner_name = ucfirst($storage->owner->first_name .' '.  $storage->owner->last_name);
                $result['_source']['owner_name'] = $owner_name;
                if(!empty($storage->s3_name)){
                    $result['_source']['s3_link'] =$storage->s3Link($storage->s3_name);
                }
            }
        }

        $fractal = new Manager();

        $searchdata = new Collection($response['hits']['hits'], function(array $result) use ($request) {
            return [
                'id'  => $result['_id'] ?? 0,
                'city' => $result['_source']['city'] ?? '',
                'company' => [
                    'id'=>$result['_source']['company_id'],
                    'name'=>$result['_source']['company_name'] ?? '',
                ],
                'company_id' => $result['_source']['company_id'],
                'created_at' => $result['_source']['created_at'],
                'file_type' => $result['_source']['file_extn'] ?? '',
                'ip' => $result['_source']['ip'] ?? '',
                //'location'=> $result['_source']['location'] ?? [] ,
                'locations'=> $result['_source']['locations']?? [] ,
                'meta' => '',
                'mime_type' => $result['_source']['file_mime_type'] ?? '',
                'owner' => [],
                'quantity' => $result['_source']['city'] ?? '',
                's3_name' =>$result['_source']['s3_link'] ?? '',
                'size' => $result['_source']['file_size'] ?? '',
                'slug' => '',
                'status' => '',
                'storage_access' => '',
                'title' => $result['_source']['title'] ?? '',
                'tags' => $result['_source']['tagss'] ?? [],
                'topics' => $result['_source']['topicss'] ?? [],
                'type' => null,
                'updated_at' => $result['_source']['updated_at'],
                'highlight' => empty($request->highlights) ? $result['highlight']??[] : [],
            ];
        });
        $array = $fractal->createData($searchdata)->toArray();
        //Remove Zero index
        $array = array_shift($array);

        $response['aggregations'] = !isset($request->aggregations) && !$request->aggregations ? $response['aggregations'] : [];
        $data = [
            'aggregations'      => $response['aggregations'] ,
            'current_page'      => $page,
            'title'   => '',
            'year'    => '',
            'data'=>$array,
            'from'    => $from,
            'last_page'    => ceil($response['hits']['total']/$per_page),
            'next_page_url'  => '',
            'path'    => '',
            'per_page'    => (int)$per_page,
            'prev_page_url'    => '',
            'to'    =>$to,
            'total'    => $response['hits']['total']
        ];
        return response()
            ->json( [$data], 200);


    }

    /**
     * Download Storage
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function downloadStorage($token){
        $storage = ShareStorageObject::where('token',$token)->first();
        if($storage){
            //@todo Remove this before Beta
            $path = 'test';
            if($storage->storage->created_at > '2018-01-03 06:45:08'){
                $path = $storage->storage->company->storage_folder_name;
            }

            if($storage->token_expire < Carbon::now()){
                return response()
                    ->json(['Link has been expired!'], 404);
            }
            $stream = Storage::disk()->getDriver()->readStream($path.'/'.$storage->storage->s3_name);
            $headers= [
                'Cache-Control'=>'public',
                'Content-Description'=>'File Transfer',
                'Content-Disposition'=>'attachment; filename="'. $storage->storage->getFullFileName(). '"',
                'Content-Type'=>$storage->storage->mime_type,
            ];
            return response()->stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, $headers);

        }

        return response()
            ->json([ 'Link not found'], 404);

    }
    /**
     * DirectDownload Storage
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function directDownload($id){

        $storage = StorageObject::where('id',$id)->first();

        if($storage){
            $path = 'test';
            if($storage->created_at > '2018-01-03 06:45:08'){
                $path = $storage->company->storage_folder_name;
            }
            $stream = Storage::disk()->getDriver()->readStream($path.'/'.$storage->s3_name);
            //$assetPath = Storage::disk()->url('test/'.$storage->s3_name);
            $headers= [
                'Cache-Control'=>'public',
                'Content-Description'=>'File Transfer',
                'Content-Disposition'=>'attachment; filename="'. $storage->getFullFileName(). '"',
                'Content-Type'=>$storage->mime_type,
            ];
            return response()->stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, $headers);

        }

        return response()
            ->json([ 'Link not found' ], 404);

    }

    /**
     * Storage downloadLink
     *
     * @param ShareStorageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function downloadLink(ShareStorageRequest $request){
        //@todo check access storage Object if not owner
        $storage = StorageObject::find($request->storage_object_id);
        if(empty($storage)){
            return response()
                ->json(['error' => 'document not exist'], 404);
        }
        if(!$this->accessOwnered($storage)){
            return response()
                ->json(['error' => 'Link has not been ownered'], 201);
        }

        $attributes=[
            'storage_object_id'=> $storage->id,
            'token'=> strtolower(str_random(20)),
            'token_expire'=> Carbon::now()->addHour(23),
        ];

        $shareStorage = ShareStorageObject::create($attributes);
        $link = [
            'success'=>'Link has been created',
            'link'=> url('download/'.$shareStorage->token)
        ];
        return response()
            ->json([ $link], 201);
    }

    private function accessOwnered($storage){

        if($storage->user_id == Auth::user()->id){
            return true;
        }
        foreach ($storage->storageAccess as $key => $accessStorage) {
            if ($accessStorage->share_with == 'company') {
                if(Auth::user()->company_validate($accessStorage->share_id)){
                    return true;
                }
            } else if ($accessStorage->share_with == 'team') {
                if(Auth::user()->team_validate($accessStorage->share_id)){
                    return true;
                }
            } else if($accessStorage->share_with == 'user') {
                if(!empty(Auth::user()->id == $accessStorage->share_id)){
                    return true;
                }
            }
            return false;
        }

    }

    /**
     * Storage Add
     *
     * @param StorageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorageRequest $request){

        ini_set('max_execution_time', 0);

        $hasCompany = Auth::user()->company_validate($request->get('company_id'));
        if(empty($hasCompany)){
            return response()
                ->json(['status' => 'User not exist in company'], 401);
        }
        $company = Company::find($request->get('company_id'));
        if(!$request->hasFile('file')){
            throw new HttpException(500);
        };
        //$location = new Location();
        $location =[];
        //$userLocation = $location->get($request->ip());
        $parent_id = null;
        $storagefile = $request->file;

        $parent_id = $request->folder_id ?? null;

        //find Parent id from path
        if(!empty($request->path)){
            $path = $request->path;
            $directories = explode("/",$path);
            array_pop($directories);
            foreach ($directories as $directory)
            {
                $slug = str_slug($directory);
                $attributes=[
                    'user_id'=>Auth::user()->id,
                    'company_id'=>$company->id,
                    'parent_id'=>$parent_id,
                    's3_name'=> $slug,
                    'title'=> $directory,
                    'slug'=>$slug,
                    'size'=>'',
                    'type'=>'folder',
                    'file_type'=>'',
                    'ip'=>'',
                    'lat'=>'',
                    'lag'=>'',
                    'city'=>'',
                    'mime_type'=>'',

                ];
                $storageObject = StorageObject::firstOrNew(['slug' => $slug, 'user_id'=>Auth::user()->id, 'company_id'=>$company->id, 'parent_id'=>$parent_id],$attributes );
                $storageObject->save();
                $parent_id = $storageObject->id;
            }
        }
        //Get lat and lonn in case if storage is image
        if(substr($storagefile->getMimeType(), 0, 5) == 'image') {

            $data = Image::make($storagefile->getRealPath())->exif();
            if(!empty($data['GPSLatitudeRef'])) {
                $location = $this->get_image_location($data);
            }

        }
        $fileObject = Storage::disk()->put($company->storage_folder_name,$storagefile);
        $mimeType=  $storagefile->getMimeType();
        if($storagefile->getMimeType() == 'inode/x-empty') {
            $mimeType= 'text/plain';
        }

        $attributes=[
            'user_id'=>Auth::user()->id,
            'company_id'=>$request->company_id,
            'parent_id'=>$parent_id,
            's3_name'=>basename($fileObject),
            'title'=> basename ( $storagefile->getClientOriginalName(), '.' . $storagefile->getClientOriginalExtension()),
            'slug'=>str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $storagefile->getClientOriginalName())),
            'size'=>$storagefile->getClientSize(),
            'file_type'=>$storagefile->getClientOriginalExtension(),
            'ip'=>$request->ip(),
            'lat'=>$location['lat'] ?? '',
            'lag'=>$location['lon'] ??'',
            'city'=>'',
            'mime_type'=>$mimeType,
        ];

        $node = StorageObject::create($attributes);
        if( $node->save()){
           ExtractText::dispatch($node)->onQueue('default');
            //event(new UploadedToS3($node,'web'));
        }else{
            throw new HttpException(500);
        }

        if(!empty($request->get('share_with')) && !empty($request->get('share_id'))){
            $accessAttributes=[
                'storage_object_id'=>$node->id,
                'share_with'=>$request->get('share_with'),
                'share_id'=>$request->get('share_id'),
                'read'=>1,
            ];
            AccessStorageObject::create($accessAttributes);
        }
        return response()
            ->json(['data' => $node,'listing'=>$this->getListings($request)], 201);
    }
    private function gps2Num($coordPart){
        $parts = explode('/', $coordPart);
        if(count($parts) <= 0)
            return 0;
        if(count($parts) == 1)
            return $parts[0];
        return floatval($parts[0]) / floatval($parts[1]);
    }
    private function get_image_location($exif){
        if($exif){
            $GPSLatitudeRef = $exif['GPSLatitudeRef'];
            $GPSLatitude    = $exif['GPSLatitude'];
            $GPSLongitudeRef= $exif['GPSLongitudeRef'];
            $GPSLongitude   = $exif['GPSLongitude'];

            $lat_degrees = count($GPSLatitude) > 0 ? $this->gps2Num($GPSLatitude[0]) : 0;
            $lat_minutes = count($GPSLatitude) > 1 ? $this->gps2Num($GPSLatitude[1]) : 0;
            $lat_seconds = count($GPSLatitude) > 2 ? $this->gps2Num($GPSLatitude[2]) : 0;

            $lon_degrees = count($GPSLongitude) > 0 ? $this->gps2Num($GPSLongitude[0]) : 0;
            $lon_minutes = count($GPSLongitude) > 1 ? $this->gps2Num($GPSLongitude[1]) : 0;
            $lon_seconds = count($GPSLongitude) > 2 ? $this->gps2Num($GPSLongitude[2]) : 0;

            $lat_direction = ($GPSLatitudeRef == 'W' or $GPSLatitudeRef == 'S') ? -1 : 1;
            $lon_direction = ($GPSLongitudeRef == 'W' or $GPSLongitudeRef == 'S') ? -1 : 1;

            $latitude = $lat_direction * ($lat_degrees + ($lat_minutes / 60) + ($lat_seconds / (60*60)));
            $longitude = $lon_direction * ($lon_degrees + ($lon_minutes / 60) + ($lon_seconds / (60*60)));

            return array('lat'=>number_format($latitude,6), 'lon'=>number_format($longitude,6));
        }else{
            return false;
        }
    }
    /**
     * Storage Directory
     *
     * @param StorageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDirectory(DirectoryStoreRequest $request){
        $company = Company::find($request->company_id);
        //print_r($request->paths);exit;
        //print_r($request->paths);exit;
        $parent_id = $request->folder_id ?? null;
        $parent_id_cpy = $request->folder_id ?? null;

        foreach($request->paths as $key=> $folder){
            $path = $folder ?? null;
            $directories = explode("/",$path);
            //Folders insertion
            array_pop($directories);
            foreach($directories as $directory){
                $slug = str_slug($directory);
                $attributes=[
                    'user_id'=>Auth::user()->id,
                    'company_id'=>$company->id,
                    'parent_id'=>$parent_id,
                    's3_name'=> $slug,
                    'title'=> $directory,
                    'slug'=>$slug,
                    'type'=>'folder',
                ];
                $storageObject = StorageObject::firstOrNew(
                    ['slug' => $slug,
                        'user_id'=>Auth::user()->id,
                        'company_id'=>$company->id,
                        'parent_id'=>$parent_id],$attributes
                );
                $storageObject->save();
                $parent_id = $storageObject->id;
            }
            $parent_id = $parent_id_cpy;
        }

        // id of the last directory, hint this is the also the parent id needed for the file
        $storageObject= StorageObject::with(['owner','tags','topics','company'])
            ->where(['user_id'=>Auth::user()->id])
            ->where(['parent_id'=>$parent_id_cpy])
            ->orderBy('type','desc')
            ->orderBy('created_at','desc')
            ->paginate(10);
        return $this->processListing($storageObject);
    }

    public function update(){
        $attributes=[
            'parent_id'=>6,
            'title'=>'test5',
            'slug'=>'test5',
        ];
        $node = new StorageObject($attributes);
        $node->save();
    }

}
