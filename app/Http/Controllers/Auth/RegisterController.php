<?php

namespace FAF\Http\Controllers\Auth;

use FAF\SocialProvider;
use FAF\User;
use FAF\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use League\Flysystem\Exception;
use Mail;
use FAF\Mail\verifyEmail;
use Session;
use Snowfire\Beautymail\Beautymail;
use Laravel\Socialite\Facades\Socialite;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        Session::flash('status','Registered! Verify You email to activate your account.');
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'verify_token' => Str::random(40),
        ]);
        $thisUser = User::findOrFail($user->id);
        $this->SendEmail($thisUser);
        return $user;
    }
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

       // $this->guard()->login($user);
        return redirect(route('login'));

    }

    public function VerifyEmailFirst()
    {
        return view('email.VerifyEmailFirst');
    }

    public function SendEmail($thisUser)
    {
        //Mail::to($thisUser['email'])->send(new verifyEmail($thisUser));
        $data = [
            'subject'=> 'Verify Your Account!',
            'email'=> $thisUser['email'],
            'username'=> $thisUser['name'],
            'salute'=>'Welcome',

        ];
        $data['message'] ='<p>You have been registered successfully.</p>';
        $data['message'] .='<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>';
        $data['message'] .='<br><p  class="btn-verify-p">Please verify your account</p><p class="btn-verify-p-c"><a class="btn-verify" href="'.route('EmailDone',['email'=>$thisUser->email,'verifyToken'=>$thisUser->verify_token]).'">Verify Now</a></p>';
        $data['message'] .='<br><br><p>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('email.welcome', ['data'=>$data], function($message) use ($data)
        {
            $message
                ->from('info@craft.com','Craft')
                ->to($data['email'], $data['username'])
                ->subject($data['subject']);
        });



    }

    public function EmailDone($email, $verifyToken)
    {
        $user = User::where(['email'=>$email, 'verify_token'=>$verifyToken])->first();
        if($user){
            $result=  User::where(['email'=>$email, 'verify_token'=>$verifyToken])->update(['status'=>1, 'verify_token'=>NULL]);
            if($result == 1){
                return redirect(route('login'));
            }else{
                return 'User Not verify';
            }

        } else{
            return 'User Not Found';
        }


    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try{
            $socialUser = Socialite::driver($provider)->user();

        }catch(Exception $e){
            return redirect('/');

        }
        $socialProvider = SocialProvider::where('provider_id',$socialUser->getId())->first();

        if(!$socialProvider){
            $user = User::where('email',$socialUser->getEmail())->first();
            if ($user){
                $user->soicalProviders()->create(
                    [
                        'provider_id'=> $socialUser->getId(),
                        'provider'=> $provider
                    ]
                );
            }
            if (!$user){
                $user = User::firstOrCreate(
                    [
                        'email'=>$socialUser->getEmail(),
                        'name'=>$socialUser->getName(),

                    ]
                );

                $user->soicalProviders()->create(
                    [
                        'provider_id'=> $socialUser->getId(),
                        'provider'=> $provider
                    ]
                );
            }


        }else{
            //dd($socialProvider);
            $user = $socialProvider->user;
        }

        auth()->login($user);

        return redirect('/home');
        // $user->token;
    }
}
