<?php

namespace FAF\Http\Controllers;

use FAF\Http\Requests\TeamMemberGetRequest;
use FAF\Http\Requests\TeamMemberListingRequest;
use FAF\Team;
use FAF\TeamMember;
use FAF\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamMemberController extends Controller
{
    /**
     * Team Member Add
     *
     * @param TeamMemberGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TeamMemberGetRequest $request)
    {
        $userExist = User::where('id',$request->user_id)->count();
        if($userExist == 0){
            return response()
                ->json(['status' => 'User doesnot exist'], 401);
        }

        $teamExist = Team::where('id',$request->team_id)->count();
        if($teamExist == 0){
            return response()
                ->json(['status' => 'Team doesnot exist'], 401);
        }

        $request->request->add(['user_id' => Auth::user()->id]);
        $teamMember = TeamMember::firstOrCreate($request->all());
        $teams = TeamMember::with('team')
            ->find($teamMember->id)
            ->paginate(10);

        return response()
            ->json(['data' => $teamMember,'listing'=>$teams], 201);
    }

    /**
     * Team Member Listing
     *
     * @param TeamMemberListingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(TeamMemberListingRequest $request)
    {
        $teamMembers = TeamMember::with('team')
            ->where('company_id',$request->company_id)
            ->where('user_id',Auth::user()->id)
            ->paginate(10);
        return response()
            ->json($teamMembers, 201);
    }
}
