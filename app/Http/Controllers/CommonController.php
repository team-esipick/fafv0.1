<?php

namespace FAF\Http\Controllers;

use FAF\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use FAF\Team;
use FAF\TeamMember;
use Snowfire\Beautymail\Beautymail;
use Illuminate\Support\Facades\Auth;
class CommonController extends Controller
{

    function verifyTeamMember($email,$verifyToken){
        Auth::logout();
        $user = User::where(['email'=>$email, 'password_set_token'=>$verifyToken])->first();
        $data['token'] = $verifyToken;
        $data['email'] = $email;
        if($user){
            return view('auth.passwords.setpassword',$data);

            /*$result=  User::where(['email'=>$email, 'password_set_token'=>$verifyToken])->update(['status'=>1, 'password_set_token'=>NULL]);
            if($result == 1){
                return redirect(route('login'));
            }else{
                return 'User Not verify';
            }*/

        }
        abort(404);

    }
    function verifyTeamMemberPost(Request $request){
        Auth::logout();
        $user = User::where(['email'=>$request->email, 'password_set_token'=>$request->token])->first();
        if($user){

            $result =  User::where(['email'=>$request->email, 'password_set_token'=>$request->token])->update(['status'=>1,'password_set_token' =>NULL,'password'=>bcrypt($request->password)]);
            $teamMember = TeamMember::where('user_id',$user->id)->where('status','pending')->first();
            if(count($teamMember)){
                $teamMember->status = 'active';
                $teamMember->save();
            }
            if($result == 1){
                $request->session()->flash('message',['type'=>'success','message'=>'You can login now!'] );
                return redirect(route('login'));
            }else{
                $request->session()->flash('message',['type'=>'error','message'=>'Something went wrong try again!'] );
                return redirect(route('login'));
            }
        }
        $request->session()->flash('message',['type'=>'error','message'=>'Something went wrong try again!'] );
        return redirect(route('login'));
    }
}
