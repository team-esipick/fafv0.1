<?php

namespace FAF\Http\Controllers;

use FAF\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use FAF\User;
use FAF\Post;
use Snowfire\Beautymail\Beautymail;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('admin.home');
    }

    public function userStoreForm(){
        return view('admin.user.addUser');
    }

    public function users(){
        $users = User::orderBy('id', 'desc')->paginate(10);
        $data = [
            'title' => '',
            'users' => $users
        ];

        return view('admin.user.user')->with($data);
    }

    public function userEdit($id)
    {
        $user = User::where('id', $id)->first();
        //dd($user);
        $data = [
            'title' => '',
            'user' => $user
        ];
        return view('admin.user.editUser')->with($data);
    }

    public function userDelete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/admin/users');
    }

    public function searchUsers(Request $request){
        $users = User::where('name', 'like', '%'.$request->keyword.'%')->orderBy('id', 'desc')->paginate(10);
        $data = [
            'title' => '',
            'users' => $users
        ];

        return view('admin.user')->with($data);
    }

    public function userUpdate($id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'fullName' => 'required|max:255',
            'role' => 'required',
            'email' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/user/edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($id);
        $user->name = $request->fullName;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->status = $request->status;

        $user->save();

        $request->session()->flash('message', 'User Update successfully!');
        return redirect('/admin/users');
    }


    public function userStore(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/addUser')
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->status = !empty($request->active)?1:0;
        $user->role = $request->role;
        $user->save();

        if(!empty($request->email_send)){
            //Send email
            $data = [
                'subject'=> 'Signup Confirmation!',
                'email'=>$user->email,
                'username'=> $user->name,
                'salute'=>'Dear',

            ];

            $data['message'] ='Your account has been successfully added at Craft.<br><stong>email:</stong> '.$user->email.'<br><strong>Password:</strong>'.$request->password.'.';

            $beautymail = app()->make(Beautymail::class);
            $beautymail->send('email.welcome', ['data'=>$data], function($message) use ($data)
            {
                $message
                    ->from('info@craft.com','Craft')
                    ->to($data['email'], $data['username'])
                    ->subject($data['subject']);
            });
        }


        $request->session()->flash('message', 'Customer successfully Added!');
        return redirect('/admin/addUser');

    }


    //Category CRUD

    public function categoryStoreForm(){
        $categories= Category::orderBy('id', 'desc')->get();
        $data = [
            'title' => '',
            'categories' => $categories,

        ];

        return view('admin.category.addCategory')->with($data);
    }

    public function categories(){

        //$categories  =  Category::with('children')->whereNull('parent_id')->orderBy('name', 'asc')->get();

       // dd($categories);
        $categories = Category::orderBy('id', 'desc')->paginate(10);
        $data = [
            'title' => '',
            'categories' => $categories,

        ];

        return view('admin.category.category')->with($data);
    }

    public function categoryEdit($id)
    {
        $categories= Category::orderBy('id', 'desc')->get();
        $category = Category::where('id', $id)->first();
        //dd($user);
        $data = [
            'title' => '',
            'category' => $category,
            'categories' => $categories
        ];
        return view('admin.category.editCategory')->with($data);
    }

    public function categoryDelete($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('/admin/categories');
    }

    public function searchCategories(Request $request){
        $categories = Category::where('name', 'like', '%'.$request->keyword.'%')->orderBy('id', 'desc')->paginate(10);
        $data = [
            'title' => '',
            'categories' => $categories
        ];

        return view('admin.category.category')->with($data);
    }

    public function categoryUpdate(Request $request)
    {
        $id = $request->id;
//dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect('admin/category/edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        }
        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = (empty($request->parent_id))?'':$request->parent_id;
        $category->slug = str_slug($request->name, '-');
        $category->status = !empty($request->active)?1:0;

        $category->save();
       // dd($category);

        $request->session()->flash('message', 'Category Update successfully!');
        return redirect('/admin/categories');
    }


    public function categoryStore(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required',
            'parent_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/addCategory')
                ->withErrors($validator)
                ->withInput();
        }

        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->parent_id;
        $category->slug = str_slug($request->name, '-');
        $category->status = !empty($request->active)?1:0;

        $category->save();


        $request->session()->flash('message', 'Category successfully Added!');
        return redirect('/admin/addCategory');

    }

    /////////////////////////////////////////////POST/////////////////////////////////////////////////
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function posts(Request $request)
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        //
        $data = [
            'title' => '',
            'posts' => $posts,

        ];
        return view('admin.posts.post')->with($data);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createPost(Request $request)
    {
        //
          return view('admin.posts.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storePost(Request $request)
    {
       // dd($request->all());
        $post = new Post();
        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->slug = str_slug($post->title);
        if(!empty($request->file('thumbnail'))){
            $path = $request->file('thumbnail')->store('thumnails');
            if(!empty($path))$post->image = basename($path);
        }


        $duplicate = Post::where('slug',$post->slug)->first();
        if($duplicate)
        {
            return redirect('admin/addPost')->withErrors('Title already exists.')->withInput();
        }

        $post->author_id = $request->user()->id;
        if($request->has('save'))
        {
            $post->active = 0;
            $message = 'Post saved successfully';
        }
        else
        {
            $post->active = 1;
            $message = 'Post published successfully';
        }
        $post->save();
        return redirect('admin/editPost/'.$post->slug)->withMessage($message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editPost(Request $request,$slug)
    {
        $post = Post::where('slug',$slug)->first();
        if($post)
            return view('admin.posts.edit')->with('post',$post);
        else
        {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updatePost(Request $request)
    {
        //
        $post_id = $request->input('post_id');
        $post = Post::find($post_id);
        if($post)
        {
            $title = $request->input('title');
            $slug = str_slug($title);
            $duplicate = Post::where('slug',$slug)->first();

            if($duplicate)
            {
                if($duplicate->id != $post_id)
                {
                    return redirect('/admin/editPost/'.$post->slug)->withErrors('Title already exists.')->withInput();
                }
                else
                {
                    $post->slug = $slug;

                }
            }

            $post->title = $title;
            $post->body = $request->input('body');
            if(!empty($request->file('thumbnail'))){
                $path = $request->file('thumbnail')->store('thumnails');
                if(!empty($path))$post->image = basename($path);
            }

            if($request->has('save'))
            {
                $post->active = 0;
                $message = 'Post saved successfully';
                $landing = 'admin/editPost/'.$post->slug;
            }
            else {
                $post->active = 1;
                $message = 'Post updated successfully';
                $landing = "/admin/posts";
            }
            $post->save();
            return redirect($landing)->withMessage($message);
        }
        else
        {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyPost(Request $request, $id)
    {
        //
        $post = Post::find($id);
        if($post)
        {
            $post->delete();
            $data['message'] = 'Post deleted Successfully';
        }
        else
        {
            $data['errors'] = 'Invalid Operation. You have not sufficient permissions';
        }

        return redirect('/admin/posts')->with($data);
    }

}
