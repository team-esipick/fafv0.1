<?php

namespace FAF\Http\Controllers;

use FAF\Http\Requests\TagAddRequest;
use FAF\Http\Requests\TagDeleteRequest;
use FAF\Http\Requests\TagGetRequest;
use FAF\Http\Requests\TagSearchRequest;
use FAF\Http\Requests\TagUpdateRequest;
use FAF\Libs\Contracts\IndexerInterface;
use FAF\StorageObject;
use FAF\Tag;
use FAF\TagStorageObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{

    protected $Indexer;
    protected $TagModel;
    public function __construct(IndexerInterface $indexer, Tag $TagModel)
    {
        $this->Indexer = $indexer;
        $this->TagModel = $TagModel;
    }

    /**
     * Tag Add
     *
     * @param TagAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TagAddRequest $request){

        $request->request->add(['slug' => str_slug($request->slug)]);

        $EsTags=[];


        $EsTags = $this->TagModel->store($request->all());


        return response()->json($EsTags, 201);
    }

    /**
     * Tag Update
     *
     * @param TagUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TagUpdateRequest $request){
        $tag = TagStorageObject::find($request->id);
        $EsTags =[];
        if(empty($tag)){
            return response()->json('not found', 401);
        }
        $tag->status = ($request->status == 'correct')?1:2;
        $tag->save();
        if($request->status == 'incorrect'){
            $EsTags = $this->storeTagsES($tag->storage_object_id);
        }

        return response()->json(empty($EsTags)?$tag:$EsTags, 201);
    }

    private function storeTagsES($storage_object_id){
        $EsTags=[];
        $allCorrectTags = TagStorageObject::where(['storage_object_id'=>$storage_object_id])->whereIn('status',[0,1])->get();
        foreach ($allCorrectTags as $tags){
            $EsTags[]=$tags->tag->title;
        }
        $this->Indexer->updateTags($storage_object_id,$EsTags );
        return $EsTags;
    }

     /**
     * Tag get
     *
     * @param TagGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(TagGetRequest $request){

        return response()->json(['success' => $request->all()], 201);
    }

    /**
     * Tag Search
     *
     * @param TagSearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(TagSearchRequest $request){
        $tags = Tag::search($request->title)->take(10)->get();
        $data=[];
        foreach($tags as $key=>$tag){
            $data[$key]['value'] = $tag->id;
            $data[$key]['display'] = $tag->title;
        }
        return response()->json($data, 201);
    }

    /**
     * Tag delete
     *
     * @param TagDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TagDeleteRequest $request){
       $tag =  TagStorageObject::find($request->id);
        $tag->delete();
        return response()->json($request->all(), 201);
    }

}
