<?php

namespace FAF\Http\Middleware;

use Closure;
use Auth;
use FAF\TeamMember;

class SubscribedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if (!$request->user()->subscribed('main')) {
           if(empty($request->user()->team_id)){
               return redirect('/subscription');
           }
        }
        return $next($request);

    }
}
