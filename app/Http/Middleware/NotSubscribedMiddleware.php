<?php

namespace FAF\Http\Middleware;

use Closure;
use Auth;

class NotSubscribedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->subscribed('main') && empty(Auth::user()->team_id)) {
            return $next($request);
        }
        return redirect('/home');

    }
}
