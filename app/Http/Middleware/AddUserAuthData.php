<?php

namespace FAF\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AddUserAuthData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->request->add([ 'user_id' => Auth::user()->id ]);
        return $next($request);
    }
}
