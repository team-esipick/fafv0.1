<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;
class TeamMemberGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'=>'required|numeric',
            'user_id'=>'required|numeric',
            'team_id'=>'required|numeric',
            'status'=>'boolean',
        ];
    }
    public function messages()
    {
        return [
            'company_id.required'=>'Company ID is required',
            'user_id.required'=>'User Id is required.',
            'team_id.required'=>'Team Id is required.'
        ];
    }
}
