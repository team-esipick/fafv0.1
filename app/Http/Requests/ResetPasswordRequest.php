<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'token.required'  => 'Please enter token',
            'email.required'  => 'Please enter email address',
            'email.email'  => 'Please enter a valid email address.',
            'password.required'  => 'Please enter password',
            'password_confirmation.same' => 'Confirm password should match the Password',
            'password_confirmation.required'  => 'Please enter confirm password',
        ];
    }
}
