<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class StorageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'=>'required|numeric',
            'file'=>'required|file',
            'share_with'=>'string',
            'share_id'=>'numeric',
            'folder_id'=>'numeric',
            'path'=>'string'
        ];
    }
    public function messages()
    {
        return [
            'company_id.required'=>'Company ID is required',
            'file.required'=>'There are no files. Please click on “Upload Documents” to upload files.'
        ];
    }
}
