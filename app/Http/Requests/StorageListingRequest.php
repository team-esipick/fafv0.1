<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class StorageListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'=>'numeric',
            'page'=>'numeric',
            'sort'=>'string',
            'size'=>'string',
            'per_page'=>'numeric'
        ];
    }
}
