<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'=>'required|numeric',
            'keyword'=>'required|string',
            'from_date'=>'date_format:"Y-m-d"',
            'to_date'=>'date_format:"Y-m-d"',
            'sort_type'=>'in:date,size',
            'sort_order'=>'in:asc,desc',
            'tags'=>'array',
            'topics'=>'array',
            'types'=>'array',
            'locations'=>'array',
            'distance'=>'string',
            'page'=>'integer',
            'per_page'=>'integer',
            'highlights'=>'boolean',
            'aggregates'=>'boolean'
        ];
    }

    public function messages()
    {
        return [
            'sort_type.in'  => 'Type should be date or size',
            'sort_order.in'  => 'Order should be asc or desc',
        ];
    }
}
