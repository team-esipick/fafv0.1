<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class CompanyVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'company_name'=>'required|unique:companies,name'
        ];
    }
    public function messages()
    {
        return [
//            'company_name.required'  => 'Please enter company name.',
            'company_name.unique'  => 'This company name is already taken',
        ];
    }

}
