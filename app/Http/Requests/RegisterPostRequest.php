<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class RegisterPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'company_name' => 'required|max:255|unique:companies,name',
            'first_name' => 'required|max:255|min:1',
            'last_name' => 'required|max:255|min:1',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'sub_domain' => 'required|unique:companies',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'company_name' => 'Please enter Company Name',
            'first_name.required' => 'Please enter First name.',
            'first_name.max' => 'First name should not be greater then 255 characters',
            'first_name.min' => 'First name should be greater then 5 characters',
            'email.required'  => 'Please enter email address ',
            'last_name.required' => 'Please enter Last name.',
            'last_name.max' => 'Last name should not be greater then 255 characters',
            'last_name.min' => 'Last name should be greater then 5 characters',
            'email.required'  => 'Please enter email address ',
            'email.email'  => 'Please enter a valid email address.',
            'c_password.required'  => 'Please enter Confirm password',
            'c_password.same' => 'Confirm password should match the Password',
            'password'  => 'Please enter Password',
            'passwordrequired'  => 'Please enter Password',
        ];
    }
}
