<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class WebsiteVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_domain'=>'required|string'
        ];
    }
    public function messages()
    {
        return [
            'sub_domain.required'=>'Please enter the site name which would be used to create the dedicated URL for your enterprise'
        ];
    }
}
