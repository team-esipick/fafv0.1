<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;
use FAF\User;
use Illuminate\Validation\Rule;
class LoginPostRequest extends FormRequest
{
    /**
     * Get the validaation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
            'sub_domain' => 'required',
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

            'email.required'  => 'Please enter email address',
            'email.email'  => 'Please enter a valid email address.',
            'password.required'  => 'Please enter Password',

        ];
    }







}
