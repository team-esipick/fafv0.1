<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class EmailVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'email'=>'required|email|unique:users'
        ];
    }
    public function messages()
    {
        return [
            'email.required'  => 'Please enter email address.',
            'email.unique'  => 'The email address already exists. If you think this is an error then 
            please contact support.',
            'email.email'  => 'Please enter a valid email address.',
        ];
    }

}
