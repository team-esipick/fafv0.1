<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class TeamGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'=>'required|numeric',
            'id'=>'required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'company_id.required'=>'Company ID is required',
            'id.required'=>'Team Id is required.'
        ];
    }
}
