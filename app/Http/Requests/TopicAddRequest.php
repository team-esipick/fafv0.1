<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class TopicAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'=>'required|numeric',
            'storage_object_id'=>'required|numeric',
            'title'=>'required|string',
        ];
    }
    public function messages()
    {
        return [
            'company_id.required'=>'Company ID is required',
            'title.required'=>'Please enter Title',
        ];
    }
}
