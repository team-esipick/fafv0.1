<?php

namespace FAF\Http\Requests;

use Dingo\Api\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required|numeric',
            'first_name' => 'required|max:255|min:1',
            'last_name' => 'required|max:255|min:1',
            'email' => 'required|email|unique:users'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter First name.',
            'first_name.max' => 'First name should not be greater then 255 characters',
            'first_name.min' => 'First name should be greater then 5 characters',
            'email.required'  => 'Please enter email address ',
            'last_name.required' => 'Please enter Last name.',
            'last_name.max' => 'Last name should not be greater then 255 characters',
            'last_name.min' => 'Last name should be greater then 5 characters',
            'email.required'  => 'Please enter email address ',
            'email.email'  => 'Please enter a valid email address.'

        ];
    }
}
