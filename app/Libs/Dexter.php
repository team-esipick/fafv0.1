<?php
/**
 * Created by PhpStorm.
 * User: usama
 * Date: 11/22/17
 * Time: 2:57 PM
 */

namespace FAF\Libs;
use Dompdf\Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


class Dexter
{

    public $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function analyze($id,$text){

        try{

            $ml_data = $this->client->request('POST',env('Dexter_HOST'),
                    ['json' => ['id' => $id ,
                        'text' => $text,
                    ]
                ]
            );

            if ( in_array($ml_data->getStatusCode(), range(400, 550) )) {
                throw new Exception($ml_data->getBody());
            }
            //@todo check if ml proposed that data or not properly
            return jsonDecode($ml_data->getBody());

        }catch (\Exception $e){

            throw  new Exception($e->getMessage());
        }
    }

}