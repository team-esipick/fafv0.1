<?php

namespace FAF\Libs\Contracts;


use phpDocumentor\Reflection\Types\Integer;

Interface IndexerInterface {


public function store($index,$type,$id,$body);
public function searchBool($index,$q,$company_name,$sort_order ='asc', $sort_type ='date',$from=0,$size=10,$from_date=null, $to_date= null,$tags=[],$topics=[], $departments=[]);
public function search($index,$q,$company_name,$sort_order ='asc', $sort_type ='date',$from=0,$size=10,$from_date=null, $to_date= null,$tags=[],$topics=[], $types=[],$locations=[], $distance='');
public function updateTags($id, $body);
public function updateTopics($id, $body);
}