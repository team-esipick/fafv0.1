<?php

namespace FAF\Libs\Contracts;

/**
 * Interface IngestorInterface
 * This interface define a generic method any class implementing
 * document indestor should implement
 * @package FAF\Libs\Contracts
 */
Interface IngestorInterface {


    /**
     * @param $f file path
     * @return void the text of the document
     */
    public function getText($filepath);

    /**
     * @param $f resource a file pointer
     * @return array associated Array with meta information
     */
    public function getMeta($f) : array ;

}