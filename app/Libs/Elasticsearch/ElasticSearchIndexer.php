<?php

namespace FAF\Libs\Elasticsearch;

use Carbon\Carbon;
use Elasticsearch\ClientBuilder;
use FAF\Libs\Contracts\IndexerInterface;
use Illuminate\Support\Arr;

Class ElasticSearchIndexer implements IndexerInterface
{

    public $client;
    public $actions = ['add', 'update', 'remove'];
    public $action;
    const  INDEX_TYPE = 'doc';
    /**
     * @var object
     */
    public $skeys ;

    public function __construct()
    {

        $this->query =arrToObject(
            [
                'keyword' => 'query.bool.must.1.multi_match.query',
                'multi' => [

                    'user_ids' => 'query.bool.must.0.bool.should.1.terms.user_ids',
                    'team_ids' => 'query.bool.must.0.bool.should.2.terms.team_ids',
                    'company_ids' => 'query.bool.must.0.bool.should.3.terms.company_ids',
                    'extn' => 'query.bool.filter.0.terms.file_extn',
                    'tag' => 'query.bool.filter.1.terms.tags',
                    'topic' => 'query.bool.filter.2.terms.topic',




                ],
                'single' =>[

                    'user_id' => 'query.bool.must.0.bool.should.0.term.user_id.value',

                ],
                'size' => 'size',
                'from' => 'from',
                'date_gte' => 'query.bool.filter.3.range.created_date.gte',
                'date_lte' => 'query.bool.filter.3.range.created_date.lte',
                'filter' => 'query.bool.filter',
                'highlight' => 'highlight',
                'sort' => 'sort',


            ]);

        //@todo consult expert for optimal connection pool
        $this->client = ClientBuilder::create()
            ->setConnectionPool('\Elasticsearch\ConnectionPool\SniffingConnectionPool', [])
            ->setHosts(['host' => env('ELASTIC_HOST')])
            ->build();


    }
    public function getSearchQuery(){
        $json_search = file_get_contents(__DIR__ . '/search.json');
        return jsonDecode($json_search,true);
    }
    private static function getSettings(){

        $json_settings = file_get_contents(__DIR__.'/settings.json');
        return jsonDecode($json_settings,true);
    }

    /**
     * @return mixed
     *
     */
    private static function getMapping(){
        $json_mapping = file_get_contents(__DIR__.'/mappings.json');
        return jsonDecode($json_mapping,true);

    }
    private static function getAliases(){
        return ['aliases' => [
            'storage_objects' => new \stdClass()
        ]];
    }

    private static function getHighlights(){
        return
            [
                'fields'  => [
                    'short_description' => new \stdClass(),
                    'description' => new \stdClass(),
                    'title' => new \stdClass(),
                    'tags' => new \stdClass(),
                    'file_name' => new \stdClass(),

                ],

            ];

    }
    /**
     * @param $index
     * @param $type
     * @param $id
     * @param $body
     * @return array
     */

    public function store($index, $type, $id, $body)
    {


        $config =  array_merge(self::getAliases(),self::getMapping());
        $param =[
            'index'=>$index,
            'body'=> $config

        ];


        if($this->client->indices()->exists(['index' => $index]) === false){
            $this->client->indices()->create($param);
        }

        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => $body
        ];


        if($this->client->exists(['index' => $index, 'id'=>$id, 'type'=>$type]) === false){
            return $this->client->index($params);
        }else{
            $params['body'] = ['doc'=>$params['body']];
            return $this->client->update($params);
        }

    }

    /**
     * @param $index
     * @param null $type
     * @param null $id
     * @return array|void
     */
    public function delete($index, $type = null, $id = null)
    {
        if($id == null){
            $this->client->indices()->delete(['index' => $index ]);
            return;
        }
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id
        ];
        //Delete doc at /my_index/my_type/my_id
        return $this->client->delete($params);

    }

    /**
     * @todo read multi_match
    /*https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html
    /* The main search method for querying elastic search for docs
     *
     * @param $index
     * @param $q
     * @param $company_name
     * @param string $sort_order
     * @param string $sort_type
     * @param int $from
     * @param int $size
     * @param null $from_date
     * @param null $to_date
     * @param array $tags
     * @param array $topics
     * @param array $doc_types
     * @return array
     */
    public function searchBool($index, $q,$company_name,$sort_order ='desc', $sort_type ='size',$from=0,$size=10,$from_date = null, $to_date =null, $tags=[], $topics=[], $doc_types=[]){
        $range =[];
        $must =[];
        $filter = [];
        $filter []= [ 'match'  => [ 'user_id' =>$company_name ] ];

        if(!empty($from_date)){
            $from_date = Carbon::parse($from_date)->format('Y-m-d');
            $to_date = !empty($to_date)?Carbon::parse($to_date)->format('Y-m-d'):Carbon::now()->addDay(100)->format('Y-m-d');
            $range =  ['range'=>[
                'created_at'=> [
                    'gte'=>$from_date,
                    'lte'=>$to_date,
                    'format'=>'yyyy-MM-dd||yyyy'
                ]
            ]];
        }

        if (count($range)  !== 0 ){
            $filter[] = $range;
        }

        // Set Must incase of refines
        if(!empty($tags) || !empty($topics)  || !empty($doc_types) ){
            $refines = [];

            $refines[] = ['match' => [ 'user_id' =>$company_name]];

            if(!empty($tags)){
                $refines[]=['terms' => [ 'tags' => $tags]];
            }
            if(!empty($topics)){
                $refines[]=['terms' => [ 'topics' =>$topics]];
            }
            if(!empty($doc_types)){
                $refines[]=['terms' => [ 'doc_type' => $doc_types]];
            }
            $must = $refines;

        } else{
            $must = [ 'match' => [ 'user_id' =>$company_name]];
        }
        $params = [
            'index' => $index,
            'type' => 'string',
            'body' => [
                'query' => [
                    'bool' => [

                        'must' => [

                            'query_string' => [
                                'fields' =>  [ 'description' , 'title', 'tags', 'topics' ],
                                'query' =>  $q ,
                                'analyze_wildcard' => true

                            ],


                        ],

                        'filter' => $must

                    ]

                ],
                'aggregations' => [
                    'tags' =>  [
                        'terms' =>  ["field" => "tags"],

                    ],
                    'types' =>  [
                        'terms' =>  ["field" => "doc_type"],

                    ],
                    'topics' =>  [
                        'terms' =>  ["field" => "topics"],

                    ]

                ],

                'sort' => [
                    [ $sort_type => ['order' => $sort_order] ]
                ],

                'from'=>$from,
                'size'=>$size
            ]
        ];

        return $this->client->search($params);

    }

    /**
     * @todo read multi_match
    /*https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html
    /* The main search method for querying elastic search for docs
     *
     * @param $index
     * @param $q
     * @param $company_name
     * @param string $sort_order
     * @param string $sort_type
     * @param int $from
     * @param int $size
     * @param null $from_date
     * @param null $to_date
     * @param array $tags
     * @param array $topics
     * @param array $doc_types
     * @return array
     */
    public function search($index, $q,$company_name,$sort_order ='desc', $sort_type ='size',$from=0,$size=10,$from_date = null, $to_date =null, $tags=[], $topics=[], $doc_types=[],$locations=[], $distance=''){
        $range =[];
        $must =[];
        $filter = [];
        $filter []= [ 'match'  => [ 'user_id' =>$company_name ] ];
        /*
        $this->query =arrToObject(
            [
                'keyword' => 'query.bool.must.1.multi_match.query',
                'multi' => [

                    'user_ids' => 'query.bool.must.0.bool.should.1.terms.user_ids',
                    'team_ids' => 'query.bool.must.0.bool.should.2.terms.team_ids',
                    'company_ids' => 'query.bool.must.0.bool.should.3.term.company_ids',
                    'extn' => 'query.bool.filter.0.terms.file_extn',
                    'tags' => 'query.bool.filter.1.terms.tags',
                    'topic' => 'query.bool.filter.2.terms.topic',


                ],
                'single' =>[

                    'user_id' => 'query.bool.must.0.bool.should.0.term.user_id.value',

                ],
                'size' => 10,
                'from' => 0,
                'date_gte' => 'query.bool.filter.3.range.created_date.gte',
                'date_lte' => 'query.bool.filter.3.range.created_date.lte',

            ]);

        */
        $query = $this->getSearchQuery();


        if(!empty($from_date)){
            $from_date = Carbon::parse($from_date)->format('Y-m-d');
            $to_date = !empty($to_date)?Carbon::parse($to_date)->format('Y-m-d'):Carbon::now()->addDay(100)->format('Y-m-d');

            Arr::set($query,$this->query->date_gte,$to_date);
            Arr::set($query,$this->query->date_lte,$from_date);
        }
        Arr::set( $query ,$this->query->keyword,$q) ;
        // check if the user has the rigth view rights
        Arr::set($query,$this->query->single->user_id,$company_name);
        Arr::set($query,$this->query->multi->user_ids,[]);
        Arr::set($query,$this->query->multi->team_ids,[]);
        Arr::set($query,$this->query->multi->company_ids,[]);

        // Set Must incase of refi  nes
        $refines = [];
        if(!empty($tags) || !empty($topics)  || !empty($doc_types) || !empty($locations) ){

            if(!empty($tags)){
                $refines[]=['terms' => [ 'tags.keyword' => $tags]];
            }
            if(!empty($topics)){
                $refines[]=['terms' => [ 'topics.keyword' =>$topics]];
            }
            if(!empty($doc_types)){
                $refines[]=['match' => [ 'file_extn' => implode($doc_types,' ')]];
            }
            if(!empty($locations)){
                $resquestLocation = [];
                foreach($locations as $loc){
                    $locData = [];
                    $data =explode( ',',$loc);
                    $locData['lat']=(float)$data[0];
                    $locData['lon']=(float)$data[1];
                    $resquestLocation[]=$locData;
                }
                $refines[]=['geo_distance' => [ 'distance'=>$distance ?? '2000km','locations.locations' => (object)$locData]];
            }
        }
//dd($refines);
        // add tags, topic and extn to search filter
        Arr::set($query,$this->query->filter,$refines);

        //pagniation date
        Arr::set($query,$this->query->size,$size);
        Arr::set($query,$this->query->from,$from);

        //set highlight
        Arr::set($query,$this->query->highlight,self::getHighlights());

        //set sort
        Arr::set($query,$this->query->sort,[ '_score' => new \stdClass() ]);


        $param = [
            'index' => $index,
            'type' => 'doc',
            'body' => $query

        ];
logger(json_encode($param));
       // echo json_encode($param);
       // exit;
        return $this->client->search($param);

    }


    /*
     *  This can be used as in this fashion
     *  $this->update->meta(1,[a])
     *  helper method to update meta, tags and topics
     */

    public function __call($method, $args)
    {

        return $this->updateDoc($args[0],$method, $args[1]);

    }

    public function __get($name)
    {

        if (in_array($name, $this->actions)) {

            $this->action = $this->action;
            return $this;
        }


    }

    public function updateIndex($index, $type, $id, $body)
    {

        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => $body
        ];

        return $this->client->update($params);

    }


    public function updateData($index, $type = 'string', $id, $body)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => $body
        ];

        return $this->client->update($params);
    }

    public function updateTags($id, $tags)
    {
        $params = [
            'index' => env('STORAGE_OBJECT_INDEX_NAME'),
            'type' => self::INDEX_TYPE,
            'id' => $id,
            'body' => ['tags'=>$tags]
        ];

        if($this->client->exists(['index' => $params['index'], 'id'=>$id, 'type'=>$params['type']]) === false){
            return true;
        }else{
            $params['body'] = ['doc'=>$params['body']];
            return $this->client->update($params);
        }

    }

    public function updateTopics($id, $topics)
    {
        $params = [
            'index' => env('STORAGE_OBJECT_INDEX_NAME'),
            'type' => self::INDEX_TYPE,
            'id' => $id,
            'body' => ['topics'=>$topics]
        ];

        if($this->client->exists(['index' => $params['index'], 'id'=>$id, 'type'=>$params['type']]) === false){
            return true;
        }else{
            $params['body'] = ['doc'=>$params['body']];
            return $this->client->update($params);
        }
    }


    public function updateDoc($id,$prop ,$val)
    {
        $params = [
            'index' => env('STORAGE_OBJECT_INDEX_NAME'),
            'type' => self::INDEX_TYPE,
            'id' => $id,
            'body' => [
                'doc' => [$prop => $val]
            ]
        ];

        return $this->client->update($params);

    }
}