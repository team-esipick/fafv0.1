<?php
/**
 * Created for Engeo.
 * User: usama
 * Date: 11/22/17
 * Time: 12:06 PM
 */

namespace FAF\Libs;

use Dompdf\Exception;
use GuzzleHttp\Client;
use FAF\Libs\Contracts\IngestorInterface;
use FAF\Exceptions\Handler;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\HasApiTokens;
use GuzzleHttp\Exception\ConnectException;

class TikaIngestor implements IngestorInterface
{
    public $rclient;
    public $logger;
    public function __construct()
    {
       $this->rclient = $qclient = new Client([
            // Base URI is used with relative requests
            // 'base_uri' => 'http://localhost:9998/tika',
            // You can set any number of default request options.
            //'timeout'  => 2.0,
        ]);


    }

    public function getMeta($filepath): array
    {
        $resutl = '';
        try {

            $body = fopen($filepath, "r");

            $r = $this->rclient->request('PUT',
                env('TIKA_HOST').'/meta',
                ['body' => $body,
                    'headers' =>[ "Accept" => "application/json" ]
                ]
            );

            $resutl = jsonDecode(  $r->getBody() , true);


        }catch (ConnectException $e){

            Log::info($e->getMessage());
            return [];

        }
        catch (Exception $e){

            Log::info($e->getMessage());
            return [];

        }

        return  $resutl ;
    }


    public function getText($filepath)
    {
        $resutl = '';

        try {

            $body = fopen($filepath, "r");
            if ($body === FALSE){
                throw new Exception('file not opened correctly.');
            }
            $r = $this->rclient->request('POST',
                env('TIKA_HOST').'/tika/form',
                ['multipart' => [
                    [
                        'name' => 'server1.php',
                        'contents' => $body
                    ],
                ],
                    'headers' => ['Accept'=>'text/plain']
                ]
            );
            //@todo fclose, throwing exception
            //fclose($body);

            if ( in_array($r->getStatusCode(), range(400, 550) )) {
                throw new Exception($r->getBody());
            }

            $resutl = utf8_encode( $r->getBody() );

        }catch (ConnectException $e){
            throw  new  Exception('path: '.$filepath.'  '.$e->getMessage());
            //return null;
        }
        catch (Exception $e){
            throw  new  Exception($e->getMessage());
        }
        return  $resutl;

    }


}