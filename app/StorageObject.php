<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
class StorageObject extends Model
{
    public $index_name ;
    protected $fillable = ['id','user_id','company_id','parent_id','title','s3_name','slug','user_id','detail','size','type','description','short_description','mime_type','ip','lat','lag','city','meta','file_type','is_shared_with_company'];
    protected $hidden = [
        '_lft', '_rgt','parent_id','user_id' , 'description', 'short_description','ancestors'
    ];
    use NodeTrait;
    public function __construct(array  $attributes = []){
        parent::__construct($attributes);
        $this->index_name = env('STORAGE_OBJECT_INDEX_NAME');
    }
    public function storageAccess(){
        return $this->hasMany('FAF\AccessStorageObject','storage_object_id');
    }
    public function owner(){
        return $this->belongsTo('FAF\User','user_id');
    }

    public function tags(){
        return $this->belongsToMany( Tag::class ,'tag_storage_objects')->withPivot('status','type')->withTimestamps();
    }
    public function topics(){
        return $this->belongsToMany( Topic::class ,'topic_storage_objects')->withPivot('status','type')->withTimestamps();
    }

    public function getFileUrl($subdomain){

        if(env('STORATE_ENGINE') == 'sftp'){
            return env('SFTP_WEB_HOST') . '/'.$subdomain.'/' .$this->s3_name;
        }else if(env('STORATE_ENGINE') == 's3'){
            //@todo remove this code
            if($this->created_at < '2018-01-03 06:45:08'){
                $subdomain = 'test';
            }

            return Storage::url($subdomain.'/'.$this->s3_name,Carbon::now()->addMinutes(20));

        } else if(env('STORATE_ENGINE') == 'public'){
            return Storage::url($subdomain.'/'.$this->s3_name );
        }

        return storage_path('app/'.$subdomain.'/' . $this->s3_name );



    }
    public static function put($file){

        if(env('sftp')){
            return  Storage::disk()->put('test', $file);
        }
        return Storage::disk()->put('test',$file);

    }

    public function s3Link($s3_name){
        return env('S3').$s3_name;
    }
    public function company(){
        return $this->belongsTo('FAF\Company','company_id');
    }
    public function getFullFileName()
    {
        return $this->title . '.' . $this->file_type;
    }
    public function filecount($id)
    {
        return StorageObject::where(['parent_id' => $id])->count();
    }
    //Completion for auto complete useful on file name
    //Completion for auto complete useful on author maybe


    public function getMappings(){

        return [
            'mappings'=>[
                'string'=>[
                    'properties'=>[
                        'tags'=>[
                            'type'=>'keyword'
                        ],
                        'topics'=>[
                            'type'=>'keyword'
                        ],
                        'created_at'=>[
                            'type'=>'date',
                            "format" => "yyyy-MM-dd"
                        ],
                        'update_at'=>[
                            'type'=>'date',
                            "format" => "yyyy-MM-dd"
                        ],
                        'description'=>[
                            'type'=>'string',
                            'analyzer' => 'english'

                        ],
                        'file_type'=>[
                            'type'=>'keyword',
                        ],
                        'location' =>[
                            'type' => 'geo_point'
                        ],
                        'author'=>[
                            'type'=>'keyword'
                        ],
                        'mine_type'=>[
                            'type'=>'keyword'
                        ],
                        'size'=>[
                            'type'=>'long'
                        ],
                        'ip' =>[
                            'type' => 'ip'
                        ],
                        'title' =>[
                            'type' => 'completion'
                        ],
                        'tags'=>[
                            'type' => 'keyword'
                        ],
                        'doc_type'=>[
                            'type'=>'keyword'
                        ],

                    ]
                ]
            ]
        ];

    }

}
