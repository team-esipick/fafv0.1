<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'id','name','email', 'phone','sub_domain', 'site_url','site_title','status','storage_folder_name'
    ];
}
