<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //each category might have one parent
    public function parent() {
        return $this->belongsToOne(static::class, 'parent_id');
    }

    //each category might have multiple children
    public function children() {
        return $this->hasMany(static::class, 'parent_id')->orderBy('name', 'asc');
    }
}
