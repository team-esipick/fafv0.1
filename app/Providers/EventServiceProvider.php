<?php

namespace FAF\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'FAF\Events\UploadedToS3' => [
            'FAF\Listeners\ExtractText',
        ],
        'FAF\Events\NewUserSignup' => [
            'FAF\Listeners\SendSignupEmail',
        ],
        'FAF\Events\TagsUploaded' => [
            'FAF\Listeners\IndexTagsToEs',
        ],
        'FAF\Events\TopicsUploaded' => [
            'FAF\Listeners\IndexTopicsToEs',
        ],

        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\Instagram\InstagramExtendSocialite@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
