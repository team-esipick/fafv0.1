<?php
/**
 * Created by PhpStorm.
 * User: usama
 * Date: 11/21/17
 * Time: 3:56 PM
 */
namespace FAF\Providers;
use Illuminate\Support\ServiceProvider;
use Elasticsearch\ClientBuilder;
use FAF\Libs\Elasticsearch\ElasticSearchIndexer;


class FafServiceProvider extends ServiceProvider{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /*
     *  Register the binding
     */
    public function register(){

        $app = $this->app;
        $app->singleton('FAF\Libs\Contracts\IndexerInterface','FAF\Libs\Elasticsearch\ElasticSearchIndexer');
        $app->singleton('FAF\Libs\Contracts\IngestorInterface','FAF\Libs\TikaIngestor');

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['FAF\Libs\ElasticSearchIndexer','FAF\Libs\TikaIngestor'];
    }


}