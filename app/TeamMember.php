<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{

    protected $fillable = ['id','user_id','company_id','team_id','status'];
    protected $hidden = [  ];
    public function user()
    {
        return $this->hasOne('FAF\User','id');
    }
    public function team(){
        return $this->belongsTo('FAF\Team','team_id');
    }
}
