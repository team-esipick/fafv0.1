<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class TopicStorageObject extends Model
{
    protected $fillable = [
        'id','storage_object_id','topic_id','type'
    ];

    public function topic(){
        return $this->belongsTo('FAF\Topic','topic_id');
    }
}
