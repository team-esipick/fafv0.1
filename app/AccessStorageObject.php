<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class AccessStorageObject extends Model
{
    protected $fillable = ['id','storage_object_id','share_with','share_id','read'];
    public function company(){
        return $this->belongsTo('FAF\Company','share_id');
    }
    public function user(){
        return $this->belongsTo('FAF\User','share_id');
    }
    public function team(){
        return $this->belongsTo('FAF\Team','share_id');
    }

    public function storage_object(){
        return $this->belongsTo('FAF\StorageObject','storage_object_id');
    }


}
