<?php namespace FAF;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	//comments table in database
	protected $guarded = [];
	
	// user who commented
	public function author()
	{
		return $this->belongsTo('FAF\User','from_user');
	}
	
	public function post()
	{
		return $this->belongsTo('FAF\Post','on_post');
	}

}
