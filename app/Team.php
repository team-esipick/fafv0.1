<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['id','user_id','company_id','title','description','status'];
    protected $hidden = [  ];

    public function members()
    {
        return $this->hasMany('FAF\TeamMember','team_id');

    }

}
