<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class TagStorageObject extends Model
{
    protected $fillable = [
        'id','storage_object_id','tag_id','type'
    ];
    public function tag(){
        return $this->belongsTo('FAF\Tag','tag_id');
    }

    public function storageObjects(){

        return $this->belongsTo('FAF\StorageObject','storage_object_id')->where('company_id',1);
    }
}
