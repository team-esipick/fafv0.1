<?php

namespace FAF\Listeners;

use FAF\Events\NewUserSignup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class SendSignupEmail  implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserSignup  $event
     * @return void
     */
    public function handle(NewUserSignup $event)
    {


        $email = $event->user->email;
        $name = $event->user->first_name;
        $data = [];
        $data['message'] = "Hello $name,<br><br>
        
        ​​​​​​​ 
        Your account is fully configured on FAF and you can start using it right away to manage your files/content effectively.<br><br>
        
        In case you forgot to note down, please see below for your login detail:<br>
        Username: $email    <br><br>
        
        Thanks,<br>
        FAF Support Team";

        $beautymail = app()->make(Beautymail::class);
        $data['user'] = $event->user;
        $beautymail->send('email.welcome', ['data'=>$data], function($message) use ($data)
        {
            $message
                ->from('info@gofetchcode.com','FAF')
                ->to($data['user']->email)
                ->subject('Your FAF account is ready');
        });


    }
}
