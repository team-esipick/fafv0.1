<?php

namespace FAF\Listeners;
use FAF\StorageObject;
use FAF\Events\UploadedToS3;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UploadedToS3  $event
     * @return void
     */
    public function handle()
    {
        Log::info('uploaded to S3');
    }
}
