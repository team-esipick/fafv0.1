<?php

namespace FAF\Listeners;

use FAF\Events\TopicsUploaded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use FAF\Libs\Contracts\IndexerInterface;

class IndexTopicsToEs implements ShouldQueue
{
    protected $Indexer;
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'web';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(IndexerInterface $indexer)
    {
        $this->Indexer = $indexer;
    }

    /**
     * Handle the event.
     *
     * @param  TopicsUploaded  $event
     * @return void
     */
    public function handle(TopicsUploaded $event)
    {
        $this->Indexer->updateTopics($event->storage_object_id,$event->EsTopics);
    }
}
