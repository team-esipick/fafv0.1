<?php

namespace FAF\Listeners;

use Carbon\Carbon;
use FAF\Events\UploadedToS3;
use FAF\Http\Controllers\TagController;
use FAF\Libs\Contracts\IngestorInterface;
use FAF\Libs\Dexter;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use FAF\StorageObject;
use Illuminate\Support\Facades\Log;
use Elasticsearch\ClientBuilder;
use FAF\Tag;
use FAF\Topic;
use FAF\TagStorageObject;
use FAF\TopicStorageObject;
use FAF\Libs\Contracts\IndexerInterface;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Illuminate\Foundation\Application;

class ExtractText implements ShouldQueue
{
    protected $Ingestor;
    protected $Indexer;
    protected $dexter;


    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct(IngestorInterface $ingestor, IndexerInterface $indexer ,Dexter $dexter)
    {
        $this->Ingestor = $ingestor;
        $this->Indexer = $indexer;
        $this->dexter = $dexter;
        $this->queue='test1';
    }

    private function resetLocations($mlResult) {
        $fractal = new Manager();
//dd($mlResult);
        $locationData = new Collection($mlResult, function( $result)  {
            return [
                'address'  => $result->address ?? '',
                'city' => $result->loc ?? '',
                'locations' => [
                    'lat'=>$result->lat ?? '',
                    'lon'=>$result->lon ?? '',
                ]
            ];
        });

        $locations = $fractal->createData($locationData)->toArray();
        //Remove Zero index
        $locations = array_shift($locations);
        return $locations;

    }

    /**
     * Handle the event.
     *
     * @param  UploadedToS3  $event
     * @return void
     */
    public function handle(UploadedToS3 $event)
    {

        try{
            // file url
            $ml_json_data = new \stdClass();
            $locations = [];
            $content = $this->Ingestor->getText($event->node->getFileUrl($event->node->company->storage_folder_name));

            if(substr($event->node->mime_type, 0, 5) == 'image') {
                if(!empty($event->node->lat)){
                    $location = [
                        'lat'=>(float)$event->node->lat,
                        'lon'=>(float)$event->node->lag,
                    ];
                    $locations[] = (object)$location;
                }
            } else{
                $ml_json_data = $this->dexter->analyze($event->node->id, $content);
            }


            if($content === null){
                //@todo save the status on storage object
                return ;
            }

            if(!empty($ml_json_data->locations)){
                $locations = $ml_json_data->locations;
            }
            //dd($locations);
            if($locations){
                $locations = $this->resetLocations($locations);
            }


            $event->node->short_description = str_limit($content,200);
            $event->node->save();
            $shared_company_id =  $event->node->is_shared_with_company ? [$event->node->company_id] : null;

            $this->Indexer->store(
                $event->node->index_name,
                'doc',
                $event->node->id,
                [
                    'description' => $content,
                    'short_description' => $event->node->short_description,
                    'title'=> $event->node->title ,
                    'author' => '',
                    'company_id'=> $event->node->company_id ,
                    'user_id' => $event->node->user_id,
                    'team_ids' => [],
                    // 'team_id' => '',  @todo include in future
                    'user_ids' => [],
                    'company_ids' => [],
                    'company_name'=> $event->node->company->name,

                    'file_size'=> $event->node->size,
                    'file_extn'=> strtolower($event->node->file_type),
                    'file_mime_type'=> $event->node->mime_type,
                    'created_at'=> Carbon::parse($event->node->created_at)->format('Y-m-d H:i:s'),
                    'updated_at'=> Carbon::parse($event->node->created_at)->format('Y-m-d H:i:s'),
                    'tags'=>  [],
                    'topics'=> [],
                    'city'=> $event->node->city,
                    'country' => $event->node->city,
                    'locations' => $locations ?? new \stdClass(),
                    'ip' =>empty($event->node->ip) ? '0.0.0.0':$event->node->ip
                ]

            );


            logger(json_encode($ml_json_data));

            if ( $ml_json_data == null ) {
                return;
            }

            if( !empty($ml_json_data->Tags)){

                $tag = new Tag();
                $allTags = $tag->storeTags($ml_json_data,$event->node,'ml');
                $this->Indexer->updateTags($event->node->id, $allTags);
            }

            if( !empty($ml_json_data->Topics) ){

                $topic = new Topic();
                $allTopics = $topic->storeTopics($ml_json_data,$event->node,'ml');
                $this->Indexer->updateTopics($event->node->id, $allTopics);

            }


        }catch (Exception $e){
            Log::info($e->getMessage());
            echo $e->getMessage();
        }
    }
}
