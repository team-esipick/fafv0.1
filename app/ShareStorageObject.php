<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class ShareStorageObject extends Model
{
    protected $fillable = ['storage_object_id','token','token_expire'];

    public function storage(){
        return $this->belongsTo('FAF\StorageObject','storage_object_id');
    }
}
