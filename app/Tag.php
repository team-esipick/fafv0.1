<?php

namespace FAF;

use Dompdf\Exception;
use FAF\Events\TagsUploaded;
use FAF\Listeners\IndexTagsToEs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rules\In;
use Laravel\Scout\Searchable;
use FAF\Libs\Contracts\IndexerInterface;
use FAF\TagStorageObject;
use Illuminate\Support\Facades\App;
class Tag extends Model
{
    use Searchable;

    protected $fillable = [
        'id','company_id','user_id','title','slug','type'
    ];
    protected $hidden = [
         'status',
    ];

    protected $Indexer;
    public function __construct()
    {
        $this->Indexer = App::make('FAF\Libs\Contracts\IndexerInterface');
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'tags_index';
    }
    public function getTypeAttribute(){

        $this->TagStorageObject->type;
    }
    /**
     * Get the index name for the model.
     *
     * @return mixed FALSE if failure, object if success
     */

    public function store($data,$type = 'user'){


        try{

        $tag  = Tag::updateOrCreate(['title'=>$data['title']], $data );
        $tagStorageAttrributes = [
            'storage_object_id'=>$data['storage_object_id'],
            'tag_id'=>$tag->id,
            'type'=>$type

        ];
        $tso =  TagStorageObject::updateOrCreate(['storage_object_id'=>$data['storage_object_id'],
            'tag_id'=>$tag->id],$tagStorageAttrributes);

        }catch (Exception $e){

            return false;
        }

        /**
         *  We are expliciting indexing tags in case of
         *  when extractText job is ran for a document, so no
         *  need to index it again.
         */
        if($type === 'ml'){
            return true;
        }
        $EsTags=[];
        $allCorrectTags = TagStorageObject::
        where( [ 'storage_object_id' => $tso->storage_object_id ] )
            ->whereIn('status',[0,1])->get();

        foreach ($allCorrectTags as $tags){
            $EsTags[]=$tags->tag->title;
        }

        event(new TagsUploaded($data['storage_object_id'], $EsTags));

        return $EsTags;


    }

    public function storeTags($ml_json_data,$so,$type = 'user' ){

        if( !isset($ml_json_data->Tags) ){
            return true;
        }

        foreach($ml_json_data->Tags as $tag){
            $attrributes = [
                'company_id'=> $so->company_id,
                'user_id'=>$so->user_id,
                'title'=>$tag,
                'slug'=>str_slug($tag),
                'storage_object_id'=> $so->id,
            ];

             $this->store($attrributes,$type);

        }
        //Get all storage object tags
        $allTags = TagStorageObject::where(['storage_object_id'=>$attrributes['storage_object_id']])->get();
        $EsTags=[];
        foreach ($allTags as $tags){
            $EsTags[]=$tags->tag->title;
        }

        return $EsTags;
    }

}
