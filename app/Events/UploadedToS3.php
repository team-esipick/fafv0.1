<?php

namespace FAF\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use FAF\StorageObject;


class UploadedToS3
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $node;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StorageObject $node, $queuename)
    {
        //
        $this->node = $node;
        $this->queue = $queuename ?? 'default1';
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
