<?php

namespace FAF\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TagsUploaded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $EsTags;
    public $storage_object_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($storage_object_id, $EsTags)
    {
        $this->EsTags = $EsTags;
        $this->storage_object_id = $storage_object_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
