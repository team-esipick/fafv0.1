<?php namespace FAF;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

	//posts table in database
	protected $guarded = [];
	public function comments()
	{
		return $this->hasMany('FAF\Comment','on_post');
	}
	
	public function author()
	{
		return $this->belongsTo('FAF\User','author_id');
	}

}
