<?php

namespace FAF\Console\Commands;

use Illuminate\Console\Command;


class TestFileCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'directory:filecount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count files in given directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        function recursiveDirectoryIterator($path) {
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path,\RecursiveDirectoryIterator::SKIP_DOTS)) as $file) {
                if (!$file->isDir()) {
                    yield $file->getFilename() . $file->getExtension();
                }
            }
        }

        $start = microtime(true);
        $instance = recursiveDirectoryIterator('/home/faf/files');
        //$instance =  Storage::allFiles('/Users/usama/PhpstormProjects/jane/vendor');
        //dd($instance);
        $total_files = 0;
        $values = [];
        foreach($instance as $value) {
            $values[] =  $value ;
            $total_files++;
        }
        echo "Mem peak usage: " . (memory_get_peak_usage(true)/1024/1024)." MiB";
        echo "Total number of files: " . $total_files;
        echo "Completed in: ", microtime(true) - $start, " seconds";
        logger( \GuzzleHttp\json_encode( $values ));




        exit;


    }
}
