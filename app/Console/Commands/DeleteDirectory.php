<?php

namespace FAF\Console\Commands;

use FAF\StorageObject;
use FAF\TagStorageObject;
use FAF\TopicStorageObject;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DeleteDirectory extends Command
{

    public $user_id;
    public $disk;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'directory:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all files from disks and DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->user_id = $this->ask('User ID which you want to delete all files?');
        $this->disk = $this->ask('Please enter disk (s3, sftp, local, public)?');
        $allFiles = StorageObject::where('user_id',$this->user_id)->get();
        Storage::disk($this->disk )->delete(array_pluck($allFiles->toArray(),'s3_name'));
        StorageObject::destroy(array_pluck($allFiles->toArray(),'id'));
        TagStorageObject::destroy(array_pluck($allFiles->toArray(),'storage_object_id'));
        TopicStorageObject::destroy(array_pluck($allFiles->toArray(),'storage_object_id'));
    }
}
