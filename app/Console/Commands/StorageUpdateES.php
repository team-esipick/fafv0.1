<?php

namespace FAF\Console\Commands;

use FAF\Libs\Contracts\IndexerInterface;
use FAF\Libs\Contracts\IngestorInterface;
use FAF\Libs\Dexter;
use FAF\Listeners\ExtractText;
use FAF\StorageObject;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use FAF\Tag;
use FAF\Topic;
use Carbon\Carbon;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class StorageUpdateES extends Command
{
    protected $Ingestor;
    protected $Indexer;
    protected $dexter;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:updatees {--storageobjectid= : storage object id }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IngestorInterface $ingestor, IndexerInterface $indexer ,Dexter $dexter)
    {
        parent::__construct();
        $this->Ingestor = $ingestor;
        $this->Indexer = $indexer;
        $this->dexter = $dexter;
    }


    private function resetLocations($mlResult) {
        $fractal = new Manager();
//dd($mlResult);
        $locationData = new Collection($mlResult, function( $result)  {
            return [
                'address'  => $result->address ?? '',
                'city' => $result->loc ?? '',
                'location' => [
                    'lat'=>$result->lat ?? '',
                    'lon'=>$result->lon ?? '',
                ]
            ];
        });

        $locations = $fractal->createData($locationData)->toArray();
        //Remove Zero index
        $locations = array_shift($locations);
        return $locations;

    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
/*
        if($this->argument('user_id') != 0){
            $storageObjects = StorageObject::with(['tags', 'topics'])
                ->where('user_id',(int)$this->argument('userid'))
                ->whereNull('type')
                ->get();

        }*/
        if($this->option('storageobjectid') != 0){
            $storageObjects = StorageObject::with(['tags', 'topics'])
                ->where('id',(int)$this->option('storageobjectid'))
                ->whereNull('type')
                ->get();

        }
        foreach ($storageObjects as $storageObject) {
            //if (count($storageObject->tags) == 0 || count($storageObject->topics) == 0) {
            if (true) {

                try{
                    // file url
                    $ml_json_data = new \stdClass();
                    $locations = [];
                    $content = $this->Ingestor->getText($storageObject->getFileUrl($storageObject->company->storage_folder_name));
                    $meta = $this->Ingestor->getMeta($storageObject->getFileUrl($storageObject->company->storage_folder_name));

                    if(substr($storageObject->mime_type, 0, 5) == 'image') {
                        if(!empty($storageObject->lat)){
                            $location = [
                                'lat'=> $storageObject->lat,
                                'lon'=> $storageObject->lag,
                            ];
                            $locations[] = (object)$location;
                        }
                    } else{
                        $ml_json_data = $this->dexter->analyze($storageObject->id, $content);
                    }

                    if($content === null){
                        //@todo save the status on storage object
                        return ;
                    }

                    if(!empty($ml_json_data->locations)){
                        $locations = $ml_json_data->locations;
                    }
                    //dd($locations);
                    if($locations){
                        $locations = $this->resetLocations($locations);
                    }

                    //dd($locations);
                    $storageObject->short_description = str_limit($content,200);
                    $storageObject->save();

                    $this->Indexer->store(
                        $storageObject->index_name,
                        'doc',
                        $storageObject->id,
                        [
                            'description' => $content,
                            'short_description' => $storageObject->short_description,
                            'title'=> $storageObject->title ,
                            'author' => '',

                            'company_id'=> $storageObject->company_id ,
                            'user_id' => $storageObject->user_id,
                            'team_ids' => [],
                            // 'team_id' => '',  @todo include in future
                            'user_ids' => [],
                            'company_name'=> $storageObject->company->name,

                            'file_size'=> $storageObject->size,
                            'file_extn'=> $storageObject->file_type,
                            'file_mime_type'=> $storageObject->mime_type,
                            'created_at'=> Carbon::parse($storageObject->created_at)->format('Y-m-d H:i:s'),
                            'updated_at'=> Carbon::parse($storageObject->created_at)->format('Y-m-d H:i:s'),
                            'tags'=>  [],
                            'topics'=> [],
                            'city'=> $storageObject->city,
                            'country' => $storageObject->city,
                            'location' => $locations ?? new \stdClass(),
                            'ip' =>empty($storageObject->ip) ? '0.0.0.0':$storageObject->ip
                        ]

                    );


                    if ( $ml_json_data === null ) {
                        return;
                    }
                   if( !empty($ml_json_data->Tags)){

                        $tag = new Tag();
                        $allTags = $tag->storeTags($ml_json_data,$storageObject,'ml');
                        $this->Indexer->updateTags($storageObject->id, $allTags);
                    }

                    if( !empty($ml_json_data->Topics) ){
                        $topic = new Topic();
                        $allTopics = $topic->storeTopics($ml_json_data,$storageObject,'ml');
                        $this->Indexer->updateTopics($storageObject->id, $allTopics);
                    }

                }catch (Exception $e){
                    Log::info($e->getMessage());
                    echo $e->getMessage();
                }
            }
        }
    }
}
