<?php

namespace FAF\Console\Commands;

use FAF\StorageObject;
use Illuminate\Console\Command;

class StorageMoveExtension extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:extension';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replace extension from s3_name to file_type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $files = StorageObject::where(['type'=>null])->get();
        foreach ($files as $file) {
            $file->file_type= pathinfo($file->s3_name, PATHINFO_EXTENSION);
            $file->save();
        }

    }
}
