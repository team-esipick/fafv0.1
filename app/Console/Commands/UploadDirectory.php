<?php

namespace FAF\Console\Commands;

use Dompdf\Exception;
use FAF\Company;
use FAF\Jobs\ExtractText;
use FAF\StorageObject;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use FAF\Events\UploadedToS3;
use League\Flysystem\Util\MimeType;

class UploadDirectory extends Command
{
    public $company_id;
    public $user_id;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'directory:upload {--company_id= : company id of user } {--user_id= : user id of user }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload Directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //@todo check the company and user exist
        $this->company_id = $this->option('company_id');
        $this->user_id = $this->option('user_id');

        if($this->company_id == null  || $this->user_id == null){
            dd('aborting! give php artisan directory:upload --user_id=xx and --company_id=xx as option values');

        }

        $company = Company::find($this->company_id);

        $files = Storage::disk('sftp')->allFiles('files');

        $count = 0;
        foreach ($files as $file)
        {
            $this->uploadDirectory((string)$file,$company);
            //if($count==50)break;
            //
            $count++;
        }

    }

    /**
     * This method can upload a directory to any FAF
     * internal storage, right now user drive is sftp_user defined in
     *
     *
     *
     * @param $path
     */
    private function uploadDirectory($path, $company){

        $directories = explode("/",$path);
        //Get file from path
        $fil_name_with_ext = array_last($directories);
        $file = getFileNameWithoutExtension( $fil_name_with_ext );
        $ext = pathinfo($fil_name_with_ext, PATHINFO_EXTENSION);

        //Remove file from array
        array_pop($directories);
        $parent_id = null;
        //Folders insertion
        foreach ($directories as $directory)
        {
            $slug = str_slug($directory);
            $attributes=[
                'user_id'=>$this->user_id,
                'company_id'=>$this->company_id,
                'parent_id'=>$parent_id,
                's3_name'=> $slug,
                'title'=> $directory,
                'slug'=>$slug,
                'size'=>'',
                'type'=>'folder',
                'file_type'=>'',
                'ip'=>'',
                'lat'=>'',
                'lag'=>'',
                'city'=>'',
                'mime_type'=>'',

            ];
            $storageObject = StorageObject::firstOrNew(['slug' => $slug, 'user_id'=>$this->user_id, 'company_id'=>$this->company_id, 'parent_id'=>$parent_id],$attributes );
            $storageObject->save();
            $parent_id = $storageObject->id;
        }
        $fileExist = StorageObject::where(['user_id'=>$this->user_id, 'parent_id'=>$parent_id, 'slug'=>str_slug($file)])->count();
        //@todo think of better way of naming files
        $s3_name = time() . md5(uniqid(rand(),true));

        //File insert

        if($fileExist == 0){
            try{
                //Commentted if we are not copying
                Storage::disk('s3')->put($company->storage_folder_name.'/'.$s3_name.'.'.$ext, Storage::disk('sftp_user')->get($path));
            }catch (Exception $ex){
                logger($ex->getMessage());
            }
            $attributes=[
                'user_id'=>$this->user_id,
                'company_id'=>$this->company_id,
                'parent_id'=>$parent_id,
                's3_name'=>$s3_name . '.' . $ext,
                'title'=> $file,
                'slug'=>str_slug($file),
                'size'=>Storage::disk('sftp_user')->size($path),
                'file_type'=>$ext,
                'ip'=>'0.0.0.0',
                'lat'=>'',
                'lag'=>'',
                'city'=>'',
                'mime_type'=>MimeType::detectByFileExtension($ext),

            ];
            $fileStorageObject = StorageObject::create($attributes );
            //
            ExtractText::dispatch($fileStorageObject)->onQueue('command');
            //event(new UploadedToS3($fileStorageObject,'command'));
        }
    }

}
