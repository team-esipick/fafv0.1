<?php

namespace FAF\Console\Commands;

use FAF\FailedJob;
use Illuminate\Console\Command;
use Illuminate\Queue\Events\JobFailed;

class StorageInvalidExtension extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $failed_jobs = FailedJob::first();
        print_r(serialize(json_decode($failed_jobs->toArray()['payload'])->data->command));
    }
}
