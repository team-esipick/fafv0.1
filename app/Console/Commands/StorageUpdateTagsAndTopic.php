<?php

namespace FAF\Console\Commands;

use FAF\Events\UploadedToS3;
use FAF\Jobs\ExtractText;
use FAF\Libs\Contracts\IndexerInterface;
use FAF\Libs\Contracts\IngestorInterface;
use FAF\Libs\Dexter;
use FAF\StorageObject;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class StorageUpdateTagsAndTopic extends Command
{
    public $storage_object_id;
    public $user_id;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:updatetagsandtopics {--storage_object_id= : company id of user } {--user_id= : user id of user }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $Ingestor;
    private $dexter;
    private $ingestor;
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->storage_object_id = $this->option('storage_object_id');
        $this->user_id = $this->option('user_id');

        if(empty($this->storage_object_id) && empty($this->user_id) ) {
            $storageObjects = StorageObject::with(['tags', 'topics'])
                ->whereNull('type')
                ->get();
        }

        if($this->user_id){
            $storageObjects = StorageObject::with(['tags', 'topics'])
                ->where('user_id',(int)$this->user_id)
                ->whereNull('type')
                ->get();

        }
        if($this->storage_object_id){
            $storageObjects = StorageObject::with(['tags', 'topics'])
                ->where('id',(int)$this->storage_object_id)
                ->whereNull('type')
                ->get();

        }
        foreach ($storageObjects as $storageObject) {
            // if (count($storageObject->tags) == 0 || count($storageObject->topics) == 0) {
            if (true) {
                ExtractText::dispatch($storageObject)->onQueue('command');
                //event(new UploadedToS3($storageObject ,'command'));
            }
        }
    }
}
