<?php

namespace FAF;
use Dompdf\Exception;
use FAF\Events\TopicsUploaded;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rules\In;
use Laravel\Scout\Searchable;
use FAF\Libs\Contracts\IndexerInterface;
use FAF\TopicStorageObject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

class Topic extends Model
{
    use Searchable;

    protected $fillable = [
        'id','company_id','user_id','title','slug','type'
    ];
    protected $hidden = [
        'status'
    ];
    protected $Indexer;
    public function __construct()
    {
        $this->Indexer = App::make('FAF\Libs\Contracts\IndexerInterface');
    }
    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'topics_index';
    }

    /**
     * Get the index name for the model.
     * @param array $data
     * @param string $type default to user, ml for when called from extractText.php after reading from machine learning module
     * @return mixed FALSE if failure, object if success
     */

    public function store($data,$type = 'user'){


        try{

            $topic  = Topic::updateOrCreate(['title'=>$data['title']], $data );
            $topicStorageAttrributes = [

                'storage_object_id'=>$data['storage_object_id'],
                'topic_id'=>$topic->id,
                'type'=>$type

            ];
            $tso =  TopicStorageObject::updateOrCreate(['storage_object_id'=>$data['storage_object_id'],
                'topic_id'=>$topic->id],$topicStorageAttrributes);

        }catch (Exception $e){

            return false;
        }
        /*
          *  We are expliciting indexing topic in case of
          *  when extractText job is ran for a document, so no
          *  need to index it again.
          */
        if($type === 'ml'){
            return true;
        }
        $EsTopics=[];
        $allCorrectTopics = TopicStorageObject::
        where( [ 'storage_object_id' =>  $tso->storage_object_id] )
            ->whereIn('status',[0,1])->get();

        foreach ($allCorrectTopics as $topics){
            $EsTopics[]=$topics->topic->title;
        }
        event(new TopicsUploaded($data['storage_object_id'],$EsTopics));
        //$this->Indexer->updateTopics($data['storage_object_id'],$EsTopics );
        return $EsTopics;



    }
    /**
     * Helper function to store a array of topic into
     * data and Elastic search
     * @param array $ml_json_data object containing list of topic
     * @param StorageObject $so
     * @param string $type default to user, ml for when called from extractText.php after reading from machine learning module
     * @return array a list of topics
     */

    public function storeTopics($ml_json_data, $so ,$type = 'user' ){

        if( !isset($ml_json_data->Topics) ){
            return true;
        }
        foreach($ml_json_data->Topics as $topic){
            $attrributes = [
                'company_id'=> $so->company_id,
                'user_id'=>$so->user_id,
                'title'=>$topic,
                'slug'=>str_slug($topic),
                'storage_object_id'=> $so->id,
            ];
            $this->store($attrributes,$type);
        }


        //Get all storage object topics
        $allTopics = TopicStorageObject::where(['storage_object_id'=>$attrributes['storage_object_id']])->get();
        $EsTopics=[];
        foreach ($allTopics as $topics){
            $EsTopics[]=$topics->topic->title;
        }

        return $EsTopics;
    }

}
