<?php

namespace FAF\Exceptions;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Dingo\Api\Exception\DeleteResourceFailedException,
    Dingo\Api\Exception\ResourceException,
    Dingo\Api\Exception\StoreResourceFailedException,
    Dingo\Api\Exception\UpdateResourceFailedException;



class Rescue extends HttpException{

    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int         $code     The internal exception code
     *
     * @return code 403
     */
    public static function AccessDenied($message = null, \Exception $previous = null, $code = 0){

        throw new AccessDeniedHttpException($message);
    }


    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     *
     * @return code 400
     */
    public static function BadRequest($message = null, \Exception $previous = null, $code = 0){

        throw new BadRequestHttpException($message);
    }



    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     *
     * @return code 409
     */
    public static function ConflictHttp($message = null, \Exception $previous = null, $code = 0){

        throw new ConflictHttpException($message);
    }


    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     *
     * @return code 412
     */
    public static function PreconditionFailed($message = null, \Exception $previous = null, $code = 0){

        throw new PreconditionFailedHttpException($message);
    }


    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     *
     * @return code 428
     */
    public static function PreconditionRequired($message = null, \Exception $previous = null, $code = 0){

        throw new PreconditionRequiredHttpException($message);
    }

    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     *
     * @return code 401
     */
    public static function Unauthorized($message = null, \Exception $previous = null, $code = 0){

        throw new UnauthorizedHttpException($message);
    }

    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     *
     * @return code 415
     */
    public static function UnsupportedMediaType($message = null, \Exception $previous = null, $code = 0){

        throw new UnsupportedMediaTypeHttpException($message);
    }

    /**
     * Create a new resource exception instance.
     *
     * @param string                               $message
     * @param \Illuminate\Support\MessageBag|array $errors
     * @param \Exception                           $previous
     * @param array                                $headers
     * @param int                                  $code
     *
     * @return void
     */
    public static function DeleteFailed($message = null, $errors = null, Exception $previous = null, $headers = [], $code = 0){

        throw new DeleteResourceFailedException($message,$errors);
    }

    /**
     * Create a new resource exception instance.
     *
     * @param string                               $message
     * @param \Illuminate\Support\MessageBag|array $errors
     * @param \Exception                           $previous
     * @param array                                $headers
     * @param int                                  $code
     *
     * @return void
     */
    public static function StoreFailed($message = null, $errors = null, Exception $previous = null, $headers = [], $code = 0){

        throw new StoreResourceFailedException($message,$errors);
    }

    /**
     * Create a new resource exception instance.
     *
     * @param string                               $message
     * @param \Illuminate\Support\MessageBag|array $errors
     * @param \Exception                           $previous
     * @param array                                $headers
     * @param int                                  $code
     *
     * @return void
     */
    public static function UpdateFailed($message = null, $errors = null, Exception $previous = null, $headers = [], $code = 0){

        throw new UpdateResourceFailedException($message,$errors);
    }

    /**
     * Create a new resource exception instance.
     *
     * @param string                               $message
     * @param \Illuminate\Support\MessageBag|array $errors
     * @param \Exception                           $previous
     * @param array                                $headers
     * @param int                                  $code
     *
     * @return void
     */
    public static function ResourceException($message = null, $errors = null, Exception $previous = null, $headers = [], $code = 0){

        throw new ResourceException($message,$errors);
    }


}