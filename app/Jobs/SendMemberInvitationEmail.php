<?php

namespace FAF\Jobs;

use FAF\User;
use FAF\UserCompany;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use FAF\Mail\MemberInvitationEmail;
use Snowfire\Beautymail\Beautymail;

class SendMemberInvitationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    protected $userCompany;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $user, $userCompany )
    {
        $this->user = $user;
        $this->userCompany = $userCompany;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data =[
            'user' => $this->user,
            'message'=>'Your account is fully configured on FAF and you can start using it right away to manage your files/content effectively. <br>Please use below link to access your account:',
            'link' => env('FRONT_APP_URL').'/invitation?token'.$this->user->invitation_token,
            'subject' => 'Your FAF account is ready'
        ];
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('email.MemberInvitation', ['data'=>$data], function($message) use ($data)
        {
            $message
                ->from('info@fetchafile.com','Fetchafile')
                ->to($data['user']->email, $data['user']->first_name)
                ->subject($data['subject']);
        });

      /*  $email = new MemberInvitationEmail($this->user,  $this->userCompany);
        dd($this->user->email);
        Mail::to($this->user->email)->send($email);*/
    }
}
