<?php

namespace FAF\Jobs;

use FAF\Extension;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use FAF\Events\UploadedToS3;
use FAF\Http\Controllers\TagController;
use FAF\Libs\Contracts\IngestorInterface;
use FAF\Libs\Dexter;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use FAF\StorageObject;
use Illuminate\Support\Facades\Log;
use Elasticsearch\ClientBuilder;
use FAF\Tag;
use FAF\Topic;
use FAF\TagStorageObject;
use FAF\TopicStorageObject;
use FAF\Libs\Contracts\IndexerInterface;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Illuminate\Foundation\Application;

class ExtractText implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $Ingestor;
    protected $Indexer;
    protected $dexter;
    protected $storageObject;
    public $queue;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StorageObject $storageObject)
    {
        $this->storageObject = $storageObject;

    }

    private function resetLocations($mlResult) {
        $fractal = new Manager();
//dd($mlResult);
        $locationData = new Collection($mlResult, function( $result)  {
            return [
                'address'  => $result->address ?? '',
                'city' => $result->loc ?? '',
                'locations' => [
                    'lat'=>$result->lat ?? '',
                    'lon'=>$result->lon ?? '',
                ]
            ];
        });

        $locations = $fractal->createData($locationData)->toArray();
        //Remove Zero index
        $locations = array_shift($locations);
        return $locations;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(IngestorInterface $ingestor, IndexerInterface $indexer ,Dexter $dexter)
    {
        $this->Ingestor = $ingestor;
        $this->Indexer = $indexer;
        $this->dexter = $dexter;
        $storageObject = $this->storageObject;

        try{
            // file url
            $ml_json_data = new \stdClass();
            $locations = [];
            $skippedExtension = array_pluck(Extension::all()->toArray(),'type');
            if(!in_array ($storageObject->file_type,array_pluck($skippedExtension,'type'))) {
                $content = $this->Ingestor->getText($storageObject->getFileUrl($storageObject->company->storage_folder_name));

                if(substr($storageObject->mime_type, 0, 5) == 'image') {
                    if(!empty($storageObject->lat)){
                        $location = [
                            'lat'=>(float)$storageObject->lat,
                            'lon'=>(float)$storageObject->lag,
                        ];
                        $locations[] = (object)$location;
                    }
                } else{
                    $ml_json_data = $this->dexter->analyze($storageObject->id, $content);
                }


                if($content === null){
                    //@todo save the status on storage object
                    return ;
                }

                if(!empty($ml_json_data->locations)){
                    $locations = $ml_json_data->locations;
                }
                //dd($locations);
                if($locations){
                    $locations = $this->resetLocations($locations);
                }


                $storageObject->short_description = str_limit($content,200);
                $storageObject->save();
                $shared_company_id =  $storageObject->is_shared_with_company ? [$storageObject->company_id] : null;

                $this->Indexer->store(
                    $storageObject->index_name,
                    'doc',
                    $storageObject->id,
                    [
                        'description' => $content,
                        'short_description' => $storageObject->short_description,
                        'title'=> $storageObject->title ,
                        'author' => '',
                        'company_id'=> $storageObject->company_id ,
                        'user_id' => $storageObject->user_id,
                        'team_ids' => [],
                        // 'team_id' => '',  @todo include in future
                        'user_ids' => [],
                        'company_ids' => [],
                        'company_name'=> $storageObject->company->name,

                        'file_size'=> $storageObject->size,
                        'file_extn'=> strtolower($storageObject->file_type),
                        'file_mime_type'=> $storageObject->mime_type,
                        'created_at'=> Carbon::parse($storageObject->created_at)->format('Y-m-d H:i:s'),
                        'updated_at'=> Carbon::parse($storageObject->created_at)->format('Y-m-d H:i:s'),
                        'tags'=>  [],
                        'topics'=> [],
                        'city'=> $storageObject->city,
                        'country' => $storageObject->city,
                        'locations' => $locations ?? new \stdClass(),
                        'ip' =>empty($storageObject->ip) ? '0.0.0.0':$storageObject->ip
                    ]

                );


                //logger(json_encode($ml_json_data));

                if ( $ml_json_data == null ) {
                    return;
                }

                if( !empty($ml_json_data->Tags)){

                    $tag = new Tag();
                    $allTags = $tag->storeTags($ml_json_data,$storageObject,'ml');
                    $this->Indexer->updateTags($storageObject->id, $allTags);
                }

                if( !empty($ml_json_data->Topics) ){

                    $topic = new Topic();
                    $allTopics = $topic->storeTopics($ml_json_data,$storageObject,'ml');
                    $this->Indexer->updateTopics($storageObject->id, $allTopics);

                }

            }


        }catch (Exception $e){
            Log::info($e->getMessage());
            echo $e->getMessage();
        }
    }
}
