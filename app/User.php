<?php

namespace FAF;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use FAF\SocialProvider;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;
use FAF\Notifications\MailResetPasswordToken;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','first_name','last_name', 'email','phone', 'password','verify_token','invitation_token','invitation_verified','invited_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'phone', 'role', 'stripe_id','card_brand','card_brand','verify_token','password_set_token'
    ];

    public function soicalProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

    // user has many posts
    public function posts()
    {
        return $this->hasMany('FAF\Post','author_id');
    }
    // user has many posts
    public function companies()
    {
        return $this->belongsToMany(Company::class,'user_companies')
            ->withTimestamps();
    }

    // user has many posts
    public function firstCompany()
    {
        return $this->hasOne(UserCompany::class,'user_id');
    }
    // user has many posts
    public function hasCompanyBySubdomain($sub_domain)
    {
       return $this->companies
            ->where('sub_domain', $sub_domain )
            ->count() > 0 ;
    }
    public function company_validate($id){

       return $this->hasMany('FAF\UserCompany','user_id')->where(['company_id'=>$id,'user_id'=>$this->id])->count() !== 0 ;
    }
    public function team_validate($id){
        return $this->hasMany('FAF\TeamMember','user_id')->where(['team_id'=>$id,'user_id'=>$this->id])->count() !== 0;
    }

    // user has many comments
    public function comments()
    {
        return $this->hasMany('FAF\Comment','from_user');
    }

    public function can_post()
    {
        $role = $this->role;
        if($role == 'author' || $role == 'admin')
        {
            return true;
        }
        return false;
    }

    public function is_admin()
    {
        $role = $this->role;
        if($role == 'admin')
        {
            return true;
        }
        return false;
    }

    public function sendPasswordResetNotification($token){

        $this->notify(new MailResetPasswordToken($token,$this));
    }
}
