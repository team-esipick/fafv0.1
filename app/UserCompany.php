<?php

namespace FAF;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    protected $fillable = [
        'user_id','company_id'
    ];

    public function company(){
        return $this->belongsTo('FAF\Company','company_id');
    }
}
