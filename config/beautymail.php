<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#dedede',
        'button'    => '#dedede',

    ],

    'view' => [
        'senderName'  => null,
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,

        'logo'        => [
            'path'   => 'http://staging.fetchafile.com/assets/images/logo.png',
            'width'  => '155',
            'height' => '30',
        ],

        'twitter'  => null,
        'facebook' => null,
        'flickr'   => null,
    ],

];
